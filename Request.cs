﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace WebServer
{
	public class Request
	{
		/// <summary>
		/// HTTP Method (GET/POST/etc)
		/// </summary>
		public string Method;

		/// <summary>
		/// Whole URL that was invoked
		/// </summary>
		public string URL;

		/// <summary>
		/// List of query parameters supplied by the user
		/// </summary>
		public Dictionary<string, string> QueryParameters;

		/// <summary>
		/// Class name that will be called for an API request
		/// </summary>
		public string ClassName;

		/// <summary>
		/// Method name that will be called for an API request
		/// </summary>
		public string MethodName;

		/// <summary>
		/// Not derrived in a secure way. This is not security.
		/// But we can use it anyway (if we want)
		/// </summary>
		public long SessionId;

		/// <summary>
		/// The prefix used in this url -- should be 'api' or 'files'
		/// </summary>
		public string Prefix;

		/// <summary>
		/// The requested static resource (for file requests only)
		/// </summary>
		public string FileName;

		/// <summary>
		/// Deserialises JSON data sent by the client.
		/// </summary>
		public T GetFormData<T>()
		{
			return Common.Deserialiser.Deserialise<T>(Common.SerialiserType.JSON, RawRequest.InputStream);
		}

		/// <summary>
		/// Access to the underlying request object. For any advanced use-case.
		/// </summary>
		public HttpListenerRequest RawRequest;
	}
}
