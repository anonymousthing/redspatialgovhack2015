﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;

namespace WebServer
{
	public class FileCache
	{
		bool CaseSensitive;
		long MaxCacheSize;
		long CacheSize;
		long MaxCachableFileSize;

		/// <summary>
		/// FIFO of files so we know what to delete.
		/// </summary>
		Queue<string> Fifo = new Queue<string>();

		Dictionary<string, byte[]> Files = new Dictionary<string, byte[]>();

		private readonly object Lock = new object();

		/// <summary>
		/// File cache!
		/// </summary>
		/// <param name="maxSizeMB">Ensure the cache is never larger than this.</param>
		/// <param name="maxFileSizeMB">Do not cache files larger than this. -1 to cache all</param>
		public FileCache(string rootDirectory, long maxSizeMB = 100, long maxFileSizeMB = 1, bool caseSensitive = false)
		{
			CaseSensitive = caseSensitive;
			MaxCacheSize = maxSizeMB * 1024 * 1024;
			MaxCachableFileSize = maxFileSizeMB * 1024 * 1024;
			CacheSize = 0;

			if (!Directory.Exists(rootDirectory))
			{
				Console.WriteLine("'" + rootDirectory + "' not found, creating");
				Directory.CreateDirectory(rootDirectory);
			}
			FileSystemWatcher watcher = new FileSystemWatcher(rootDirectory);
			watcher.IncludeSubdirectories = true;
			watcher.Renamed += (sender, e) => { invalidateFile(e.OldFullPath); invalidateFile(e.FullPath); };
			watcher.Changed += (sender, e) => { invalidateFile(e.FullPath); };
			watcher.Deleted += (sender, e) => { invalidateFile(e.FullPath); };
			watcher.Created += (sender, e) => { invalidateFile(e.FullPath); };
			watcher.Error += (sender, e) => { dumpCache(); };
			watcher.EnableRaisingEvents = true;
		}

		void dumpCache()
		{
			lock (Lock)
			{
				Files.Clear();
				Fifo.Clear();
				CacheSize = 0;
			}
		}

		void invalidateFile(string filename)
		{
			if (!CaseSensitive) filename = filename.ToUpper();

			lock (Lock)
			{
				byte[] contents;
				if (Files.TryGetValue(filename, out contents))
				{
					Console.WriteLine("Uncaching {0}", filename);
					Files.Remove(filename);

					//Remove from the queue -- we have to construct a new queue...
					var newFifo = new Queue<string>(Fifo.Count + 10);
					while (Fifo.Count > 0)
					{
						var item = Fifo.Dequeue();
						if (item != filename)
							newFifo.Enqueue(item);
					}
					Fifo = newFifo;

					CacheSize -= contents.LongLength;
				}
			}
		}

		private bool isCachable(string fullPath)
		{
			FileInfo info = new FileInfo(fullPath);
			return info.Exists && info.Length <= MaxCachableFileSize;
		}

		private void uncacheOld()
		{
			//We're already locked.
			while (CacheSize > MaxCacheSize)
			{
				string toRemove = Fifo.Dequeue();
				long size = Files[toRemove].LongLength;
				Files.Remove(toRemove);
				CacheSize -= size;
			}
		}

		public byte[] GetFile(string filename)
		{
			if (!CaseSensitive) filename = filename.ToUpper();

			byte[] contents;
			while (true)
			{
				lock (Lock)
				{
					if (Files.TryGetValue(filename, out contents))
					{
						if (contents != null)
							return contents;
					}
					else
					{
						//We need to load
						if (isCachable(filename))
							Files.Add(filename, null);
						break;
					}
				}
				//Waiting on other thread...
				Thread.Sleep(10);
			}

			//Loading...
			contents = File.ReadAllBytes(filename);
			if (contents.LongLength <= MaxCachableFileSize)
			{
				lock (Lock)
				{
					Console.WriteLine("Caching {0}", filename);
					Files[filename] = contents;
					Fifo.Enqueue(filename);
					CacheSize += contents.LongLength;
					uncacheOld();
				}
			}
			return contents;
		}
	}
}
