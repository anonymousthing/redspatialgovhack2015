﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServer
{
	public class FileAccess
	{
		FileCache cache;

		public FileAccess(string rootDirectory)
		{
			cache = new FileCache(rootDirectory);
		}

		public Stream GetFileStream(string filename)
		{
			byte[] file = cache.GetFile(filename);
			Stream stream = new MemoryStream(file);
			return stream;
		}

		public byte[] GetFile(string filename)
		{
			return cache.GetFile(filename);
		}
	}
}
