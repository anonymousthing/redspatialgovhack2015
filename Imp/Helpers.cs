﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;

namespace GovHack.Imp
{
	public static class Statistics
	{
		public static double StandardDeviation(this IEnumerable<int> col)
		{
			var average = col.Average();
			var deviations = col.Select(i => (i - average).Sqr());
			var variance = deviations.Average();
			return Math.Sqrt(variance);
		}

		public static double StandardDeviation(this IEnumerable<float> col)
		{
			var average = col.Average();
			var deviations = col.Select(i => (i - average).Sqr());
			var variance = deviations.Average();
			return Math.Sqrt(variance);
		}

		public static double StandardDeviation(this IEnumerable<double> col)
		{
			var average = col.Average();
			var deviations = col.Select(i => (i - average).Sqr());
			var variance = deviations.Average();
			return Math.Sqrt(variance);
		}

		public static Tuple<float, float> MinMax(this IEnumerable<float> col)
		{
			float min = float.MaxValue;
			float max = float.MinValue;

			foreach (float f in col)
			{
				if (f > max)
					max = f;
				if (f < min)
					min = f;
			}

			return new Tuple<float, float>(min, max);
		}
	}

	public class ColourInterpolator
	{
		static readonly Color[] baseColors = new Color[] { Color.Red, Color.Orange, Color.Yellow, Color.Green };

		public static string InterpolateColorString(float ratio)
		{
			Color color = InterpolateColor(ratio);
			return ColorTranslator.ToHtml(color);
		}

		public static Color InterpolateColor(float ratio)
		{
			//return InterpolateColor(ratio, baseColors[0], baseColors[baseColors.Length - 1]);
			return Interpolate(ratio, baseColors);
		}

		static Color Interpolate(float factor, Color[] colours)
		{
			int length = colours.Length;
			int baseIndex = (int)(factor * length);

			Color a = colours[baseIndex];

			if (baseIndex == colours.Length - 1 || ((factor * length) - baseIndex) < 0.00001f)
				return a;

			Color b = colours[baseIndex + 1];

			//Now work out the factor between a & b.
			float baseFactor = baseIndex / (float)length;
			factor = (factor - baseFactor) * length;
			float invFactor = 1 - factor;

			//Now interpolate
			int red = (int)(factor * a.R + invFactor * b.R);
			int green = (int)(factor * a.G + invFactor * b.G);
			int blue = (int)(factor * a.B + invFactor * b.B);
			return Color.FromArgb(red, green, blue);
		}

		public static Color InterpolateColor(float ratio, Color a, Color b)
		{
			float inv = 1.0f - ratio;

			return Color.FromArgb((int)(inv * a.R + ratio * b.R), (int)(inv * a.G + ratio * b.G), (int)(inv * a.B + ratio * b.B));
		}
	}
}
