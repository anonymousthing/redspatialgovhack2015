﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Server = WebServer.WebServer;

namespace GovHack.Imp
{
	public partial class Form1 : Form
	{
		Server webserver;

		public Form1(Server webserver)
		{
			InitializeComponent();
			this.webserver = webserver;
		}

		private void btnImportData_Click(object sender, EventArgs e)
		{
			new GovHack.Database.ImportManager().ShowDialog();
		}
	}
}
