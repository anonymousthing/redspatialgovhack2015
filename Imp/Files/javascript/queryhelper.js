//className and methodName are strings, params is a javascript object.
var QueryServer = function(callback, className, methodName, params) {
	if (!params) params = {};
	var url = '/api/' + className + '/' + methodName + '/?';
	for (key in params) {
		if (params.hasOwnProperty(key)) {
			url += key + '=' + params[key] + '&';
		}
	}
	if (url.slice(-1) == '&') {
		url = url.slice(0, -1);
	}
	
	var req = new XMLHttpRequest();
	req.open('GET', url);
	req.onload = function(e) {
		callback(JSON.parse(req.responseText));
	}
    req.onerror = function(e) {
        console.log("Error making API call: " + e.error);
    }
	req.send();
}

