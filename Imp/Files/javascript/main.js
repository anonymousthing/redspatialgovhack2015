String.prototype.startsWith = function (str) {
	return this.indexOf(str) == 0;
};

$(document).ready(function () {
	var sidePanelChartOptions = {
		scaleShowGridLines: true,
		scaleGridLineColor: 'rgba(0, 0, 0, 0.05)',
		legendTemplate: '<ul>'
                  + '<% for (var i=0; i<datasets.length; i++) { %>'
                    + '<li>'
                    + '<span style=\"background-color:<%=datasets[i].lineColor%>\"></span>'
                    + '<% if (datasets[i].label) { %><%= datasets[i].label %><% } %>'
                  + '</li>'
                + '<% } %>'
              + '</ul>'
	};

	var sidePanel = $('#mainRight');
	var mainLeft = $('#mainLeft');

	var entityTitle = $('#entityTitle');
	var entitySubtitle = $('#entitySubtitle');
	var entityDescription = $('#entityDescription');
	var entityChartData = $('#entityChartData');

	var allEntityData = {
		//A lot of the data here will be retrieved from C#, so naming convention is UpperCamelCase
	};

	//ControlData - We store all state in a 'ControlData' variable to allow multiple maps on 1 screen.
	//	map		L.Map		Our leaflet map.
	//
	var controlData;

	/******************** FIND MASTER STUFF ***********************/

	function initialise() {
		createMap($('.map')[0]);
		initialiseEventHandlers();
		//getAllEntityData();
		//var data = QueryServer(createLineChart, 'TestRequestHandler', 'GetTemperatureData');
	};

	function initialiseEventHandlers() {
		$('#mainRight #closeBtn').click(function (e) {
			hideSidePanel();
		});

		$('#mainRight #viewBtn').click(function (e) {
			openBigView();
		});

		$('#fltEconDivirsity').click(function (e) {
		    alert('Economic Diversity');
		    requestNewData('EconomicDivirsityIndex');
		});

		$('#fltBusinessActivity').click(function (e) {
		    alert('Business Activity');
		    requestNewData('BusinessActivityIndex');
		});

		$('#fltIncome').click(function (e) {
		    alert('Income');
		    requestNewData('IncomeIndex');
		});

		$('#fltHealth').click(function (e) {
		    alert('Health');
		    requestNewData('HealthIndex');
		});

		$('#fltEducation').click(function (e) {
		    alert('Education');
		    requestNewData('EducationIndex');
		});

		$('#fltCrime').click(function (e) {
		    alert('Crime');
		    requestNewData('CrimeIndex');
		});

		$('#fltLivingIndex').click(function (e) {
		    alert('Living');
		    requestNewData('LivingIndex');
		});

		$(document).keyup(function (e) {
			if (e.keyCode == 27) {
				if (sidePanel.hasClass('bigView'))
					closeBigView();
				else if (sidePanel.hasClass('visible'))
					hideSidePanel();
			}
		});

		controlData.map.on('moveend', function () { requestNewData.call(controlData, controlData.lastRequestedDataset) });
		controlData.map.on('dblclick', function () { alert(controlData.map.getZoom()); });
	};

	function getAllEntityData() {
		var callback = function (data) {
			//data is a list of entityData's. Need to reprocess into id -> entityData dict
			allEntityData = { };

			for (var key in data.entities) {
				var entityData = data.entities[key];
				allEntityData[entityData.Id] = entityData;
				var marker = L.marker([-27.4696 + ((Math.random() - 0.5) * 0.01), 153.0252 + ((Math.random() - 0.5) * 0.01)]);
				marker.entityId = entityData.Id;
				marker.on('click', function (e) {
					var entityData = allEntityData[e.target.entityId];
					if (entityData) {
						featureClicked(entityData);
						controlData.map.panTo(e.latlng);
					}
				});
				marker.addTo(controlData.map);
			}
		}

		QueryServer(callback, 'TestRequestHandler', 'GetEntityData');
	}

	/*********************** FIND MAP STUFF ********************************/

	function createMap(mapDiv) {
		var map = new L.Map(mapDiv, {
			center: [-27.465, 153.025],
			zoom: 4,
			minZoom: 4,
            maxBounds: L.latLngBounds(L.latLng(-55, 110), L.latLng(-5, 165)),
		    doubleClickZoom: false
		});

		controlData = {
			map: map
		};
		
		var url = 'http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png'; //  'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
		var server = new L.TileLayer(url, {
			attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors, &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
		});

		server.addTo(map);
		requestNewData.call(controlData, 'BusinessActivityIndex');
	}

	function featureClicked(entityData) {
		showSidePanel();
		populateSidePanelData(entityData);
	}

	function convertToLatLon(serverPoint) {
		return new L.LatLng(serverPoint.Y, serverPoint.X);
	}

	function convertToLatLons(serverPoints) {
		var converted = [];
		for (var i = 0; i < serverPoints.length; i++) {
			converted.push(convertToLatLon(serverPoints[i]));
		}
		return converted;
	}

	function convertToMultiLatLons(serverMultiPoints) {
	    var converted = [];
	    for (var i = 0; i < serverMultiPoints.length; i++) {
	    	converted.push(convertToLatLons(serverMultiPoints[i]));
	    }
	    return converted;
	}

		function processData(data) {
		if (controlData.layers) {
			for (var i = 0; i < controlData.layers.length; i++) {
				controlData.map.removeLayer(controlData.layers[i]);
			}
		}
		var layers = [];
		var geomCount = 0;
		for (var i in data.Layers) {
			var dataLayer = data.Layers[i];

			var layer = new L.FeatureGroup();

			for (var j = 0; j < dataLayer.Geometries.length; j++) {
				var geometry = dataLayer.Geometries[j];
				geomCount++;
				var points = geometry.Points ? convertToLatLons(geometry.Points) : null;
				var multipoints = geometry.MultiPoints ? convertToMultiLatLons(geometry.MultiPoints) : null;
				var colour = (!geometry.Colour || geometry.Colour == '') ? dataLayer.Colour : geometry.Colour;

				var polylineOptions = {
					fillColor: colour,
					fillOpacity: 0.2,
                    colour: 'black',
                    weight: 2
				};
				
				var feature;

				switch (geometry.Type) {
					case 'Polygon':
						feature = L.polygon(points, polylineOptions);
						break;
				    case 'MultiPolygon':
				    	feature = L.multiPolygon(multipoints, polylineOptions);
				        break;
				}

				feature.dataset = controlData.dataset;
				feature.regionId = geometry.EntityId;
				feature.indexValue = geometry.IndexValue;
				feature.on('click', function (e) {
					var callback = function (entityData) {
						featureClicked(entityData);
						controlData.map.panTo(e.target.getBounds().getCenter());
					}

					QueryServer(callback, 'DetailAPI', 'GetRegionInfo', { 'dataset': e.target.dataset, 'regionId': e.target.regionId })
				});
				layer.addLayer(feature);
			}

			layers.push(layer);
		}

		layers.forEach(function (layer) { layer.addTo(controlData.map); }, controlData);
		controlData.layers = layers;
		//alert(geomCount);
	}

	function padBounds(bounds, ratio) {
	    ratio = (1 + 2 * (ratio - 1));
	    var latDiff = ((bounds.getNorth() - bounds.getSouth()) / 2) * ratio;
	    var lonDiff = ((bounds.getEast() - bounds.getWest()) / 2) * ratio;
	    var center = bounds.getCenter();

	    var east = center.lng - lonDiff;
	    var west = center.lng + lonDiff;
	    var north = center.lat - latDiff;
	    var south = center.lat + latDiff;

	    return L.latLngBounds(L.latLng(south, west), L.latLng(north, east));
	}

	function requestNewData(dataset) {
		var map = controlData.map;
		controlData.dataset = dataset;

		var bounds = map.getBounds();
		if (controlData.lastRequestedDataset === dataset &&
            controlData.lastRequestedZoom === map.getZoom() &&
            controlData.lastRequestedBounds && controlData.lastRequestedBounds.contains(bounds)) {
		    return;
		}

		bounds = padBounds(bounds, 1.2);

		var request = {
            Dataset: dataset,
			North: bounds.getNorth(),
			South: bounds.getSouth(),
			East: bounds.getEast(),
			West: bounds.getWest(),
			Zoom: map.getZoom()
		};

		controlData.lastRequestedDataset = dataset;
		controlData.lastRequestedBounds = bounds;
		controlData.lastRequestedZoom = map.getZoom();

		$.ajaxq('mapmapmampamapmapmapmapmampa');
		$.ajaxq('mapmapmampamapmapmapmapmampa', {
			method: 'POST',
			url: '/api/TestAPI/GetIndexGeometries/',
			data: JSON.stringify(request),
			dataType: 'json',
			contentType: 'application/json',
			context: controlData,
			success: function (data) {
			    processData(data);
			},
			error: function (a, b, c) {
				//alert('error');
			}
		});
	}

	/******************* FIND CHART STUFF ***********************/

	function populateSidePanelData(entityData) {
		entityTitle.html(entityData.Names.Name);
		if (entityData.Names.Description) entitySubtitle.html(entityData.Names.Description);

		var innerHtml = '';
		
		var datasets = entityData.Charts;
		for (var i in datasets) {
			var dataset = datasets[i];
			var id = 'Chart' + dataset;
			innerHtml += '<canvas id="' + id.split(' ').join('') + '" class="entityDataChart"></canvas>';
		}

		entityChartData.html(innerHtml);
		for (var i in datasets) {
			var dataset = datasets[i];
			createLineChart(dataset, entityData.Names.RegionId);
		}
	};

	function createLineChart(id, entityId) {
		//Create blank area for now, with a spinner
		//Turn it into a div, spin.js can't draw on top of canvas
		var chartName = "Chart" + id.split(' ').join('');

		$('#' + chartName).replaceWith($('<div id="' + chartName + '" class="entityDataChart"></div>'));
		$('#' + chartName).spin();

		//Query server for data and fill in the chart data on the callback
		QueryServer(function (data) {
			//Stop and remove the spinner
			var canvas = $('#' + chartName);
			canvas.spin(false);
			canvas.replaceWith($('<canvas id="' + chartName + '" class="entityDataChart"></canvas>'));

			if (!data.title) data.title = '';
			//Need to re-get the jquery obj
			$('#' + chartName).before('<h2 class="chartTitle">' + data.title + '</h2>');

			var context = $('#' + chartName)[0].getContext('2d');
			var chart = new Chart(context).Line(data, sidePanelChartOptions);
			$('#' + chartName).after('<div id="' + chartName + "Legend" + '" class="legend"></div>')

			legend($('#' + chartName + "Legend")[0], data);
		}, "DetailAPI", "GetRegionData", {'dataset': id, 'regionId': entityId});
	};

	/************************ FIND PANEL STUFF ************************/

	function toggleSidePanel() {
		if (sidePanel.css('display') == 'none') {
			showSidePanel();
		} else {
			hideSidePanel();
		}
	};

	function showSidePanel() {
		if (!sidePanel.hasClass('visible')) {
			sidePanel.addClass('visible');
			sidePanel.show();
			$(window).trigger('resize');
			mainLeft.css('width', '70%');
			controlData.map.invalidateSize();
		}
	};

	function hideSidePanel() {
		if (sidePanel.hasClass('visible') && !sidePanel.hasClass('bigView')) {
			sidePanel.removeClass('visible');
			mainLeft.css('width', '100%');
			sidePanel.hide();
			controlData.map.invalidateSize();
		} else if (sidePanel.hasClass('bigView')) {
			closeBigView();
		}
	};

	function openBigView() {
		sidePanel.css('width', '100%');
		sidePanel.css('border-left', '');
		sidePanel.addClass('bigView');
		$('#viewBtn').hide();
		mainLeft.hide();

		//Show more content and reflow
	};

	function closeBigView() {
		sidePanel.css('width', '30%');
		sidePanel.css('border-left', '2px solid grey');
		sidePanel.removeClass('bigView');
		$('#viewBtn').show();
		mainLeft.show();

		//Show only summary content
	}

	initialise();
});