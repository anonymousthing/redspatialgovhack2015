﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GovHack.Database;

using Common;
using System.Threading;
using WebServer;

namespace GovHack.Imp
{
	public class ChartJsDataset
	{
		public string label;
		public string fillColor = "rgba(255, 192, 0, 0.2)";
		public string strokeColor = "rgba(220, 220, 200, 1)";
		public string pointColor = "rgba(220, 220, 220, 1)";
		public string pointStrokeColor = "#fff";
		public string pointHighlightFill = "#fff";
		public string pointHighlightStroke = "rgba(220,220,220,1)";
		public float[] data;

		public ChartJsDataset(float[] data)
		{
			this.data = data;
		}

		public ChartJsDataset(int[] data)
		{
			this.data = data.Select(i => (float)i).ToArray();
		}
	}

	public class TestRequestHandler
	{
		static FileCache fileCache = new FileCache(Path.Combine(Environment.CurrentDirectory, "Content"));

		[RequestHandler]
		public object GetChartTemp(Request request)
		{
			ChartJsDataset dataset1 = new ChartJsDataset(new[] { 0.2528638f, 0.22522282f, 0.230616f, 0.2254224f, 0.2128843f });
			ChartJsDataset dataset2 = new ChartJsDataset(new[] { 0.0843077f, 0.08272731f, 0.07548813f, 0.07169881f, 0.06263368f });
			return new {
				title = "Economic Diversity Index",
				labels = new [] { 
					"2008", "2009", "2010", "2011", "2012"
				},
				datasets = new[] {
					dataset1,
					dataset2
				}
			};
		}

		[RequestHandler]
		public object GetChartEcon(Request request)
		{
			if (request.QueryParameters["entityId"].StartsWith("4"))
			{
				ChartJsDataset dataset1 = new ChartJsDataset(new[] { 28, 64, 12, 38, 12, 1877, 29 });
				return new
				{
					labels = new[] { 
						"Microsoft", "Apple", "Google", "IBM", "Sony", "Recursive Red", "Samsung"
					},
					datasets = new[] {
						dataset1
					}
				};
			}
			else
			{
				ChartJsDataset dataset1 = new ChartJsDataset(new[] { 15, 12, 17, 29 });
				return new
				{
					labels = new[] { 
						"Apples", "Oranges", "Pears", "Strawberries"
					},
					datasets = new[] {
						dataset1
					}
				};
			}
		}

		[RequestHandler]
		public object GetEntityData(Request request)
		{
			var context = Db.GetContext();

			var entities = context.Entities.ToArray();

			return new { entities };

			//return new
			//{
			//	e1 = new
			//	{
			//		Id = "e1",
			//		Name = "House1",
			//		Subtitle = "A house in the middle of Queen St Mall, apparently.",
			//		Data_Temp = (object)null,
			//		Data_Econ = (object)null
			//	},
			//	e2 = new
			//	{
			//		Id = "e2",
			//		Name = "House2",
			//		Subtitle = "Another house in the middle of Queen St Mall.",
			//		Data_Temp = (object)null,
			//		Data_Econ = (object)null
			//	},
			//};
		}
	}
}
