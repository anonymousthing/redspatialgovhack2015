﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GovHack.Database;
using System.IO;

namespace GovHack.Imp.Data
{
	class BusinessActivityImporter : DataImporter
	{
		const string BusinessActivityCsv = @"..\..\..\Resources\National Regional Profile, Economy, ASGS, 2008-2012.csv";
		const string BusinessActivityTable = "BusinessActivity";

		public BusinessActivityImporter()
		{
			TruncateBeforeInsert = true;
			AffectedTables = new string[] { BusinessActivityTable };
			Name = "Business Activity Importer";
		}

		public override bool CheckRequirements()
		{
			return File.Exists(BusinessActivityCsv);
		}

		protected override void GetTableDefinition(string table, ICreateTable builder)
		{
			builder
				.CreateColumn("RegionId", "int")
					.PrimaryKey()
				.CreateColumn("Year", "int")
					.PrimaryKey()
				.CreateColumn("BusinessCount", "int")
				.CreateColumn("EntryBusinessCount", "int")
				.CreateColumn("ExitBusinessCount", "int")
				.CreateColumn("BuildingValueMillions", "real")
				.CreateColumn("IndexValue", "real")
					.Nullable();
		}

		public override void Execute()
		{
			var db = Db.GetContext();
			var list = new List<BusinessActivity>();
			var mapping = new Dictionary<string, Population>();

			string[] econ = File.ReadAllLines(BusinessActivityCsv);

			foreach (var row in econ.Skip(1))
			{
				string[] parts = row.Split(',');
				//141 columns
				if (parts.Length != 141) continue;

				int id, year, businessCount, entryCount, exitCount;
				float buildingValueMillions;

				if (!int.TryParse(parts[0], out id)) continue;
				if (!int.TryParse(parts[2], out year)) continue;
				if (!int.TryParse(parts[5], out businessCount)) continue;
				if (!int.TryParse(parts[10], out entryCount)) continue;
				if (!int.TryParse(parts[14], out exitCount)) continue;
				if (!float.TryParse(parts[139], out buildingValueMillions)) continue;

				db.BusinessActivities.InsertOnSubmit(new BusinessActivity
				{
					RegionId = id,
					Year = year,
					BusinessCount = businessCount,
					EntryBusinessCount = entryCount,
					ExitBusinessCount = exitCount,
					BuildingValueMillions = buildingValueMillions
				});
			}

			db.SubmitChanges();

			var query = (
				from businessActivity in db.BusinessActivities
				join popInfo in db.Populations on new { businessActivity.RegionId, businessActivity.Year } equals new { popInfo.RegionId, popInfo.Year }
				select new { BusinessActivity = businessActivity, popInfo });
			
			foreach (var item in query)
			{
				var pop = item.popInfo.TotalPopulation;

				if (pop < 200) continue;

				double netBusinessCount = item.BusinessActivity.EntryBusinessCount - item.BusinessActivity.ExitBusinessCount;
				double totalBusinessMovement = item.BusinessActivity.EntryBusinessCount + item.BusinessActivity.ExitBusinessCount;
				double construction = item.BusinessActivity.BuildingValueMillions;
				double businessCount = item.BusinessActivity.BusinessCount;
				double density = item.popInfo.PopulationDensity;

				netBusinessCount /= pop;
				totalBusinessMovement /= pop;
				construction /= pop;
				businessCount /= pop;

				item.BusinessActivity.IndexValue = (float)(
					netBusinessCount * 1.8 +
					totalBusinessMovement * 1 +
					construction  * 1.4 +
					businessCount * 0.5 +
					density / 10000);
			}

			db.SubmitChanges();

			float min = float.MaxValue, max = float.MinValue;
			foreach (var item in db.BusinessActivities.Where(item => item.IndexValue.HasValue))
			{
				float value = item.IndexValue.Value;

				min = value < min ? value : min;
				max = value > max ? value : max;
			}

			float range = max - min;
			float scale = 1 / range;

			foreach (var item in db.BusinessActivities.Where(item => item.IndexValue.HasValue))
				item.IndexValue = (item.IndexValue.Value - min) * scale;

			db.SubmitChanges(); 
		}
	}
}
