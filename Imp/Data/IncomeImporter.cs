﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GovHack.Database;
using System.IO;

namespace GovHack.Imp.Data
{
	class IncomeImporter : DataImporter
	{
		const string EconomyCsv = @"..\..\..\Resources\National Regional Profile, Economy, ASGS, 2008-2012.csv";
		const string IncomeTable = "Incomes";

		public IncomeImporter()
		{
			TruncateBeforeInsert = true;
			AffectedTables = new string[] { IncomeTable }; 
			Name = "Income Importer";
		}

		public override bool CheckRequirements()
		{
			return File.Exists(EconomyCsv);
		}

		protected override void GetTableDefinition(string table, ICreateTable builder)
		{
			builder
				.CreateColumn("RegionId", "int")
					.PrimaryKey()
				.CreateColumn("Year", "int")
					.PrimaryKey()
				.CreateColumn("IndexValue", "real").Nullable();
		}

		public override void Execute()
		{
			var db = Db.GetContext();
			var list = new List<Income>();

			var lines = File.ReadAllLines(EconomyCsv);

			foreach (string row in lines)
			{
				string[] parts = row.Split(',');
				if (parts.Length != 141)
				{
					continue;
				}

				int id;
				if (!int.TryParse(parts[0], out id))
				{
					continue;
				}

				int year = int.Parse(parts[2]);
				int income;
				if (!int.TryParse(parts[63], out income))
				{
					//No income data
					continue;
				}

				list.Add(new Income
				{
					RegionId = id,
					Year = year,
					IndexValue = income
				});
			}

			var minMax = list.Where(i => i.IndexValue.HasValue).Select(i => i.IndexValue.Value).MinMax();
			var min = minMax.Item1;
			var max = minMax.Item2 - minMax.Item1;
			var scale = 1 / max;

			foreach (var item in list)
			{
				item.IndexValue = (item.IndexValue - min) * scale;
			}

			db.Incomes.InsertAllOnSubmit(list);
			db.SubmitChanges();
		}
	}
}
