﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Common;

using GovHack.Database;

namespace GovHack.Imp.Data
{
	/// <summary>
	/// Calculates (from imported data) or retrieves (from existing tables) the master index list.
	/// </summary>
	class IndexImporter : DataImporter
	{
		const string IndexPath = @"..\..\..\Resources\index.csv";

		const string CompiledIndices = "Index_All";

		//Migration
		//Population Density
		//Qualification

		public IndexImporter()
		{
			AffectedTables = new string[] { 
                CompiledIndices, "Years"
            };
			TruncateBeforeInsert = false;
			Name = "Index Importer";
		}

		public override bool CheckRequirements()
		{
			//return System.IO.File.Exists(IndexPath);
			return true;
		}

        protected override void GetTableDefinition(string table, ICreateTable builder)
        {
			switch (table)
			{
			case "Years":
				builder
					.CreateColumn("Year", "int")
						.PrimaryKey();
				break;
			case CompiledIndices:
				builder
					.CreateColumn("RegionId", "int")
						.PrimaryKey()
					.CreateColumn("Year", "int")
						.PrimaryKey()
					.CreateColumn("EconomicDiversityIndex", "real").Nullable()
					.CreateColumn("BusinessActivityIndex", "real").Nullable()
					.CreateColumn("IncomeIndex", "real").Nullable()
					.CreateColumn("HealthIndex", "real").Nullable()
					.CreateColumn("EducationIndex", "real").Nullable()
					.CreateColumn("KnowledgeEconomyIndex", "real").Nullable()
					.CreateColumn("LivabilityIndex", "real").Nullable()
					.CreateColumn("MigrationIndex", "real").Nullable()
					.CreateColumn("PopulationDensityIndex", "real").Nullable()
					.CreateColumn("QualificationIndex", "real").Nullable()
					.CreateColumn("CrimeIndex", "real").Nullable();
				break;
			default:
				break;
			}
        }

		public override void Execute()
		{
			var db = Db.GetContext();
			
			db.Years.InsertOnSubmit(new Year { Year1 = 2008 });
			db.Years.InsertOnSubmit(new Year { Year1 = 2009 });
			db.Years.InsertOnSubmit(new Year { Year1 = 2010 });
			db.Years.InsertOnSubmit(new Year { Year1 = 2011 });
			db.Years.InsertOnSubmit(new Year { Year1 = 2012 });

			db.SubmitChanges();

			var query = (
				from region in db.Regions
				from year in db.Years
				join _economicData in db.EconomicDiversities on new { RegionId = region.Id, Year = year.Year1 } equals new { RegionId = _economicData.RegionId, Year = _economicData.Year } into j0
				from economicData in j0.DefaultIfEmpty()
				join _businessData in db.BusinessActivities on new { RegionId = region.Id, Year = year.Year1 } equals new { RegionId = _businessData.RegionId, Year = _businessData.Year } into j1
				from businessData in j1.DefaultIfEmpty()
				join _incomeData in db.Incomes on new { RegionId = region.Id, Year = year.Year1 } equals new { RegionId = _incomeData.RegionId, Year = _incomeData.Year } into j2
				from incomeData in j2.DefaultIfEmpty()
				join _popData in db.Populations on new { RegionId = region.Id, Year = year.Year1 } equals new { RegionId = _popData.RegionId, Year = _popData.Year } into j3
				from popData in j3.DefaultIfEmpty()
				join _educData in db.Qualifications on new { RegionId = region.Id, Year = year.Year1 } equals new { RegionId = _educData.RegionId, Year = _educData.Year } into j4
				from educData in j4.DefaultIfEmpty()
				select new {
					RegionId = region.Id,
					Year = year,
					EconomicIndex = economicData.IndexValue,
					BusinessIndex = businessData.IndexValue,
					IncomeIndex = incomeData.IndexValue,
					EducationIndex = educData.EducationIndex
				});

            List<Index_All> indexEntries = new List<Index_All>();


			foreach (var row in query)
			{
                indexEntries.Add(new Index_All
                {
                    RegionId = row.RegionId,
                    Year = row.Year.Year1,
                    EconomicDiversityIndex = row.EconomicIndex,
                    IncomeIndex = row.IncomeIndex,
                    BusinessActivityIndex = row.BusinessIndex,
					EducationIndex = row.EducationIndex
                });
			}

            CalculateLivabilityIndex(indexEntries);

            db.Index_Alls.InsertAllOnSubmit(indexEntries);
            db.SubmitChanges();
		}

        static Dictionary<Func<Index_All, float?>,double> weightings = new Dictionary<Func<Index_All,float?>,double>()
        {
            { (a => a.BusinessActivityIndex), 1.0 },
            { (a => a.EconomicDiversityIndex), 1.0 },
            { (a => a.EducationIndex), 1.0 },
            { (a => a.IncomeIndex), 1.0 },
            { (a => a.KnowledgeEconomyIndex), 1.0 },
            { (a => a.MigrationIndex), 1.0 },
            { (a => a.PopulationDensityIndex), 1.0 },
            { (a => a.QualificationIndex), 1.0 }
        };

        void CalculateLivabilityIndex(List<Index_All> entries)
        {
            foreach (var region in entries.GroupBy(e => e.RegionId))
            {
                List<Index_All> livabilityScores = region.ToList();

                double sumWeights = 0.0;
                double sumValue = 0.0;
                foreach (Index_All item in livabilityScores)
                {
                    foreach (var weighting in weightings)
                    {
                        var accessor = weighting.Key;
                        double weight = weighting.Value;
                        float? value = ValueOrAny(item, livabilityScores, accessor);
                        if (value != null)
                        {
                            sumWeights += weighting.Value;
                            sumValue += (float)(value.Value * weight);
                        }
                    }

                    item.LivabilityIndex = (float)((sumValue == 0.0) ? 0.0 : sumValue / sumWeights);
                }
            }
        }

        float? ValueOrAny(Index_All value, List<Index_All> other, Func<Index_All, float?> accessor)
        {
            float? result = accessor(value);
            if (result.HasValue)
            {
                return result.Value;
            }
            return other.Select(v => accessor(v)).Where(v => v != null).FirstOrDefault();
        }
	}
}
