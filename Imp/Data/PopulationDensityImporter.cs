﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GovHack.Database;

namespace GovHack.Imp.Data
{
	public class PopulationDensityImporter : DataImporter
	{
		const string PopulationCsv = @"..\..\..\Resources\National Regional Profile, Population, ASGS, 2008-2012.csv";

		const int
			ColId = 0,
			ColYear = 2,
			ColTotalMales = 30,
			ColTotalFemales = 49,
			ColTotalPop = 68,
			ColPopDensity = 75,
			ColQualPostGrad = 89,
			ColQualGradDip = 90,
			ColQualBachelor = 91,
			ColQualAdvDip = 92,
			ColQualCert = 93,
			ColMigration = 130;

		const string PopulationInfoTable = "Population";
		const string EducationTable = "Qualifications";

		public PopulationDensityImporter()
		{
			TruncateBeforeInsert = true;
			Name = "Population Counts & Education";
			AffectedTables = new[] { PopulationInfoTable, EducationTable };
		}

		public override bool CheckRequirements()
		{
			return File.Exists(PopulationCsv);
		}

		protected override void GetTableDefinition(string table, ICreateTable builder)
		{
			switch (table)
			{
			case PopulationInfoTable:
				builder
					.CreateColumn("RegionId", "int")
						.PrimaryKey()
					.CreateColumn("Year", "int")
						.PrimaryKey()
					.CreateColumn("PopulationDensity", "real")
					.CreateColumn("TotalPopulation", "real")
					.CreateColumn("TotalMales", "real")
					.CreateColumn("TotalFemales", "real")
					.CreateColumn("InternalMigrationPercent", "real");
				break;
			case EducationTable:
				builder
					.CreateColumn("RegionId", "int")
						.PrimaryKey()
					.CreateColumn("Year", "int")
						.PrimaryKey()
					.CreateColumn("PostGraduatePercent", "real")
					.CreateColumn("GraduateDiplomaOrCertPercent", "real")
					.CreateColumn("BachelorDegreePercent", "real")
					.CreateColumn("AdvancedDiplomaPercent", "real")
					.CreateColumn("CertificatePercent", "real")
					.CreateColumn("EducationIndex", "real")
						.Nullable();
				break;
			default:
				break;
			}
		}

		float parseFloat(string input)
		{
			if (input == "-") return 0;

			return float.Parse(input);
		}

		public override void Execute()
		{
			var lines = File.ReadAllLines(PopulationCsv);

			var db = Db.GetContext();
            string[] columnHeaders = lines[0].Split(',');


			foreach (var line in lines.Skip(1))
			{
				var columns = line.Split(',');

				if (columns.Length != 141)
					continue;

				int region;
				if (!int.TryParse(columns[ColId], out region)) continue;
				int year = int.Parse(columns[ColYear]);
				db.Populations.InsertOnSubmit(new Population
				{
					RegionId = region,
					Year = year,
					TotalPopulation = parseFloat(columns[ColTotalPop]),
					TotalMales = parseFloat(columns[ColTotalMales]),
					TotalFemales = parseFloat(columns[ColTotalFemales]),
					PopulationDensity = parseFloat(columns[ColPopDensity]),
					InternalMigrationPercent = parseFloat(columns[ColMigration])
				});
				if (year == 2011)
				{
					db.Qualifications.InsertOnSubmit(new Qualification
					{
						RegionId = region,
						Year = year,
						AdvancedDiplomaPercent = parseFloat(columns[ColQualAdvDip]),
						BachelorDegreePercent = parseFloat(columns[ColQualBachelor]),
						CertificatePercent = parseFloat(columns[ColQualAdvDip]),
						GraduateDiplomaOrCertPercent = parseFloat(columns[ColQualGradDip]),
						PostGraduatePercent = parseFloat(columns[ColQualPostGrad])
					});
				}
			}

			db.SubmitChanges();

			var query =
				from rowWithDensity in db.Populations
				where rowWithDensity.Year == 2012 && rowWithDensity.PopulationDensity != 0
				join rowWithoutDensity in db.Populations on rowWithDensity.RegionId equals rowWithoutDensity.RegionId
				where rowWithoutDensity.Year != 2012
				select new { Area = rowWithDensity.TotalPopulation / rowWithDensity.PopulationDensity, Row = rowWithoutDensity };

			foreach (var item in query)
				item.Row.PopulationDensity = item.Row.TotalPopulation / item.Area;
			
			db.SubmitChanges();
		}
	}
}
