﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;

using GovHack.Database;

namespace GovHack.Imp.Data
{
	class Entities : DataImporter
	{
		const string EntitiesTable = "Entities";

		public Entities()
		{
			TruncateBeforeInsert = true;
			AffectedTables = new[] { EntitiesTable };
			Name = "Entity Data";
		}

		public override bool CheckRequirements()
		{
			return true;
		}

		protected override void GetTableDefinition(string table, ICreateTable builder)
		{
			builder
				.CreateColumn("Id", "int")
					.PrimaryKey()
				.CreateColumn("Name", "varchar(128)")
				.CreateColumn("Subtitle", "varchar(MAX)")
				.CreateColumn("DatasetNames", "varchar(MAX)");
		}

		public override void Execute()
		{
			var context = Db.GetContext();
			var entities = context.Entities;

			for (int i = 0; i < 50; i++)
			{
				int id = RandomUtil.Next(9999);
				entities.InsertOnSubmit(new Entity
				{
					Id = id,
					Name = "House " + i.ToString(),
					Subtitle = "A house in meshblock " + RandomUtil.Next(99999999).ToString().PadLeft(8, '0'),
					DatasetNames = "Temp,Econ"
				});
			}

			context.SubmitChanges();
		}
	}
}
