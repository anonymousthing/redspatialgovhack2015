﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Common;
using GovHack.Database;

namespace GovHack.Imp.Data
{
	public class AreaOutlines : DataImporter
	{
		const string AreaTable = "Areas";

		public AreaOutlines()
		{
			TruncateBeforeInsert = true;
			AffectedTables = new[] { AreaTable };
			Name = "Statistical area outlines";
		}

		const string MidFile = @"..\..\..\Resources\SD11aAust.mid";
		const string MifFile = @"..\..\..\Resources\SD11aAust.mif";

		public override bool CheckRequirements()
		{
			return File.Exists(MidFile) && File.Exists(MifFile);
		}

		protected override void GetTableDefinition(string table, ICreateTable builder)
		{
			switch (table)
			{
			case AreaTable:
				builder
					.CreateColumn("Id", "int")
						.AutoIncrement()
						.PrimaryKey()
					.CreateColumn("State", "varchar(10)")
					.CreateColumn("Code", "int")
					.CreateColumn("Name", "varchar(100)")
					.GeometryColumn("Area");
				break;
			default:
				break;
			}
		}

		public override void Execute()
		{
			var db = Db.GetContext();

			var States = new[] { null, "NSW", "VIC", "QLD", "SA", "WA", "TAS", "NT", "ACT", "OTHER" };

			var midLines =
				File.ReadAllLines(MidFile)
					.Select(line => line.Split(','))
					.Select(parts => new { State = States[int.Parse(parts[0])], Code = int.Parse(parts[1]), Name = parts[2] });

			var mifLines = File.ReadAllLines(MifFile);

			var geometries = new List<List<List<Point>>>();
			var currentSet = new List<List<Point>>();
			for (int i = 10; i < mifLines.Length; )
			{
				int count;
				if (!int.TryParse(mifLines[i++], out count))
				{
					if (currentSet != null)
					{
						geometries.Add(currentSet);
						currentSet = null;
					}
					continue;
				}
				currentSet = currentSet ?? new List<List<Point>>();

				var points = new List<Point>();
				for (int j = 0; j < count; j++)
				{
					var line = mifLines[i++];
					var parts = line.Split(' ');
					points.Add(new Point { X = double.Parse(parts[0]), Y = double.Parse(parts[1]) });
				}
				currentSet.Add(points);
			}

			int removed = 0, removed1 = 0, removed2 = 0;
			//We have all our geometries.
			var Areas = db.Areas;
			midLines.ForEachWith(geometries, (info, geoms) =>
			{
				foreach (List<Point> points in geoms)
				{
					var geometry = new Geometry
					{
						Type = GeometryType.Polygon,
						Points = SpatialHelper.SimplifyPolygon(points.ToArray(), SpatialHelper.Epsilon0).ToList()
					};
					var simple1 = new Geometry
					{
						Type = GeometryType.Polygon,
						Points = SpatialHelper.SimplifyPolygon(points.ToArray(), SpatialHelper.Epsilon1).ToList()
					};
					var simple2 = new Geometry
					{
						Type = GeometryType.Polygon,
						Points = SpatialHelper.SimplifyPolygon(points.ToArray(), SpatialHelper.Epsilon2).ToList()
					};
					removed += points.Count - geometry.Points.Count;
					removed1 += points.Count - simple1.Points.Count;
					removed2 += points.Count - simple2.Points.Count;

					if (geometry.Points.Count < simple1.Points.Count)
						simple1.Points = geometry.Points;
					if (simple1.Points.Count < simple2.Points.Count)
						simple2.Points = simple1.Points;

					Areas.InsertOnSubmit(new Area
					{
						State = info.State,
						Code = info.Code,
						Name = info.Name,
						Geometry = geometry.ToWkt(),
						Area_simple1 = simple1.ToWkt(),
						Area_simple2 = simple2.ToWkt()
					});
				}
			});

			db.SubmitChanges();

			SpatialHelper.UpdateForInserts(db, SpatialColumn.Area.Areas);

			System.Windows.Forms.MessageBox.Show(string.Concat("Removed ", removed, ", ", removed1, ", ", removed2));
		}
	}
}
