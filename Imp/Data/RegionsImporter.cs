﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

using GovHack.Database;

namespace GovHack.Imp.Data
{
    class RegionsImporter : DataImporter
    {
        const string SA2DescriptionFile = @"..\..\..\Resources\SA2_2011_AUST.mid";
        const string SA2OutlineFile = @"..\..\..\Resources\SA2_2011_AUST.mif";
        const string SA3DescriptionFile = @"..\..\..\Resources\SA3_2011_AUST.mid";
        const string SA3OutlineFile = @"..\..\..\Resources\SA3_2011_AUST.mif";
        const string SA4DescriptionFile = @"..\..\..\Resources\SA4_2011_AUST.mid";
        const string SA4OutlineFile = @"..\..\..\Resources\SA4_2011_AUST.mif";

        const string SA2 = "Region_SA2";
        const string SA3 = "Region_SA3";
        const string SA4 = "Region_SA4";
		const string Gccsa = "Region_GGCSA";
		const string State = "Region_State";
		const string Regions = "Regions";


        public RegionsImporter()
        {
            TruncateBeforeInsert = true;
            AffectedTables = new[] { SA2, SA3, SA4, Gccsa, State, Regions };
            Name = "ABS SA2/SA3/SA4 Region Outlines";
        }

        public override bool CheckRequirements()
        {
            return File.Exists(SA2DescriptionFile) && File.Exists(SA2OutlineFile);
        }

        protected override void GetTableDefinition(string table, ICreateTable builder)
        {
			switch (table)
			{
			case SA2:
				builder
					.CreateColumn("Id", "int")
						.PrimaryKey()
					.CreateColumn("FiveDigitCode", "int")
					.CreateColumn("Name", "varchar(100)")
					.CreateColumn("SA3Id", "int")
					.CreateColumn("SA4Id", "int")
					.CreateColumn("GccsaId", "int")
					.CreateColumn("StateId", "int")
					.GeometryColumn("Area");
				break;
			case SA3:
				builder
					.CreateColumn("Id", "int")
						.PrimaryKey()
					.CreateColumn("Name", "varchar(100)")
					.CreateColumn("SA4Id", "int")
					.CreateColumn("GccsaId", "int")
					.CreateColumn("StateId", "int")
					.GeometryColumn("Area");
				break;
			case SA4:
				builder
					.CreateColumn("Id", "int")
						.PrimaryKey()
					.CreateColumn("Name", "varchar(100)")
					.CreateColumn("GccsaId", "int")
					.CreateColumn("StateId", "int")
					.GeometryColumn("Area");
				break;
			case Gccsa:
				builder
					.CreateColumn("Id", "int")
						.PrimaryKey()
					.CreateColumn("Name", "varchar(100)")
					.CreateColumn("Code", "varchar(5)")
					.CreateColumn("StateId", "int");
				break;
			case State:
				builder
					.CreateColumn("Id", "int")
						.PrimaryKey()
					.CreateColumn("Name", "varchar(100)");
				break;
			case Regions:
				builder
					.CreateColumn("Id", "int")
						.PrimaryKey()
					.CreateColumn("Name", "varchar(100)");
				break;
			}
        }

        private void ReadFiles()
        {
            // Maps the five-character GCCSA code to our own integer ID.
            Dictionary<string, int> gccsaIdsByCode = new Dictionary<string,int>();

            


        }

        private List<Region_SA2> ReadSA2s(out Dictionary<string, Region_GGCSA> gccsasByCode, out Dictionary<int, Region_State> statesById)
        {
            List<string[]> geometries = MifParser.Parse(SA2OutlineFile);
            gccsasByCode = new Dictionary<string, Region_GGCSA>();
            statesById = new Dictionary<int, Region_State>();

            var SA2Regions = File.ReadAllLines(SA2DescriptionFile);
            if (geometries.Count != SA2Regions.Length)
            {
                throw new ArgumentOutOfRangeException("Data inconsistent: not as many geometries as regions.");
            }

            List<Region_SA2> sa2s = new List<Region_SA2>();
            for (int i = 0; i < SA2Regions.Length; i++)
            {
                // Do not import regions without geometry attached.
				if (geometries[i][0] == null)
                    continue;

                string[] parts = SA2Regions[i].Split(',');

                string gccsaCode = parts[7];
                int gccsaId = 0;
                int stateId = int.Parse(parts[9]);

                if (!statesById.ContainsKey(stateId))
                {
                    statesById[stateId] = new Region_State() { Id = stateId, Name = parts[10] };
                }


                if (!gccsasByCode.ContainsKey(gccsaCode))
                {
                    // Allocate an ID for the gccsa code.
                    gccsasByCode[gccsaCode] = new Region_GGCSA() { Id = gccsasByCode.Count, Code = gccsaCode, Name = parts[8], StateId = stateId };
                }
                gccsaId = gccsasByCode[gccsaCode].Id;

                Region_SA2 sa2 = new Region_SA2()
                {
                    Id = int.Parse(parts[0]),
                    FiveDigitCode = int.Parse(parts[1]),
                    Name = parts[2],
                    SA3Id = int.Parse(parts[3]),
                    SA4Id = int.Parse(parts[5]),
                    GccsaId = gccsaId,
                    StateId = stateId,
					Area = geometries[i][0],
					Area_simple1 = geometries[i][1],
					Area_simple2 = geometries[i][2],
					Area_simple3 = geometries[i][3]
                };
                sa2s.Add(sa2);
            }
            return sa2s;
        }

        private List<Region_SA3> ReadSA3s(Dictionary<string, Region_GGCSA> gccsaIdsByCode)
        {
            List<string[]> geometries = MifParser.Parse(SA3OutlineFile);

            List<Region_SA3> sa3s = new List<Region_SA3>();
            var SA3Regions = File.ReadAllLines(SA3DescriptionFile);


            if (geometries.Count != SA3Regions.Length)
            {
                throw new ArgumentOutOfRangeException("Data inconsistent: not as many geometries as regions.");
            }


            for (int i = 0; i < SA3Regions.Length; i++)
            {
                // Do not import regions without geometry attached.                
                if (geometries[i][0] == null)
                    continue;


                string[] parts = SA3Regions[i].Split(',');

                Region_SA3 sa3 = new Region_SA3()
                {
                    Id = int.Parse(parts[0]),
                    Name = parts[1],
                    SA4Id = int.Parse(parts[2]), 
                    GccsaId = gccsaIdsByCode[parts[4]].Id,
					StateId = int.Parse(parts[6]),
					Area = geometries[i][0],
					Area_simple1 = geometries[i][1],
					Area_simple2 = geometries[i][2],
					Area_simple3 = geometries[i][3]
                };
                sa3s.Add(sa3);
            }
            return sa3s;
        }

        private List<Region_SA4> ReadSA4s(Dictionary<string, Region_GGCSA> gccsaIdsByCode)
        {
            List<string[]> geometries = MifParser.Parse(SA4OutlineFile);

            List<Region_SA4> sa4s = new List<Region_SA4>();
            var SA4Regions = File.ReadAllLines(SA4DescriptionFile);


            if (geometries.Count != SA4Regions.Length)
            {
                throw new ArgumentOutOfRangeException("Data inconsistent: not as many geometries as regions.");
            }


            for (int i = 0; i < SA4Regions.Length; i++)
            {
                // Do not import regions without geometry attached.
				if (geometries[i][0] == null)
                    continue;

                string[] parts = SA4Regions[i].Split(',');

                Region_SA4 sa4 = new Region_SA4()
                {
                    Id = int.Parse(parts[0]),
                    Name = parts[1],
                    GccsaId = gccsaIdsByCode[parts[2]].Id,
					StateId = int.Parse(parts[4]),
					Area = geometries[i][0],
					Area_simple1 = geometries[i][1],
					Area_simple2 = geometries[i][2],
					Area_simple3 = geometries[i][3]
                };
                sa4s.Add(sa4);
            }
            return sa4s;
        }



        public override void Execute()
        {
            Dictionary<string, Region_GGCSA> gccsasByCode;
            Dictionary<int, Region_State> statesById;
            var sa2s = ReadSA2s(out gccsasByCode, out statesById);
            var sa3s = ReadSA3s(gccsasByCode);
            var sa4s = ReadSA4s(gccsasByCode);

            var db = Db.GetContext();
            db.Region_States.InsertAllOnSubmit(statesById.Values);
            db.Region_GGCSAs.InsertAllOnSubmit(gccsasByCode.Values);
            db.Region_SA2s.InsertAllOnSubmit(sa2s);
            db.Region_SA3s.InsertAllOnSubmit(sa3s);
            db.Region_SA4s.InsertAllOnSubmit(sa4s);

            db.SubmitChanges();

			foreach (var item in db.Region_SA2s)
				db.Regions.InsertOnSubmit(new Region { Id = item.Id, Name = item.Name });
			foreach (var item in db.Region_SA3s)
				db.Regions.InsertOnSubmit(new Region { Id = item.Id, Name = item.Name });
			foreach (var item in db.Region_SA4s)
				db.Regions.InsertOnSubmit(new Region { Id = item.Id, Name = item.Name });
			foreach (var item in db.Region_States)
				db.Regions.InsertOnSubmit(new Region { Id = item.Id, Name = item.Name });

			db.SubmitChanges();

            SpatialHelper.UpdateForInserts(db, SpatialColumn.Region_SA2.Area);
            SpatialHelper.UpdateForInserts(db, SpatialColumn.Region_SA3.Area);
            SpatialHelper.UpdateForInserts(db, SpatialColumn.Region_SA4.Area);

        }
    }
}
