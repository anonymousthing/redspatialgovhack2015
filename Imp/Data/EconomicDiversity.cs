﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

using GovHack.Database;
using System.IO;

namespace GovHack.Imp.Data
{
	class EconomicDiversityImporter : DataImporter
	{
		const string EconomicDiversityCsv = @"..\..\..\Resources\National Regional Profile, Economy, ASGS, 2008-2012.csv";
		const string EconomicDiversityTable = "EconomicDiversity";

		public EconomicDiversityImporter()
		{
			TruncateBeforeInsert = true;
			Name = "Economic Diversity";
			AffectedTables = new string[] { EconomicDiversityTable };
		}


		public override bool CheckRequirements()
		{
			return File.Exists(EconomicDiversityCsv);
		}

		protected override void GetTableDefinition(string table, ICreateTable builder)
		{
			builder
				.CreateColumn("RegionId", "int")
					.PrimaryKey()
				.CreateColumn("Year", "int")
					.PrimaryKey()
				.CreateColumn("IndexValue", "real").Nullable();
		}

		public override void Execute()
		{
			var list = new List<EconomicDiversity>();
			var context = Db.GetContext();
			var EconomicDiv = context.EconomicDiversities;

			string[] lines = File.ReadAllLines(EconomicDiversityCsv);

			int badRows = 0;

			foreach (string row in lines.Skip(1))
			{
				string[] parts = row.Split(',');
				//141 columns
				if (parts.Length != 141)
				{
					badRows++;
					continue;
				}

				int id;
				if (!int.TryParse(parts[0], out id))
				{
					badRows++;
					continue;
				}

				var regionName = parts[1];
				int year = int.Parse(parts[2]);

				int businessCount;
				if (!int.TryParse(parts[6], out businessCount))
				{
					badRows++;
					continue;
				}

				//Columns 3-23 are "Number of business by industry"
				var industryCounts = parts.Skip(15).Take(19);
				if (industryCounts.Contains("-"))
				{
					badRows++;
					continue;
				}
				var parsedIndustryCounts = industryCounts.Select(s => int.Parse(s));

				//int diff = 0;
				//parsedIndustryCounts.PairWithPrevious().ForEach(t => diff += Math.Abs(t.Item1 - t.Item2));

				//float indexValue = diff / (float)businessCount;

				float indexValue = (float)parsedIndustryCounts.StandardDeviation() / businessCount;

				list.Add(new EconomicDiversity
				{
					RegionId = id,
					Year = year,
					IndexValue = indexValue
				});
			}

			var minMax = list.Where(i => i.IndexValue.HasValue).Select(i => i.IndexValue.Value).MinMax();
			var min = minMax.Item1;
			var max = minMax.Item2 - minMax.Item1;
			var scale = 1 / max;


			foreach (var item in list)
			{
				item.IndexValue = (item.IndexValue - min) * scale;
			}

			EconomicDiv.InsertAllOnSubmit(list);
			context.SubmitChanges();
		}
	}
}
