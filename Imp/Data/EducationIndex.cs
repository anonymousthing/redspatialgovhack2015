﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using GovHack.Database;

namespace GovHack.Imp.Data
{
	public class EducationIndex : DataImporter
	{
		public EducationIndex()
		{
			Name = "EducationIndex";
			AffectedTables = new[] { "" };
		}

		public override bool CheckRequirements()
		{
			return true;
		}

		protected override void GetTableDefinition(string table, ICreateTable builder)
		{
		}

		public override void Execute()
		{
			var db = Db.GetContext();

			float max = float.MinValue, min = float.MaxValue;
			foreach (var qual in db.Qualifications)
			{
				var index = qual.EducationIndex = 
					(qual.PostGraduatePercent / 100) * 4 +
					(qual.BachelorDegreePercent / 100) * 2.75f +
					(qual.GraduateDiplomaOrCertPercent / 100) * 4 +
					(qual.AdvancedDiplomaPercent / 100) * 1.4f +
					(qual.CertificatePercent / 100) * 0.5f;

				max = index.Value > max ? index.Value : max;
				min = index.Value < min ? index.Value : min;
			}

			float range = max - min;
			float scale = 1 / range;

			foreach (var item in db.BusinessActivities.Where(item => item.IndexValue.HasValue))
				item.IndexValue = (item.IndexValue.Value - min) * scale;

			foreach (var qual in db.Qualifications)
				qual.EducationIndex = (qual.EducationIndex.Value - min) * scale;

			db.SubmitChanges();
		}
	}
}
