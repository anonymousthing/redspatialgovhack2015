﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Common;

using WebServer;
using GovHack.Database;

namespace GovHack.Imp
{
	public class TestAPI
	{
		[RequestHandler]
		public object JSONInput(Request request)
		{
			return request.GetFormData<MapRequest>();
		}

		public class Row
		{
			public string State;
			public int Code;
			public string Name;
			public string WKT;
		}

		public class RowResult
		{
			public Row[] Rows;
		}

		[RequestHandler]
		public object GetTenRows(Request request)
		{
			var context = Db.GetContext();

			//You can use .Skip() and .Take() to control row-numbers.
			//This is done on the SQL Server. (ie works)
			var areas = context.Areas.Take(10)
							.Select(area => new Row {
								State = area.State,
								Code = area.Code,
								Name = area.Name,
								WKT = area.Geometry
							})
							.ToArray();

			//When using LINQ style, if we want to row-limit we still need skip/take.
			//This is done on the SQL Server. (ie works)
			areas =  (from area in context.Areas
						select new Row {
							State = area.State,
							Code = area.Code,
							Name = area.Name,
							WKT = area.Geometry
						}).Take(10).ToArray();

			return new RowResult { Rows = areas };
		}

		[RequestHandler]
		public object GetTenGeometries(Request request)
		{
			var db = Db.GetContext();

			var items = db.Areas.Take(10);
			List<string> wkts = new List<string>();
			wkts.AddRange(items.Select(item => item.Geometry));
			var geoms = wkts.Select(wkt => WktConverter.FromWkt(wkt)).ToList();

			var map = new Map {
				Layers = new List<MapLayer> {
					new MapLayer {
						Id = "default",
						Name = "Test",
						Colour = "blue",
						Geometries = geoms
					}
				}
			};

			return map;;
		}

		//All of the below are complete as far as these handlers go. GetIndexGeometriesSA4_BusinessActivityIndex uses its own table for testing.
		private IQueryable<Geometry> GetIndexGeometriesSA4_LivingIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA4s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.LivabilityIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.LivabilityIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA4_CrimeIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA4s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.CrimeIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.CrimeIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA4_EducationIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA4s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2011 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.EducationIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.EducationIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA4_HealthIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA4s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.HealthIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.HealthIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA4_IncomeIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA4s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2011 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.IncomeIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.IncomeIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA4_EconomicDivirsityIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA4s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.EconomicDiversityIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.EconomicDiversityIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA4_BusinessActivityIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA4s
				   where idList.Contains(area.Id)
				   join businessAct in db.BusinessActivities on new { RegionId = area.Id, Year = 2012 } equals new { businessAct.RegionId, businessAct.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
						   businessAct.IndexValue.HasValue ? ColourInterpolator.InterpolateColorString(businessAct.IndexValue.Value) : null);
		}


		private IQueryable<Geometry> GetIndexGeometriesSA3_LivingIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA3s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.LivabilityIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.LivabilityIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA3_CrimeIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA3s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.CrimeIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.CrimeIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA3_EducationIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA3s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2011 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.EducationIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.EducationIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA3_HealthIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA3s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.HealthIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.HealthIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA3_IncomeIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA3s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2011 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.IncomeIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.IncomeIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA3_EconomicDivirsityIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA3s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.EconomicDiversityIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.EconomicDiversityIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA3_BusinessActivityIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA3s
				   where idList.Contains(area.Id)
				   join businessAct in db.BusinessActivities on new { RegionId = area.Id, Year = 2012 } equals new { businessAct.RegionId, businessAct.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
						   businessAct.IndexValue.HasValue ? ColourInterpolator.InterpolateColorString(businessAct.IndexValue.Value) : null);
		}


		private IQueryable<Geometry> GetIndexGeometriesSA2_LivingIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA2s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.LivabilityIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.LivabilityIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA2_CrimeIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA2s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.CrimeIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.CrimeIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA2_EducationIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA2s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2011 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.EducationIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.EducationIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA2_HealthIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA2s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.HealthIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.HealthIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA2_IncomeIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA2s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2011 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.IncomeIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.IncomeIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA2_EconomicDivirsityIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA2s
				   where idList.Contains(area.Id)
				   join indices in db.Index_Alls on new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
							indices.EconomicDiversityIndex.HasValue ? ColourInterpolator.InterpolateColorString(indices.EconomicDiversityIndex.Value) : null);
		}
		private IQueryable<Geometry> GetIndexGeometriesSA2_BusinessActivityIndex(GovHackDataContext db, List<int> idList, int zoom)
		{
			return from area in db.Region_SA2s
				   where idList.Contains(area.Id)
				   join businessAct in db.BusinessActivities on new { RegionId = area.Id, Year = 2012 } equals new { businessAct.RegionId, businessAct.Year }
				   select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area, zoom), area.Id,
						   businessAct.IndexValue.HasValue ? ColourInterpolator.InterpolateColorString(businessAct.IndexValue.Value) : null);
		}

		[RequestHandler]
		public object GetIndexGeometries(Request request)
		{
			var mapRequest = request.GetFormData<MapRequest>();
			int region = 3;
			if (mapRequest.Zoom > 11) region = 2;
			else if (mapRequest.Zoom > 8) region = 3;
			else region = 4;

			var db = Db.GetContext();

			SpatialColumn spatialColumn = null;
			if (region == 2) spatialColumn = SpatialColumn.Region_SA2.Area;
			else if (region == 3) spatialColumn = SpatialColumn.Region_SA3.Area;
			else if (region == 4) spatialColumn = SpatialColumn.Region_SA4.Area;

			var ids = SpatialHelper.FindIntersectingAtZoom(db, spatialColumn, mapRequest);
			var areas = new List<Geometry>();

			foreach (var list in ids)
			{
				IQueryable<Geometry> query = null;
				switch (mapRequest.Dataset)
				{
				case "EconomicDivirsityIndex":
					if (region == 2) query = GetIndexGeometriesSA2_EconomicDivirsityIndex(db, list, mapRequest.Zoom);
					else if (region == 3) query = GetIndexGeometriesSA3_EconomicDivirsityIndex(db, list, mapRequest.Zoom);
					else if (region == 4) query = GetIndexGeometriesSA4_EconomicDivirsityIndex(db, list, mapRequest.Zoom);
					break;
				case "BusinessActivityIndex":
					if (region == 2) query = GetIndexGeometriesSA2_BusinessActivityIndex(db, list, mapRequest.Zoom);
					else if (region == 3) query = GetIndexGeometriesSA3_BusinessActivityIndex(db, list, mapRequest.Zoom);
					else if (region == 4) query = GetIndexGeometriesSA4_BusinessActivityIndex(db, list, mapRequest.Zoom);
					break;
				case "IncomeIndex":
					if (region == 2) query = GetIndexGeometriesSA2_IncomeIndex(db, list, mapRequest.Zoom);
					else if (region == 3) query = GetIndexGeometriesSA3_IncomeIndex(db, list, mapRequest.Zoom);
					else if (region == 4) query = GetIndexGeometriesSA4_IncomeIndex(db, list, mapRequest.Zoom);
					break;
				case "HealthIndex":
					if (region == 2) query = GetIndexGeometriesSA2_HealthIndex(db, list, mapRequest.Zoom);
					else if (region == 3) query = GetIndexGeometriesSA3_HealthIndex(db, list, mapRequest.Zoom);
					else if (region == 4) query = GetIndexGeometriesSA4_HealthIndex(db, list, mapRequest.Zoom);
					break;
				case "EducationIndex":
					if (region == 2) query = GetIndexGeometriesSA2_EducationIndex(db, list, mapRequest.Zoom);
					else if (region == 3) query = GetIndexGeometriesSA3_EducationIndex(db, list, mapRequest.Zoom);
					else if (region == 4) query = GetIndexGeometriesSA4_EducationIndex(db, list, mapRequest.Zoom);
					break;
				case "CrimeIndex":
					if (region == 2) query = GetIndexGeometriesSA2_CrimeIndex(db, list, mapRequest.Zoom);
					else if (region == 3) query = GetIndexGeometriesSA3_CrimeIndex(db, list, mapRequest.Zoom);
					else if (region == 4) query = GetIndexGeometriesSA4_CrimeIndex(db, list, mapRequest.Zoom);
					break;
				case "LivingIndex":
					if (region == 2) query = GetIndexGeometriesSA2_LivingIndex(db, list, mapRequest.Zoom);
					else if (region == 3) query = GetIndexGeometriesSA3_LivingIndex(db, list, mapRequest.Zoom);
					else if (region == 4) query = GetIndexGeometriesSA4_LivingIndex(db, list, mapRequest.Zoom);
					break;
				default:
					return null;
				}
				areas.AddRange(query);
			}

			var map = new Map
			{
				Layers = new List<MapLayer> {
					new MapLayer {
						Id = "default",
						Name = "Test",
						Colour = "blue",
						Geometries = areas
					}
				}
			};
			return map;
		}

		[RequestHandler]
		public object GetIntersectingGeometries(Request request)
		{
			var bounds = request.GetFormData<MapRequest>();
			//Take this from request
			int year = 2012;

			var db = Db.GetContext();

			var ids = SpatialHelper.FindIntersectingAtZoom(db, SpatialColumn.Region_SA4.Area, bounds);
			var areas = new List<Geometry>();

			foreach (var list in ids)
			{
				var query =
					from area in db.Region_SA2s
					where list.Contains(area.Id)
					//join economicDivRow in db.EconomicDiversities on new { RegionId = area.Id, Year = year } equals new { economicDivRow.RegionId, economicDivRow.Year }
					select Geometry.FromWkt(SpatialHelper.GetGeometryForZoom(area.Area, area.Area_simple1, area.Area_simple2, area.Area_simple3, bounds.Zoom), area.Id, null);// ColourInterpolator.InterpolateColorString(economicDivRow.IndexValue));
				
				areas.AddRange(query);
			}

			var map = new Map
			{
				Layers = new List<MapLayer> {
					new MapLayer {
						Id = "default",
						Name = "Test",
						Colour = "blue",
						Geometries = areas
					}
				}
			};
			return map;
		}
	}
}
