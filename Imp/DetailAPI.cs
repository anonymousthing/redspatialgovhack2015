﻿using GovHack.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebServer;

using Common;

namespace GovHack.Imp
{
    public class RegionNames
    {
        public int RegionId;
        public string StateName;
        public string GccsaName;
        public string SA4Name;
        public string SA3Name;
        public string SA2Name;
        public string Name;
        public string Description;
    }

    public class RegionInfo
    {
        public RegionNames Names;
        public string[] Charts;
    }

    public class ChartSet
    {
        public ChartData[] Charts;
    }

    public class ChartData
    {
        public string name;
        public int[] labels;
        public ChartSeries[] datasets;
    }

    public class ChartSeries
    {
        public string label;
		public int RegionId;
		public string fillColor = "rgba(255, 192, 0, 0.0)";
		public string strokeColor = "rgba(" + RandomUtil.Next(255) + "," + RandomUtil.Next(255) + "," + RandomUtil.Next(255) + ",0.6)";
		public string pointColor = "rgba(96, 96, 96, 1)";
		public string pointStrokeColor = "#fff";
        public double[] data;        
    }

    public class DataPoint
    {
        public int RegionId;
        public int Year;
        public float? Value;
    }

    class DetailAPI
    {
        [RequestHandler]
        public object GetRegionInfo(Request request)
        {
            string dataset = request.QueryParameters["dataset"];
            int regionId = int.Parse(request.QueryParameters["regionId"]);


            var db = Db.GetContext();
            RegionNames regionInfo = new RegionNames() { RegionId = regionId };
            
            // SA4, GCCSA, State level data.
            PopulateSA4NameUpwards(db, regionInfo);
            PopulateSA3Name(db, regionInfo);
            PopulateSA2Name(db, regionInfo);

            if (regionInfo.SA4Name != null)
            {
                regionInfo.Name = regionInfo.SA4Name;
                regionInfo.Description = regionInfo.StateName;
            }
            if (regionInfo.SA3Name != null)
            {
                regionInfo.Name = regionInfo.SA3Name;
                regionInfo.Description = regionInfo.GccsaName;
            }
            if (regionInfo.SA2Name != null)
            {
                regionInfo.Name = regionInfo.SA2Name;
                regionInfo.Description = regionInfo.SA4Name;
            }

            return new RegionInfo { 
                 Names = regionInfo,
                 Charts = GetCharts(dataset)
            };
        }

        
          static Dictionary<string, ChartDataDelgate> Charts;

          static string[] GetCharts(string dataset)
          {
              switch (dataset)
              {
                  case "BusinessActivityIndex":
                      return new string[] { "Business Activity Index" };
                      
                  case "EconomicDivirsityIndex":
                      return new string[] { "Economic Diversity Index" };
                      
                  case "EducationIndex":
                      return new string[] { "Education Index" };

                  case "IncomeIndex":
                      return new string[] { "Income Index" };

                  case "KnowledgeEconomyIndex":
                      return new string[] { "Knowledge Economy Index" };

                  case "LivingIndex":
                      return new string[] { "Livability Index" };

                  case "MigrationIndex":
                      return new string[] { "Migration Index" };

                  case "PopulationDensityIndex":
                      return new string[] { "Population Density" };

              }
              return new string[] { };
          }

          static DetailAPI()
          {
              Charts = new Dictionary<string, ChartDataDelgate>();
              Charts.Add("Business Activity Index", GetBusinessActivityData);
              Charts.Add("Economic Diversity Index", GetEconomicDiversityData);
              Charts.Add("Education Index", GetEducationData);
              Charts.Add("Income Index", GetIncomeData);
              Charts.Add("Knowledge Economy Index", GetKnowledgeEconomyData);
              Charts.Add("Livability Index", GetLivabilityData);
              Charts.Add("Migration Index", GetMigrationData);
              Charts.Add("Population Density", GetPopulationDensityData);
          }

        private int? GetSA2(int regionid)
        {
            if (regionid <= 100000000)
            {
                return null;
            }
            return regionid;
        }

        private int? GetSA3(int regionId)
        {
            if (regionId <= 1000)
            {
                return null;
            }
            // Trim to first five digits.
            while (regionId > 100000)
            {
                regionId /= 10;
            }
            return regionId;
        }

        private int GetSA4(int regionId)
        {
            // Trim to first 3 digits.
            while (regionId > 1000)
            {
                regionId /= 10;
            }
            return regionId;
        }

        private int GetState(int regionId)
        {
            while (regionId >= 10)
            {
                regionId /= 10;
            }
            return regionId;
        }

        [RequestHandler]
        public object GetRegionData(Request request)
        {
            string dataset = request.QueryParameters["dataset"];
            int regionId = int.Parse(request.QueryParameters["regionId"]);

            var db = Db.GetContext();

            int? sa2 = GetSA2(regionId);
            int? sa3 = GetSA3(regionId);
            int sa4 = GetSA4(regionId);
            int state = GetState(regionId);

            var regionInfo = (RegionInfo)GetRegionInfo(request);
           
            Dictionary<int, string> legend = new Dictionary<int,string>() ;

            if (sa2 != null)
            {
                legend.Add(sa2.Value,  regionInfo.Names.SA2Name);
            }
            if (sa3 != null)
            {
                legend.Add(sa3.Value,  regionInfo.Names.SA3Name);
            }
            legend.Add(sa4,  regionInfo.Names.SA4Name);
            legend.Add(state,  regionInfo.Names.StateName);
            legend.Add(0, "Australia");

            //List<ChartData> result = new List<ChartData>();
            //foreach (string chartName in regionInfo.Charts)
            //{
                ChartDataDelgate chartData = Charts[dataset];
                ChartData resultChart = GetChartData(chartData(db, legend.Keys.ToList()).ToList(), legend);
                resultChart.name = dataset;
            //    result.Add(resultChart);
            //}

                return resultChart;// new ChartSet() { Charts = result.ToArray() };
        }

        public delegate IQueryable<DataPoint> ChartDataDelgate(GovHackDataContext db, List<int> regions);



        private static IQueryable<Index_All> GetIndexData(GovHackDataContext db, List<int> regions)
        {
            return (from index in db.Index_Alls
                    where regions.Contains(index.RegionId)
                    select index);
        }

        private static IQueryable<DataPoint> GetBusinessActivityData(GovHackDataContext db, List<int> regions)
        {
            var index = GetIndexData(db, regions).ToList();
            return index.Select(dp => new DataPoint() { Year = dp.Year, Value = dp.BusinessActivityIndex, RegionId = dp.RegionId }).AsQueryable();

        }

        private static IQueryable<DataPoint> GetEconomicDiversityData(GovHackDataContext db, List<int> regions)
        {
            return GetIndexData(db, regions).Select(dp => new DataPoint() { Year = dp.Year, Value = dp.EconomicDiversityIndex, RegionId = dp.RegionId });
        }

        private static IQueryable<DataPoint> GetEducationData(GovHackDataContext db, List<int> regions)
        {
            return GetIndexData(db, regions).Select(dp => new DataPoint() { Year = dp.Year, Value = dp.EducationIndex, RegionId = dp.RegionId });
        }

        private static IQueryable<DataPoint> GetIncomeData(GovHackDataContext db, List<int> regions)
        {
            return GetIndexData(db, regions).Select(dp => new DataPoint() { Year = dp.Year, Value = dp.IncomeIndex, RegionId = dp.RegionId });
        }

        private static IQueryable<DataPoint> GetKnowledgeEconomyData(GovHackDataContext db, List<int> regions)
        {
            return GetIndexData(db, regions).Select(dp => new DataPoint() { Year = dp.Year, Value = dp.KnowledgeEconomyIndex, RegionId = dp.RegionId });
        }

        private static IQueryable<DataPoint> GetLivabilityData(GovHackDataContext db, List<int> regions)
        {
            return GetIndexData(db, regions).Select(dp => new DataPoint() { Year = dp.Year, Value = dp.LivabilityIndex, RegionId = dp.RegionId });
        }

        private static IQueryable<DataPoint> GetMigrationData(GovHackDataContext db, List<int> regions)
        {
            return GetIndexData(db, regions).Select(dp => new DataPoint() { Year = dp.Year, Value = dp.MigrationIndex, RegionId = dp.RegionId });
        }

        private static IQueryable<DataPoint> GetPopulationDensityData(GovHackDataContext db, List<int> regions)
        {
            return GetIndexData(db, regions).Select(dp => new DataPoint() { Year = dp.Year, Value = dp.PopulationDensityIndex, RegionId = dp.RegionId });
        }

        private static ChartData GetEconomicDiversityData(GovHackDataContext db, Dictionary<int, string> legend)
        {
            List<int> regionIds = legend.Keys.ToList();
            var list = (from businessActivity in db.BusinessActivities
                    where regionIds.Contains(businessActivity.RegionId) && businessActivity.IndexValue != null
                    select new DataPoint { RegionId = businessActivity.RegionId, Value = businessActivity.IndexValue.Value, Year = businessActivity.Year })
                    .ToList();

            ChartData result = GetChartData(list, legend);
            result.name = "Economic Diversity Index";
            return result;
        }

        private static ChartData GetChartData(List<DataPoint> list, Dictionary<int, string> legend)
        {
            list = list.Where(v => v.Value != null).ToList();

            List<int> actualYears = list.GroupBy(l => l.Year).Select(l => l.Key).ToList();

            int maxYears = 0;
            foreach (var series in list.GroupBy(i => i.RegionId))
            {
                maxYears = Math.Max(maxYears, series.Count());
            }

            var seriesToUse = list.GroupBy(i => i.RegionId).Where(g => g.Count() > (maxYears / 2)).ToList();

            foreach (var series in seriesToUse)
            {
                for (int year = actualYears.Count - 1; year >= 0; year--)
                {
                    if (!series.Any(dp => dp.Year == actualYears[year]))
                    {
                        actualYears.RemoveAt(year);
                    }
                }
            }

            ChartData data = new ChartData();

            List<ChartSeries> chartSeries = new List<ChartSeries>();
            foreach (var series in seriesToUse)
            {
                ChartSeries s = new ChartSeries() { label = legend[series.Key] };

                double[] values = new double[actualYears.Count];
                foreach (var point in series)
                {
                    if (actualYears.Contains(point.Year))
                    {
                        values[actualYears.IndexOf(point.Year)] = point.Value.Value;
                    }
                }
                s.data = values;
                s.RegionId = series.Key;
                chartSeries.Add(s);
            }

            data.datasets = chartSeries.OrderByDescending(c => c.RegionId).ToArray();
            data.labels = actualYears.ToArray();
            return data;
        }

        private void PopulateSA4NameUpwards(GovHackDataContext db, RegionNames info)
        {
            int regionId = info.RegionId;

            // Trim to first 3 digits.
            while (regionId > 1000)
            {
                regionId /= 10;
            }
            var details = (from sa4 in db.Region_SA4s
                            where sa4.Id == regionId
                            join gccsa in db.Region_GGCSAs on sa4.GccsaId equals gccsa.Id
                            join state in db.Region_States on sa4.StateId equals state.Id
                            select new { SA4Name = sa4.Name, GccsaName = gccsa.Name, StateName = state.Name }).FirstOrDefault();
            if (details != null)
            {
                info.SA4Name = details.SA4Name;
                info.GccsaName = details.GccsaName;
                info.StateName = details.StateName;
            }


            // new { RegionId = area.Id, Year = 2012 } equals new { indices.RegionId, indices.Year }
                           // where sa4.Id == regionId select new { sa4.Name }).FirstOrDefault();
        }

        private void PopulateSA3Name(GovHackDataContext db, RegionNames info)
        {
            int regionId = info.RegionId;
            if (regionId <= 1000)
            {
                return;
            }
            // Trim to first five digits.
            while (regionId > 100000)
            {
                regionId /= 10;
            }
            info.SA3Name = (from sa3 in db.Region_SA3s where sa3.Id == regionId select sa3.Name).FirstOrDefault();
        }

        private void PopulateSA2Name(GovHackDataContext db, RegionNames info)
        {
            int regionId = info.RegionId;
            if (regionId <= 100000000)
            {
                return;
            }
            info.SA2Name = (from sa2 in db.Region_SA2s where sa2.Id == regionId select sa2.Name).FirstOrDefault();
        }
        
    }
}
