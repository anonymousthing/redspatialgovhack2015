﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;

using Common;

namespace WebServer
{
	public class Program
	{
		public static void Main(params string[] args)
		{
			Start(args);
		}

		public static WebServer Start(params string[] args)
		{
			WebServer webServer = null;

			try
			{
				webServer = WebServer.FromArguments(args);
				if (webServer == null) throw new Exception("Cannot parse arguments");

				Thread thread = new Thread(x => webServer.Run());
				thread.Start();
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				Console.WriteLine(ex.StackTrace);
			}

			return webServer;
		}

		static int x = 0;

		class TestResponse
		{
			public int Value = 5;
		}

		[RequestHandler]
		public object Test(Request request)
		{
			int value = System.Threading.Interlocked.Increment(ref x);
			//System.Threading.Thread.Sleep(10000);
			System.Threading.Interlocked.Decrement(ref x);
			return new TestResponse
			{
				Value = 4
			};
		}
	}
}
