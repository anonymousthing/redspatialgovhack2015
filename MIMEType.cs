﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebServer
{
	public static class MIMEType
	{

		public static string FromExtension(string extension)
		{
			string value;
			if (mimeTypes.TryGetValue(extension.ToLower(), out value))
				return value;

			return "application/octet-stream";
		}

		static Dictionary<string, string> mimeTypes = new Dictionary<string, string> {
			{ "asm", "text/plain" },
			{ "avi", "video/avi" },
			{ "bin", "application/octet-stream" },
			{ "bmp", "image/bmp" },
			{ "bz", "application/x-bzip" },
			{ "bz2", "application/x-bzip2" },
			{ "c", "text/plain" },
			{ "c++", "text/plain" },
			{ "cc", "text/plain" },
			{ "class", "application/java" },
			{ "cpp", "text/plain" },
			{ "css", "text/css; charset=utf-8" },
			{ "dll", "application/octet-stream" },
			{ "exe", "application/octet-stream" },
			{ "gif", "image/gif" },
			{ "h", "text/plain" },
			{ "htm", "text/html" },
			{ "html", "text/html; charset=utf-8" },
			{ "java", "text/plain" },
			{ "jpeg", "image/jpeg" },
			{ "jpg", "image/jpeg" },
			{ "js", "application/javascript" },
			{ "mid", "audio/x-midi" },
			{ "midi", "audio/x-midi" },
			{ "mkv", "video/x-matroska" },
			{ "mov", "video/quicktime" },
			{ "mp2", "audio/mpeg" },
			{ "mp3", "audio/mpeg3" },
			{ "mp4", "video/mp4" },
			{ "mpe", "video/mpeg" },
			{ "mpeg", "video/mpeg" },
			{ "mpg", "video/mpeg" },
			{ "mpga", "audio/mpeg" },
			{ "pdf", "application/pdf" },
			{ "png", "image/png" },
			{ "ppt", "application/powerpoint" },
			{ "pptx", "application/powerpoint" },
			{ "rtf", "text/richtext" },
			{ "shtml", "text/html" },
			{ "swf", "application/x-shockwave-flash" },
			{ "tar", "application/x-tar" },
			{ "tgz", "application/x-compressed" },
			{ "tif", "image/tiff" },
			{ "tiff", "image/tiff" },
			{ "txt", "text/plain" },
			{ "wav", "audio/wav" },
			{ "xls", "application/excel" },
			{ "xlsx", "application/excel" },
			{ "zip", "application/zip" }
		};
	}
}
