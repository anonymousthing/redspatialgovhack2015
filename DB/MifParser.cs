﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Polygon = System.Collections.Generic.List<GovHack.Database.Point>;
using MultiPolygon = System.Collections.Generic.List<System.Collections.Generic.List<GovHack.Database.Point>>;

namespace GovHack.Database
{
	public class MifParser
	{
		public static List<string[]> Parse(string path)
		{
			var mifLines = File.ReadAllLines(path);

			var geometries = new List<MultiPolygon>();

			// Skip over the column definition data.
			int startIndex = Array.FindIndex(mifLines, line => line.StartsWith("REGION"));
			for (int i = startIndex; i < mifLines.Length; )
			{
				string regionLine = mifLines[i++];
				if (regionLine.StartsWith("BRUSH"))
					continue;

				int regionCount = 0;
				if (!regionLine.StartsWith("NONE"))
				{
					regionCount = int.Parse(regionLine.Substring("REGION ".Length));
				}

				MultiPolygon currentSet = new MultiPolygon();
				for (int r = 0; r < regionCount; r++)
				{
					int pointCount;
					string pointCountLine = mifLines[i++];
					if (!int.TryParse(pointCountLine, out pointCount))
						continue;

					var points = new Polygon();
					for (int j = 0; j < pointCount; j++)
					{
						var line = mifLines[i++];
						var parts = line.Split(' ');
						points.Add(new Point { X = double.Parse(parts[0]), Y = double.Parse(parts[1]) });
					}
					currentSet.Add(points);
				}

				geometries.Add(currentSet);
			}

			List<string[]> results = new List<string[]>(geometries.Count);

			foreach (var multipolygon in geometries)
			{
				Point[][][] points = new Point[4][][];
				for (int i = 0; i < 4; i++)
					points[i] = new Point[multipolygon.Count][];

				for (int i = 0; i < multipolygon.Count; i++)
				{
					multipolygon[i].Reverse();
					var itemArray = multipolygon[i].ToArray();
					Point[] empty = new Point[0];
					points[0][i] = SpatialHelper.SimplifyPolygon(itemArray, SpatialHelper.Epsilon0) ?? empty;
					points[1][i] = SpatialHelper.SimplifyPolygon(itemArray, SpatialHelper.Epsilon1) ?? empty;
					points[2][i] = SpatialHelper.SimplifyPolygon(itemArray, SpatialHelper.Epsilon2) ?? empty;
					points[3][i] = SpatialHelper.SimplifyPolygon(itemArray, SpatialHelper.Epsilon3) ?? empty;

					if (points[1][i].Length > points[0][i].Length)
						points[1][i] = points[0][i];
					if (points[2][i].Length > points[1][i].Length)
						points[2][i] = points[1][i];
					if (points[3][i].Length > points[2][i].Length)
						points[3][i] = points[2][i];
				}

				var dummyPolygon = new[] {
					new[] {
						new Point(59.5, -175),
						new Point(59, -175.5),
						new Point(59, -175),
						new Point(59.5, -175)
					}
				};

				if (multipolygon.Count > 1)
				{
					for (int i = 1; i <= 3; i++)
					{
						int DropThreshold = 7 + 2 * (i - 1);

						//The multipolygon at this lod.
						var lodMultiPolygon = points[i];

						int dropCount = 0;
						for (int j = 0; j < lodMultiPolygon.Length; j++)
							if (lodMultiPolygon[j].Length < DropThreshold)
								dropCount++;

						if (dropCount == lodMultiPolygon.Length)
						{
							points[i] = dummyPolygon;
							continue;
						}

						var newLodMultiPolygon = new Point[lodMultiPolygon.Length - dropCount][];
						for (int j = 0, k = 0; j < lodMultiPolygon.Length; j++)
							if (lodMultiPolygon[j].Length >= DropThreshold)
								newLodMultiPolygon[k++] = lodMultiPolygon[j];

						points[i] = newLodMultiPolygon;
					}
				}

				var lists = points.Select(p => p.Select(arr => arr.ToList()).ToList());
				results.Add(lists.Select(list =>
				{
					if (list.Count == 0)
						return null;
					return new Geometry
					{
						Type = GeometryType.MultiPolygon,
						MultiPoints = list
					}.ToWkt();
				}).ToArray());
			}

			return results;
		}
	}
}
