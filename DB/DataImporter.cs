﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GovHack.Database
{
	public interface ICreateTable
	{
		/// <summary>
		/// Creates a new column that can be configured.
		/// </summary>
		ICreateColumn CreateColumn(string name, string sqlServerType);

		/// <summary>
		/// Creates a new geometry column. There is no configuration for a geometry column.
		/// </summary>
		ICreateTable GeometryColumn(string name);
	}

	public interface ICreateColumn : ICreateTable
	{
		/// <summary>
		/// Do this on int columns only
		/// </summary>
		/// <returns></returns>
		ICreateColumn AutoIncrement();

		/// <summary>
		/// There can be at most 1 primary key column
		/// </summary>
		ICreateColumn PrimaryKey();

		/// <summary>
		/// Is this column nullable?
		/// </summary>
		ICreateColumn Nullable();
	}

	class TableBuilder : ICreateTable, ICreateColumn
	{
		string TableName;
		Column LastColumn;
		List<Column> Columns = new List<Column>();

		class Column
		{
			public string Name, SqlServerType;
			public bool IsNullable;
			public bool IsGeometry;
			public bool IsAutoInc;
			public bool IsPrimaryKey;
		}

		public TableBuilder(string name)
		{
			TableName = name;
		}

		void createTable(string name, Column[] columns)
		{
			const string CommandText =
				"create table @TableName(@Columns @Constraints)";

			const string PrimaryKeyConstraint = "constraint PK_@TableName primary key clustered(@Columns) ";

			const string ColumnText = "@ColumnName @Type @Identity @NotNull NULL,";

			var columnsText = string.Concat(columns.Select(column => {
				return ColumnText
					.Replace("@ColumnName", column.Name)
					.Replace("@Type", column.SqlServerType)
					.Replace("@Identity", column.IsAutoInc ? "identity(1,1)" : "")
					.Replace("@NotNull", column.IsNullable ? "" : "NOT");
			}));

			var columnNames = string.Join(", ", columns.Where(column => column.IsPrimaryKey).Select(column => column.Name + " asc"));

			var constraint = PrimaryKeyConstraint.Replace("@TableName", name).Replace("@Columns", columnNames);

			var command = CommandText
				.Replace("@TableName", name)
				.Replace("@Columns", columnsText)
				.Replace("@Constraints", constraint);

			Db.ExecuteCommand(command);
		}

		void createGeometryTable(Column column)
		{
			//Columns for the geometry table.
			var columns = new[] {
				new Column {
					Name = "Id",
					SqlServerType = "int",
					IsPrimaryKey = true
				},
				new Column {
					Name = column.Name,
					SqlServerType = "geography"
				},
				new Column {
					Name = "GeomArea",
					SqlServerType = "real"
				}
			};

			var tableName = TableName + "_" + column.Name;

			createTable(tableName, columns);
			createSpatialIndex(column);
		}

		void createSpatialIndex(Column column)
		{
			const string SpatialIndexText =
				"create spatial index SPATIAL_@TableName on @TableName (@Column) using GEOGRAPHY_GRID with " +
				"(GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM), " +
				"CELLS_PER_OBJECT = 16, PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)";

			var commandText =
				SpatialIndexText
					.Replace("@TableName", TableName + "_" + column.Name)
					.Replace("@Column", column.Name);

			Db.ExecuteCommand(commandText);
		}

		public ICreateColumn CreateColumn(string name, string sqlServerType)
		{
			Columns.Add(LastColumn = new Column { Name = name, SqlServerType = sqlServerType });
			return this;
		}

		public ICreateTable GeometryColumn(string name)
		{
			Columns.Add(LastColumn = new Column { Name = name, SqlServerType = "varchar(max)", IsGeometry = true });
			Columns.Add(LastColumn = new Column { Name = name + "_simple1", SqlServerType = "varchar(max)" });
			Columns.Add(LastColumn = new Column { Name = name + "_simple2", SqlServerType = "varchar(max)" });
			Columns.Add(LastColumn = new Column { Name = name + "_simple3", SqlServerType = "varchar(max)" });
			return this;
		}

		public void Create()
		{
			try { createTable(TableName, Columns.ToArray()); }
			catch (Exception ex) { System.Windows.Forms.MessageBox.Show("Error creating table '" + TableName + "' - " + ex.Message); }

			foreach (var column in Columns.Where(column => column.IsGeometry))
			{
				try { createGeometryTable(column); }
				catch (Exception ex) { System.Windows.Forms.MessageBox.Show("Error creating geom table '" + TableName + "_" + column.Name + "' - " + ex.Message); }	
			}
		}

		void dropTable(string table)
		{
			const string DropTableText = "drop table @TableName";

			try
			{
				var command = DropTableText
					.Replace("@TableName", table);
				Db.ExecuteCommand(command);
			}
			catch { }
		}

		public void Drop()
		{
			dropTable(TableName);

			foreach (var column in Columns.Where(column => column.IsGeometry))
				dropTable(TableName + "_" + column.Name);
		}

		void truncateTable(string table)
		{
			const string TruncateTableText = "truncate table @TableName";

			try
			{
				var command = TruncateTableText
					.Replace("@TableName", table);
				Db.ExecuteCommand(command);
			}
			catch { }
		}

		public void Truncate()
		{
			truncateTable(TableName);

			foreach (var column in Columns.Where(column => column.IsGeometry))
				truncateTable(TableName + "_" + column.Name);
		}

		public ICreateColumn AutoIncrement()
		{
			LastColumn.IsAutoInc = true;
			return this;
		}

		public ICreateColumn PrimaryKey()
		{
			LastColumn.IsPrimaryKey = true;
			return this;
		}

		public ICreateColumn Nullable()
		{
			LastColumn.IsNullable = true;
			return this;
		}
	}

	public abstract class DataImporter
	{
		/// <summary>
		/// The human readable name of the data to be imported.
		/// </summary>
		public string Name { get; protected set; }

		/// <summary>
		/// True to truncate all 'AffectedTables' automatically before inserting.
		/// This is shown on the UI to show the user that the table will be truncated -- rather
		/// than the DataImporter silently doing this.
		/// </summary>
		public bool TruncateBeforeInsert { get; protected set; }

		public bool DropBeforeInsert { get; protected set; }

		/// <summary>
		/// Tables that will be affected by this insert.
		/// </summary>
		public IEnumerable<string> AffectedTables { get; protected set; }

		/// <summary>
		/// Checks to ensure that all requirements for this import are met (eg all required files exist on disk)
		/// </summary>
		public abstract bool CheckRequirements();

		/// <summary>
		/// Create the given table (create table)
		/// </summary>
		/// <param name="table">One of the tables in AffectTables</param>
		protected abstract void GetTableDefinition(string table, ICreateTable builder);

		public void DropTables()
		{
			foreach (var table in AffectedTables)
			{
				var builder = new TableBuilder(table);
				GetTableDefinition(table, builder);
				builder.Drop();
			}
		}

		public void CreateTables()
		{
			foreach (var table in AffectedTables)
			{
				var builder = new TableBuilder(table);
				GetTableDefinition(table, builder);
				builder.Create();
			}
		}

		public void TruncateTables()
		{
			foreach (var table in AffectedTables)
			{
				var builder = new TableBuilder(table);
				GetTableDefinition(table, builder);
				builder.Truncate();
			}
		}
		
		/// <summary>
		/// Executes the given data operation.
		/// </summary>
		public abstract void Execute();
	}
}