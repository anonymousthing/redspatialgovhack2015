﻿namespace GovHack.Database
{
	partial class ImportManager
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lstImporters = new System.Windows.Forms.ListBox();
			this.grpImportDetails = new System.Windows.Forms.GroupBox();
			this.btnImport = new System.Windows.Forms.Button();
			this.btnTruncateTables = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.txtImporterName = new System.Windows.Forms.TextBox();
			this.lblImporterName = new System.Windows.Forms.Label();
			this.chkImporterTruncates = new System.Windows.Forms.CheckBox();
			this.chkImporterRequirementsMet = new System.Windows.Forms.CheckBox();
			this.lstImporterTables = new System.Windows.Forms.ListBox();
			this.btnDropTables = new System.Windows.Forms.Button();
			this.btnCreateTables = new System.Windows.Forms.Button();
			this.chkRebuild = new System.Windows.Forms.CheckBox();
			this.grpImportDetails.SuspendLayout();
			this.SuspendLayout();
			// 
			// lstImporters
			// 
			this.lstImporters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.lstImporters.FormattingEnabled = true;
			this.lstImporters.Location = new System.Drawing.Point(12, 12);
			this.lstImporters.Name = "lstImporters";
			this.lstImporters.Size = new System.Drawing.Size(315, 719);
			this.lstImporters.TabIndex = 0;
			this.lstImporters.SelectedIndexChanged += new System.EventHandler(this.lstImporters_SelectedIndexChanged);
			// 
			// grpImportDetails
			// 
			this.grpImportDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.grpImportDetails.Controls.Add(this.btnImport);
			this.grpImportDetails.Controls.Add(this.btnCreateTables);
			this.grpImportDetails.Controls.Add(this.btnDropTables);
			this.grpImportDetails.Controls.Add(this.btnTruncateTables);
			this.grpImportDetails.Controls.Add(this.label1);
			this.grpImportDetails.Controls.Add(this.txtImporterName);
			this.grpImportDetails.Controls.Add(this.lblImporterName);
			this.grpImportDetails.Controls.Add(this.chkRebuild);
			this.grpImportDetails.Controls.Add(this.chkImporterTruncates);
			this.grpImportDetails.Controls.Add(this.chkImporterRequirementsMet);
			this.grpImportDetails.Controls.Add(this.lstImporterTables);
			this.grpImportDetails.Location = new System.Drawing.Point(333, 12);
			this.grpImportDetails.Name = "grpImportDetails";
			this.grpImportDetails.Size = new System.Drawing.Size(628, 719);
			this.grpImportDetails.TabIndex = 1;
			this.grpImportDetails.TabStop = false;
			this.grpImportDetails.Text = "Details";
			// 
			// btnImport
			// 
			this.btnImport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.btnImport.Location = new System.Drawing.Point(101, 289);
			this.btnImport.Name = "btnImport";
			this.btnImport.Size = new System.Drawing.Size(521, 424);
			this.btnImport.TabIndex = 5;
			this.btnImport.Text = "Import!";
			this.btnImport.UseVisualStyleBackColor = true;
			this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
			// 
			// btnTruncateTables
			// 
			this.btnTruncateTables.Location = new System.Drawing.Point(101, 260);
			this.btnTruncateTables.Name = "btnTruncateTables";
			this.btnTruncateTables.Size = new System.Drawing.Size(112, 23);
			this.btnTruncateTables.TabIndex = 5;
			this.btnTruncateTables.Text = "Truncate Tables";
			this.btnTruncateTables.UseVisualStyleBackColor = true;
			this.btnTruncateTables.Click += new System.EventHandler(this.btnTruncateTables_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(21, 68);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(75, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Tables Altered";
			// 
			// txtImporterName
			// 
			this.txtImporterName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.txtImporterName.Location = new System.Drawing.Point(102, 19);
			this.txtImporterName.Name = "txtImporterName";
			this.txtImporterName.ReadOnly = true;
			this.txtImporterName.Size = new System.Drawing.Size(520, 20);
			this.txtImporterName.TabIndex = 3;
			// 
			// lblImporterName
			// 
			this.lblImporterName.AutoSize = true;
			this.lblImporterName.Location = new System.Drawing.Point(61, 22);
			this.lblImporterName.Name = "lblImporterName";
			this.lblImporterName.Size = new System.Drawing.Size(35, 13);
			this.lblImporterName.TabIndex = 2;
			this.lblImporterName.Text = "Name";
			// 
			// chkImporterTruncates
			// 
			this.chkImporterTruncates.AutoCheck = false;
			this.chkImporterTruncates.AutoSize = true;
			this.chkImporterTruncates.Location = new System.Drawing.Point(220, 45);
			this.chkImporterTruncates.Name = "chkImporterTruncates";
			this.chkImporterTruncates.Size = new System.Drawing.Size(122, 17);
			this.chkImporterTruncates.TabIndex = 1;
			this.chkImporterTruncates.Text = "Requires Truncation";
			this.chkImporterTruncates.UseVisualStyleBackColor = true;
			// 
			// chkImporterRequirementsMet
			// 
			this.chkImporterRequirementsMet.AutoCheck = false;
			this.chkImporterRequirementsMet.AutoSize = true;
			this.chkImporterRequirementsMet.Location = new System.Drawing.Point(102, 45);
			this.chkImporterRequirementsMet.Name = "chkImporterRequirementsMet";
			this.chkImporterRequirementsMet.Size = new System.Drawing.Size(112, 17);
			this.chkImporterRequirementsMet.TabIndex = 1;
			this.chkImporterRequirementsMet.Text = "Requirements Met";
			this.chkImporterRequirementsMet.UseVisualStyleBackColor = true;
			// 
			// lstImporterTables
			// 
			this.lstImporterTables.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.lstImporterTables.FormattingEnabled = true;
			this.lstImporterTables.Location = new System.Drawing.Point(101, 68);
			this.lstImporterTables.Name = "lstImporterTables";
			this.lstImporterTables.Size = new System.Drawing.Size(521, 186);
			this.lstImporterTables.TabIndex = 0;
			// 
			// btnDropTables
			// 
			this.btnDropTables.Location = new System.Drawing.Point(219, 260);
			this.btnDropTables.Name = "btnDropTables";
			this.btnDropTables.Size = new System.Drawing.Size(112, 23);
			this.btnDropTables.TabIndex = 5;
			this.btnDropTables.Text = "Drop Tables";
			this.btnDropTables.UseVisualStyleBackColor = true;
			this.btnDropTables.Click += new System.EventHandler(this.btnDropTables_Click);
			// 
			// btnCreateTables
			// 
			this.btnCreateTables.Location = new System.Drawing.Point(337, 260);
			this.btnCreateTables.Name = "btnCreateTables";
			this.btnCreateTables.Size = new System.Drawing.Size(112, 23);
			this.btnCreateTables.TabIndex = 5;
			this.btnCreateTables.Text = "Create Tables";
			this.btnCreateTables.UseVisualStyleBackColor = true;
			this.btnCreateTables.Click += new System.EventHandler(this.btnCreateTables_Click);
			// 
			// chkRebuild
			// 
			this.chkRebuild.AutoCheck = false;
			this.chkRebuild.AutoSize = true;
			this.chkRebuild.Location = new System.Drawing.Point(348, 45);
			this.chkRebuild.Name = "chkRebuild";
			this.chkRebuild.Size = new System.Drawing.Size(137, 17);
			this.chkRebuild.TabIndex = 1;
			this.chkRebuild.Text = "Requires Table Rebuild";
			this.chkRebuild.UseVisualStyleBackColor = true;
			// 
			// ImportManager
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(973, 737);
			this.Controls.Add(this.grpImportDetails);
			this.Controls.Add(this.lstImporters);
			this.Name = "ImportManager";
			this.Text = "ImportManager";
			this.grpImportDetails.ResumeLayout(false);
			this.grpImportDetails.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ListBox lstImporters;
		private System.Windows.Forms.GroupBox grpImportDetails;
		private System.Windows.Forms.TextBox txtImporterName;
		private System.Windows.Forms.Label lblImporterName;
		private System.Windows.Forms.CheckBox chkImporterRequirementsMet;
		private System.Windows.Forms.ListBox lstImporterTables;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox chkImporterTruncates;
		private System.Windows.Forms.Button btnImport;
		private System.Windows.Forms.Button btnTruncateTables;
		private System.Windows.Forms.Button btnCreateTables;
		private System.Windows.Forms.Button btnDropTables;
		private System.Windows.Forms.CheckBox chkRebuild;
	}
}