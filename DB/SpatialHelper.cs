﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;

namespace GovHack.Database
{
	/// <summary>
	/// Add your spatial column here for ease of use.
	/// 
	/// Add a static inner class [TableName], then a member [ColumnName].
	/// </summary>
	public class SpatialColumn
	{
		public readonly string TableName, KeyColumn, GeometryColumn;

		public SpatialColumn(string tableName, string keyColumn, string geometryColumn)
		{
			TableName = tableName;
			KeyColumn = keyColumn;
			GeometryColumn = geometryColumn;
		}

		public static class Area
		{
			public static readonly SpatialColumn Areas = new SpatialColumn("Areas", "Id", "Area");
		}

        public static class Region_SA2
        {
            public static readonly SpatialColumn Area = new SpatialColumn("Region_SA2", "Id", "Area");
        }

        public static class Region_SA3
        {
            public static readonly SpatialColumn Area = new SpatialColumn("Region_SA3", "Id", "Area");
        }

        public static class Region_SA4
        {
            public static readonly SpatialColumn Area = new SpatialColumn("Region_SA4", "Id", "Area");
        }

	}

	/// <summary>
	/// We use this to get around some LINQ-to-SQL limitations.
	/// With some interesting table design, this works.
	/// </summary>
	public class SpatialHelper
	{
		public const double Epsilon0 = 0.5;
		public const double Epsilon1 = 2.2;
		public const double Epsilon2 = 13;
		public const double Epsilon3 = 25;

		public const double Epsilon1Zoom = 11;
		public const double Epsilon2Zoom = 8;
		public const double Epsilon3Zoom = 6;

		public static string GetGeometryForZoom(string wkt, string simple1, string simple2, string simple3, int zoom)
		{
			if (zoom <= Epsilon3Zoom)
				return simple3;
			if (zoom <= Epsilon2Zoom)
				return simple2;
			if (zoom <= Epsilon1Zoom)
				return simple1;
			return wkt;
		}

		public static string GetGeometryForZoom(Region_SA2 region, int zoom)
		{
			return GetGeometryForZoom(region.Area, region.Area_simple1, region.Area_simple2, region.Area_simple3, zoom);
		}
		public static string GetGeometryForZoom(Region_SA3 region, int zoom)
		{
			return GetGeometryForZoom(region.Area, region.Area_simple1, region.Area_simple2, region.Area_simple3, zoom);
		}
		public static string GetGeometryForZoom(Region_SA4 region, int zoom)
		{
			return GetGeometryForZoom(region.Area, region.Area_simple1, region.Area_simple2, region.Area_simple3, zoom);
		}

		public static List<List<int>> FindIntersecting(GovHackDataContext context, SpatialColumn column, Geometry polygon)
		{
			if (polygon.Type != GeometryType.Polygon)
				throw new ArgumentException("Geometry must be polygon");

			return FindIntersecting(context, column, polygon.ToWkt());
		}
		
		public static List<List<int>> FindIntersecting(GovHackDataContext context, SpatialColumn column, MapRequest bounds)
		{
			var ne = string.Concat(bounds.East, " ", bounds.North);
			var se = string.Concat(bounds.East, " ", bounds.South);
			var nw = string.Concat(bounds.West, " ", bounds.North);
			var sw = string.Concat(bounds.West, " ", bounds.South);

			var wkt = string.Format("POLYGON (({0}, {1}, {2}, {3}, {0}))", nw, sw, se, ne);

			return FindIntersecting(context, column, wkt);
		}

		/// <summary>
		/// Returns all rows intersecting the given wkt.
		/// </summary>
		/// <param name="context">Context from Db.GetContext()</param>
		/// <param name="tableName">The base table name. tableName_columnName is the geometry table.</param>
		/// <param name="geometryColumn">The geometry column name.</param>
		/// <param name="polygonWkt"></param>
		/// <returns>A chunked list, with each entry containing 2k entries.</returns>
		public static List<List<int>> FindIntersecting(GovHackDataContext context, SpatialColumn column, string polygonWkt, int forZoom = -1)
		{
			List<List<int>> keys = new List<List<int>>();
			List<int> last = new List<int>();
			int lastCount = 0;
			keys.Add(last);

			const string CommandText =
				"select Id from @GeomTable " +
				"where @GeomColumn.STIntersects(geography::STGeomFromText(@wkt, 4326)) = 1";

			const string AreaCriteria = " and GeomArea >= @minArea";

			var connection = context.Connection;
			connection.Open();
			try
			{
				using (var command = connection.CreateCommand())
				{
					//We don't use real parameters because these are table names.
					var commandText = CommandText
						.Replace("@GeomTable", column.TableName + "_" + column.GeometryColumn)
						.Replace("@GeomColumn", column.GeometryColumn)
						.Replace("@SourceTable", column.TableName)
						.Replace("@Id", column.KeyColumn);

					if (forZoom >= 0)
					{
						commandText += AreaCriteria.Replace("@minArea", GetAreaThresholdForZoom(forZoom).ToString());
					}

					command.CommandText = commandText;
					var parameter = command.CreateParameter();
					parameter.ParameterName = "wkt";
					parameter.DbType = System.Data.DbType.String;
					parameter.Value = polygonWkt;
					command.Parameters.Add(parameter);

					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							last.Add(reader.GetInt32(0));
							lastCount++;
							if (lastCount >= 2000)
							{
								lastCount = 0;
								keys.Add(last = new List<int>());
							}
						}
					}
				}
			}
			finally
			{
				connection.Close();
			}

			return keys;
		}

		static double GetAreaThresholdForZoom(int zoom)
		{
			var scale = Math.Pow(2, zoom);

			if (zoom <= 5)
				return (1 / scale) * 600000000;
			else if (zoom <= 9)
				return (1 / scale) * 450000000;
			else if (zoom <= 10)
				return (1 / scale) * 200000000;
			else
				return (1 / scale) * 150000000;
		}

		public static List<List<int>> FindIntersectingAtZoom(GovHackDataContext context, SpatialColumn column, MapRequest bounds)
		{
			var ne = string.Concat(bounds.East, " ", bounds.North);
			var se = string.Concat(bounds.East, " ", bounds.South);
			var nw = string.Concat(bounds.West, " ", bounds.North);
			var sw = string.Concat(bounds.West, " ", bounds.South);

			var wkt = string.Format("POLYGON (({0}, {1}, {2}, {3}, {0}))", nw, sw, se, ne);

			return FindIntersecting(context, column, wkt, bounds.Zoom);
		}

		/// <summary>
		/// Calculates the distance between point p and line p1-p2.
		/// </summary>
		static double Distance(Point p, Point p1, Point p2)
		{
			double y2 = p2.Y * 1000;
			double x2 = p2.X * 1000;
			double y1 = p1.Y * 1000;
			double x1 = p1.X * 1000;
			double y0 = p.Y * 1000;
			double x0 = p.X * 1000;
			
			double ydiff = (y2 - y1);
			double xdiff = (x2 - x1);
			double numerator = Math.Abs(ydiff * x0 - xdiff * y0 + x2 * y1 - y2 * x1);
			double denominator = Math.Sqrt(ydiff * ydiff + xdiff * xdiff);

			return numerator / denominator;
		}

		public static Point[] SimplifyPolygon(Point[] points, double epsilon)
		{
			var result = SimplifyPolygonInner(points, 0, points.Length, epsilon);
			if (result.Length < 4)
				return null;
			points = result ?? points;

			return points;
		}

		/// <summary>
		/// We pass in the full list, and then our start/count. This is to avoid creating sublists.
		/// </summary>
		//static List<Point> SimplifyPolygon(List<Point> points, int start, int end, double epsilon)
		//{
		//	List<Point> results = points;

		//	double dmax = 0;
		//	int index = -1;
		//	for (int i = start; i < end - 1; i++)
		//	{
		//		double d = Distance(points[i], points[start], points[end - 1]);
		//		if (d > dmax)
		//		{
		//			index = i;
		//			dmax = d;
		//		}
		//	}
		//	if (dmax > epsilon)
		//	{
		//		results = new List<Point>();
		//		var results1 = SimplifyPolygon(points, start, index, epsilon);
		//		var results2 = SimplifyPolygon(points, index, end, epsilon);

		//		//if (results1 == null)
		//		//	for(int j = start; j <= index - 1; j++)
		//		//		results.Add(points[j - 1]);
		//		//else results.AddRange(results1.Take(results1.Count - 2));

		//		//if (results2 == null)
		//		//	for (int j = index; j <= end; j++)
		//		//		results.Add(points[j - 1]);
		//		//else results.AddRange(results2);

		//		results = results1.Take(results1.Count - 1).Concat(results2).ToList();
		//	}

		//	return results;
		//}

		static Point[] SimplifyPolygonInner(Point[] points, int startIndex, int length, double epsilon)
		{
			double dmax = 0;
			int index = -1;
			for (int i = 1; i < length - 2; i++)
			{
				double d = Distance(points[startIndex + i], points[startIndex], points[startIndex + length - 2]);
				
				if (d > dmax)
				{
					dmax = d;
					index = i;
				}
			}
			if (dmax > epsilon)
			{
				var results1 = SimplifyPolygonInner(points, startIndex, index + 1, epsilon);
				var results2 = SimplifyPolygonInner(points, startIndex + index, length - index, epsilon);

				Point[] results = new Point[results1.Length - 1 + results2.Length];
				Array.Copy(results1, 0, results, 0, results1.Length - 1);
				Array.Copy(results2, 0, results, results1.Length - 1, results2.Length);
				return results;
			}
			else
			{
				return new[] { points[startIndex], points[startIndex + length - 1] };
			}
		}

		/// <summary>
		/// Ensures all rows in the main table have a spatial entry.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="column"></param>
		public static void UpdateForInserts(GovHackDataContext context, SpatialColumn column)
		{
			const string CommandText =
				"insert into @GeomTable (Id, @GeomColumn, GeomArea) " +
					"select @SourceTable.@Id, geography::STGeomFromText(@SourceTable.@GeomColumn, 4326).MakeValid(), geography::STGeomFromText(@SourceTable.@GeomColumn, 4326).MakeValid().STArea() " +
					"from @SourceTable " +
					"left outer join @GeomTable on @SourceTable.@Id = @GeomTable.Id " +
					"where @GeomTable.Id is null";

			var connection = context.Connection;
			connection.Open();
			try
			{
				using (var transaction = connection.BeginTransaction())
				{
					using (var command = connection.CreateCommand())
					{
						command.Transaction = transaction;
						command.CommandTimeout = 600;
						//We don't use real parameters because these are table names.
						var commandText = CommandText
							.Replace("@GeomTable", column.TableName + "_" + column.GeometryColumn)
							.Replace("@GeomColumn", column.GeometryColumn)
							.Replace("@SourceTable", column.TableName)
							.Replace("@Id", column.KeyColumn);

						command.CommandText = commandText;

						command.ExecuteNonQuery();
					}
					transaction.Commit();
				}
			}
			finally
			{
				connection.Close();
			}
		}
	}
}
