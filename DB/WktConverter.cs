﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GovHack.Database
{
	/// <summary>
	/// If parts of this class seem structured weirdly, it is only so that
	/// if we need support for multipoint/linestring/polygon we can add that easily.
	/// We also may need to add support for polygons with holes in them.
	/// </summary>
	public static class WktConverter
	{
		static int skipWhitespace(string source, int index)
		{
			while (index < source.Length && char.IsWhiteSpace(source[index])) index++;
			return index;
		}

		static int match(string source, int index, string rest)
		{
			for (int i = 0; i < rest.Length; i++)
				if (source[index + i] != rest[i])
					throw new FormatException("Unexpected characters");
			return index + rest.Length;
		}

		static int matchGeomType(string source, int startIndex, out GeometryType type)
		{
			int index = skipWhitespace(source, startIndex);

			//We can disambiguate using 3 chars
			//L INESTRING
			//POL YGON
			//POI NT

			if (source[index] == 'L')
			{
				type = GeometryType.Line;
				return match(source, ++index, "INESTRING");
			}
			else if (source[index] == 'P')
			{
				if (source[++index] == 'O')
				{
					index++;

					if (source[index] == 'I')
					{
						type = GeometryType.Point;
						return match(source, ++index, "NT");
					}
					else if (source[index] == 'L')
					{
						type = GeometryType.Polygon;
						return match(source, ++index, "YGON");
					}
				}
			}
			else if (source[index] == 'M')
			{
				index = match(source, ++index, "ULTI");
				if (source[index] == 'P')
				{
					if (source[++index] == 'O')
					{
						index++;

						if (source[index] == 'I')
						{
							type = GeometryType.MultiPoint;
							return match(source, ++index, "NT");
						}
						else if (source[index] == 'L')
						{
							type = GeometryType.MultiPolygon;
							return match(source, ++index, "YGON");
						}
					}
				}
				else if (source[index] == 'L')
				{
					type = GeometryType.MultiLine;
					return match(source, ++index, "INESTRING");
				}
			}

			throw new FormatException("Expected 'POINT', 'LINESTRING', or 'POLYGON' (Or Multi-)");
		}

		static int matchNumber(string source, int startIndex, out double value)
		{
			int index = skipWhitespace(source, startIndex);

			int len = 0;
			while (true)
			{
				char c = source[index + len];

				if ((c >= '0' && c <= '9') || c == '-' || c == '.')
					len++;
				else
					break;
			}

			if (len == 0)
				throw new FormatException("Expected number");

			value = double.Parse(source.Substring(index, len));
			return index + len;
		}

		static int matchCoordinatePair(string source, int startIndex, Point point)
		{
			int index = matchNumber(source, startIndex, out point.X);
			return matchNumber(source, index, out point.Y);
		}

		/// <summary>
		/// After 'POINT' has already been matched
		/// </summary>
		static int matchPoint(string source, int startIndex, Point point)
		{
			int index = skipWhitespace(source, startIndex);

			if (source[index++] != '(')
				throw new FormatException("Point must start with '('");

			index = matchCoordinatePair(source, index, point);

			index = skipWhitespace(source, index);
			if (source[index++] != ')')
				throw new FormatException("Point must end with ')'");

			return index;
		}

		/// <summary>
		/// After 'LINESTRING' has already been matched
		/// </summary>
		static int matchLineString(string source, int startIndex, List<Point> points)
		{
			int index = skipWhitespace(source, startIndex);

			if (source[index++] != '(')
				throw new FormatException("Linestring must start with '('");

			while (true)
			{
				//Match a point
				var point = new Point();
				index = matchCoordinatePair(source, index, point);
				points.Add(point);

				//And keep matching points while we have commas.
				index = skipWhitespace(source, index);
				if (source[index] == ',')
				{
					index++;
					continue;
				}
				break;
			}

			index = skipWhitespace(source, index);
			if (source[index++] != ')')
				throw new FormatException("Linestring must end with ')'");
			return index;
		}

		/// <summary>
		/// After 'POLYGON' has already been matched
		/// </summary>
		static int matchPolygon(string source, int startIndex, List<Point> points)
		{
			int index = skipWhitespace(source, startIndex);

			if (source[index++] != '(')
				throw new FormatException("Polygon must start with '('");

			while (true)
			{
				//Match a linestring
				index = matchLineString(source, index, points);

				//And keep matching linestrings while we have commas.
				index = skipWhitespace(source, index);
				if (source[index] == ',')
				{
					index++;
					throw new FormatException("Polygons with holes are not yet supported.");
				}
				break;
			}

			index = skipWhitespace(source, index);
			if (source[index++] != ')')
				throw new FormatException("Polygon must end with ')'");
			return index;
		}

		static int matchMultiPolygon(string source, int startIndex, List<List<Point>> multipoints)
		{
			int index = skipWhitespace(source, startIndex);

			if (source[index++] != '(')
				throw new FormatException("MultiPolygon must start with '('");

			while (true)
			{
				//Match a polygon
				List<Point> points = new List<Point>();
				index = matchPolygon(source, index, points);
				multipoints.Add(points);

				//And keep matching polygons while we have commas.
				index = skipWhitespace(source, index);
				if (source[index] == ',')
				{
					index++;
					continue;
				}
				break;
			}

			if (source[index++] != ')')
				throw new FormatException("MultiPolygon must end with ')'");
			return index;
		}

		public static Geometry FromWkt(string wkt)
		{
			Geometry geometry = new Geometry();
			int i = matchGeomType(wkt, 0, out geometry.Type);

			var points = new List<Point>();
			var multiPoints = new List<List<Point>>();
			switch (geometry.Type)
			{
			case GeometryType.Point:
				multiPoints = null;
				var point = new Point();
				i = matchPoint(wkt, i, point);
				points.Add(point);
				break;
			case GeometryType.Line:
				multiPoints = null;
				i = matchLineString(wkt, i, points);
				break;
			case GeometryType.Polygon:
				multiPoints = null;
				i = matchPolygon(wkt, i, points);
				break;
			case GeometryType.MultiPolygon:
				points = null;
				i = matchMultiPolygon(wkt, i, multiPoints);
				break;
			default:
				throw new FormatException("Could not determine geometry type");
			}

			i = skipWhitespace(wkt, i);
			if (i != wkt.Length)
				throw new FormatException("Leftover characters!");

			geometry.MultiPoints = multiPoints;
			geometry.Points = points;
			return geometry;
		}

		static void addLatLon(StringBuilder sb, Point point)
		{
			sb
				.Append(point.X)
				.Append(" ")
				.Append(point.Y);
		}

		static void addLatLons(StringBuilder sb, List<Point> points)
		{
			for (int i = 0; i < points.Count; i++)
			{
				addLatLon(sb, points[i]);

				if (i + 1 < points.Count)
					sb.Append(", ");
			}
		}

		static void addPoint(StringBuilder sb, Point point)
		{
			sb.Append("(");
			addLatLon(sb, point);
			sb.Append(")");
		}

		static void addLineString(StringBuilder sb, List<Point> points)
		{
			sb.Append("(");
			addLatLons(sb, points);
			sb.Append(")");
		}

		static void addPolygon(StringBuilder sb, List<Point> points)
		{
			sb.Append("(");
			addLineString(sb, points);
			sb.Append(")");
		}

		static void addMultiPolygon(StringBuilder sb, List<List<Point>> multipoints)
		{
			sb.Append("(");
			for (int i = 0; i < multipoints.Count;i++)
			{
				if (multipoints[i].Count > 0)
					addPolygon(sb, multipoints[i]);
				if (i + 1 < multipoints.Count && multipoints[i+1].Count > 0)
					sb.Append(", ");
			}
			sb.Append(")");
		}

		public static string ToWkt(Geometry geometry)
		{
			StringBuilder sb = new StringBuilder();

			switch (geometry.Type)
			{
			case GeometryType.Point:
				sb.Append("POINT ");
				addPoint(sb, geometry.Points[0]);
				break;
			case GeometryType.Line:
				sb.Append("LINESTRING ");
				addLineString(sb, geometry.Points);
				break;
			case GeometryType.Polygon:
				sb.Append("POLYGON ");
				addPolygon(sb, geometry.Points);
				break;
			case GeometryType.MultiPolygon:
				bool hasPoints = false;
				foreach (var multiPoint in geometry.MultiPoints)
				{
					if (multiPoint.Count > 0)
					{
						hasPoints = true;
						break;
					}
				}
				if (!hasPoints)
				{
					var dummyPolygon = new List<List<Point>> {
						new List<Point> {
							new Point(59.5, -175),
							new Point(59, -175.5),
							new Point(59, -175),
							new Point(59.5, -175)
						}
					};
					geometry.MultiPoints = dummyPolygon;
				}

				sb.Append("MULTIPOLYGON ");
				addMultiPolygon(sb, geometry.MultiPoints);
				break;
			default:
				throw new Exception("Only points, lines & polygons are supported.");
			}

			return sb.ToString();
		}
	}
}
