﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GovHack.Database
{
	public enum GeometryType
	{
		/// <summary>
		/// Points should consist of a single point.
		/// </summary>
		Point,
		/// <summary>
		/// Lines should consist of a series of points.
		/// </summary>
		Line,
		/// <summary>
		/// Polygons should consist of a series of points, be convex and not self-intersect, and have distinct first and last points.
		/// </summary>
		Polygon,
		MultiPoint,
		MultiLine,
		MultiPolygon,
		/// <summary>
		/// Clusters should consist of a single point, with EntityId equal to the count for this cluster. Colour is ignored.
		/// </summary>
		Cluster
	}

	/// <summary>
	/// Represents a Geometry or cluster that can go to JSON. Each Geometry must belong to a layer.
	/// </summary>
	public class Geometry
	{
		/// <summary>
		/// The type of this geometry.
		/// </summary>
		public GeometryType Type;
		
		/// <summary>
		/// The entity id associated with this geometry.
		/// </summary>
		public long EntityId = -1;

		/// <summary>
		/// The points associated with this geometry. For polygons, the first and last points
		/// should not be the same (ie not a WKT geometry). The constructor takes care of converting this
		/// from a DbGeometry.
		/// </summary>
		public List<Point> Points = new List<Point>();

		/// <summary>
		/// Filled when this is a multi-[type] geometry.
		/// </summary>
		public List<List<Point>> MultiPoints;

		/// <summary>
		/// The colour of this geometry, or null to use the layer's colour
		/// </summary>
		public string Colour;

		/// <summary>
		/// THe value of this geometry
		/// </summary>
		public int IndexValue;
		
		public Geometry() { }

		//We have to provide overloads because in a LINQ-Query you can't use default arguments.
		public static Geometry FromWkt(string wkt) { return FromWkt(wkt, -1, null); }
		public static Geometry FromWkt(string wkt, long entityId) { return FromWkt(wkt, entityId, null); }
		public static Geometry FromWkt(string wkt, long entityId, string colour)
		{
			var geometry = WktConverter.FromWkt(wkt);

			geometry.EntityId = entityId;
			geometry.Colour = colour;

			return geometry;
		}

		public string ToWkt() { return WktConverter.ToWkt(this); }
	}

	public class MapLayer
	{
		/// <summary>
		/// The id of this layer in the code.
		/// </summary>
		public string Id;

		/// <summary>
		/// The human-readable name of this layer.
		/// </summary>
		public string Name;

		/// <summary>
		/// The default colour of geometries in this layer.
		/// </summary>
		public string Colour;

		/// <summary>
		/// The geometries in this layer.
		/// </summary>
		public List<Geometry> Geometries;

		public void Add(Geometry geometry) { Geometries.Add(geometry); }
		public void Add(string wkt, long entityId = -1, string colour = null) { Geometries.Add(Geometry.FromWkt(wkt, entityId, colour)); }

		public void AddRange(IEnumerable<Geometry> geometries) { Geometries.AddRange(geometries); }
	}

	/// <summary>
	/// Used to pass bounds from client to server.
	/// </summary>
	public class MapRequest
	{
		public string Dataset;
		public int RegionSize;
		public double North, South, East, West;
		public int Zoom;
	}

	public class Map
	{
		public List<MapLayer> Layers;

		public List<GeometryEntity> Entities;
	}

	public class GeometryEntity
	{
		/// <summary>
		/// EntityIds can be non-contiguous and sparse. They use a dictionary lookup.
		/// </summary>
		public long EntityId;

		/// <summary>
		/// The title of the popup for this geometry.
		/// </summary>
		public string PopupTitle;

		/// <summary>
		/// The contents of the pop-up assocated with this entity
		/// </summary>
		public string PopupText;
	}

	public class Point
	{
		public double Y, X;

		public Point() { }
		public Point(double lat, double lon) { Y = lat; X = lon; }
	}
}
