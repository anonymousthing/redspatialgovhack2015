﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Common;

namespace GovHack.Database
{
	public partial class ImportManager : Form
	{
		DataImporter SelectedImporter;
		static DataImporter[] AllImporters;

		public ImportManager()
		{
			InitializeComponent();

			AllImporters = AllImporters ?? DiscoverImporters();
			PopulateImportersList(AllImporters);

			UnselectImporter();
		}

		void SelectImporter(DataImporter importer)
		{
			grpImportDetails.Enabled = true;

			SelectedImporter = importer;

			bool requirementsMet = false;
			try { requirementsMet = SelectedImporter.CheckRequirements(); }
			catch { } //Ignore errors.

			txtImporterName.Text = SelectedImporter.Name;
			chkImporterRequirementsMet.Checked = requirementsMet;
			chkImporterTruncates.Checked = SelectedImporter.TruncateBeforeInsert;

			lstImporterTables.Items.Clear();
			lstImporterTables.Items.AddRange(SelectedImporter.AffectedTables.Cast<object>().ToArray());
		}

		void UnselectImporter()
		{
			SelectedImporter = null;

			txtImporterName.Text = "";
			chkImporterRequirementsMet.Checked = false;
			chkImporterTruncates.Checked = false;
			lstImporterTables.Items.Clear();

			grpImportDetails.Enabled = false;
		}

		static DataImporter[] DiscoverImporters()
		{
			return
				ReflectionHelper.FindDirectlyDerivedTypes<DataImporter>()
					.TrySelect(type => ReflectionHelper.CreateInstance(type) as DataImporter)
					.Where(instance => instance != null)
					.ToArray();
		}

		void PopulateImportersList(DataImporter[] importers)
		{
			lstImporters.Items.Clear();
			lstImporters.Items.AddRange(importers.Select(importer => importer.Name).Cast<object>().ToArray());
		}

		private void btnTruncateTables_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to truncate these tables?", "Confirmation", MessageBoxButtons.YesNoCancel) != DialogResult.Yes)
				return;

			SelectedImporter.TruncateTables();
		}

		private void btnImport_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Begin import?", "Confirmation", MessageBoxButtons.YesNoCancel) != DialogResult.Yes)
				return;

			if (SelectedImporter.DropBeforeInsert)
			{
				SelectedImporter.DropTables();
				SelectedImporter.CreateTables();
			}
			else if (SelectedImporter.TruncateBeforeInsert)
				SelectedImporter.TruncateTables();

			try
			{
				WaitDialog.WaitFor(() => SelectedImporter.Execute());
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void lstImporters_SelectedIndexChanged(object sender, EventArgs e)
		{
			int index = lstImporters.SelectedIndex;
			if (index < 0 || index >= AllImporters.Length)
			{
				UnselectImporter();
				return;
			}

			SelectImporter(AllImporters[index]);
		}

		private void btnCreateTables_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to create these tables?", "Confirmation", MessageBoxButtons.YesNoCancel) != DialogResult.Yes)
				return;

			SelectedImporter.CreateTables();
		}

		private void btnDropTables_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to drop these tables?", "Confirmation", MessageBoxButtons.YesNoCancel) != DialogResult.Yes)
				return;

			SelectedImporter.DropTables();
		}
	}
}
