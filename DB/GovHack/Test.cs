﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.Types;

namespace DB.GovHack
{
	[Table("test_wkt")]
	public class Test
	{
		[Key]
		public int id { get; set; }

		[Column("state")]
		public string State { get; set; }

		[Column("code")]
		public int Code { get; set; }

		[Column("name")]
		public string Name { get; set; }
		
		[Column("geometry")]
		public string Geometry { get; set; }
	}

	public class GovHackDB : DbContext
	{
		public GovHackDB() : base("GovHackDB") { }

		public DbSet<Test> Test { get; set; }
	}
}
