﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Common;

namespace GovHack.Database
{
	public static class PopulateDBFromWIF
	{	
#if true
		///// <summary>
		///// Read
		///// </summary>
		//public static void Execute(GovHackDB db = null)
		//{
		//	db = db ?? new GovHackDB();

		//	var items = db.Test.Take(10);
		//	List<string> wkts = new List<string>();
		//	wkts.AddRange(items.Select(item => item.Geometry));
		//	var geoms = wkts.Select(wkt => WKTParser.FromWkt(wkt)).ToArray();
		//	return;
		//}
#else
		class Point { public double Lat, Lon; }
		/// <summary>
		/// Write (user should truncate first)
		/// </summary>
		public static void Execute(GovHackDB db = null)
		{
			//return;
			db = db ?? new GovHackDB();

			var mid = @"C:\Users\James\Downloads\1259030001_sd11aaust_midmif\SD11aAust.mid";
			var mif = @"C:\Users\James\Downloads\1259030001_sd11aaust_midmif\SD11aAust.mif";

			var States = new[] { null, "NSW", "VIC", "QLD", "SA", "WA", "TAS", "NT", "ACT", "OTHER" };

			var midLines =
				File.ReadAllLines(mid)
					.Select(line => line.Split(','))
					.Select(parts => new { State = States[int.Parse(parts[0])], Code = int.Parse(parts[1]), Name = parts[2] });

			var mifLines = File.ReadAllLines(mif);

			var geometries = new List<List<List<Point>>>();
			var currentSet = new List<List<Point>>();
			for (int i = 10; i < mifLines.Length; )
			{
				int count;
				if (!int.TryParse(mifLines[i++], out count))
				{
					if (currentSet != null)
					{
						geometries.Add(currentSet);
						currentSet = null;
					}
					continue;
				}
				currentSet = currentSet ?? new List<List<Point>>();

				var points = new List<Point>();
				for (int j = 0; j < count; j++)
				{
					var line = mifLines[i++];
					var parts = line.Split(' ');
					points.Add(new Point { Lon = double.Parse(parts[0]), Lat = double.Parse(parts[1]) });
				}
				currentSet.Add(points);
			}

			db.Database.Connection.Open();
			using (var transaction = db.Database.Connection.BeginTransaction())
			{
				//We have all our geometries.
				var Table = db.Test;
				midLines.ForEachWith(geometries, (info, geoms) =>
				{
					foreach (List<Point> geom in geoms)
					{
						using (var command = transaction.Connection.CreateCommand())
						{
							//Build our polygon
							string wkt = string.Concat("POLYGON((", string.Join(", ", geom.Select(p => p.Lon + " " + p.Lat)), "))");
							var polygon = DbGeometry.FromText(wkt);
							if (!polygon.IsValid)
								continue;

							//This is safe -- we construct wkt.
							command.CommandText = "insert into govhack.test (state, code, name, geometry) values (@state, @code, @name,  GeomFromText('" + wkt + "'));";
							command.Parameters.Add(new MySqlParameter("state", info.State));
							command.Parameters.Add(new MySqlParameter("code", info.Code));
							command.Parameters.Add(new MySqlParameter("name", info.Name));
							command.ExecuteNonQuery();
						}
					}
				});
				
				transaction.Commit();
			}
		}
#endif
	}
}
