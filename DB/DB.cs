﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GovHack.Database
{
    public class Db
    {
		/// <summary>
		/// So we can change connection strings in one place.
		/// </summary>
		/// <returns></returns>
		public static GovHackDataContext GetContext()
		{
			return new GovHackDataContext("Data Source=localhost;Initial Catalog=;Persist Security Info=True;User ID=rsminda;Password=123456");
		}

		public static void ExecuteCommand(string commandText)
		{
			var db = GetContext();

			var connection = db.Connection;
			connection.Open();
			try
			{
				using (var command = connection.CreateCommand())
				{
					command.CommandText = commandText;
					//This is used by SpatialHelper.
					command.CommandTimeout = 600;

					command.ExecuteNonQuery();
				}
			}
			finally
			{
				connection.Close();
			}

		}
    }
}
