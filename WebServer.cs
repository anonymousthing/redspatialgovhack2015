﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using System.Text;

using Common;

namespace WebServer
{
	[AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
	public sealed class RequestHandlerAttribute : Attribute { }

	public class WebServer
	{
		FileAccess fileAccess = new FileAccess(Path.GetFullPath("../../Files"));
		FileAccess streamAccess = new FileAccess(Path.GetFullPath("../../Stream"));

		int Port;
		int ThreadCount;
		public delegate object RequestHandler(Request request);

		long nextId = DateTime.UtcNow.Ticks;
		Dictionary<Tuple<string, string>, RequestHandler> handlers = new Dictionary<Tuple<string, string>, RequestHandler>();

		Dictionary<string, string> getQueryParams(System.Collections.Specialized.NameValueCollection source)
		{
			var dict = new Dictionary<string, string>();
			source.AllKeys.ForEach(key => dict.Add(key, source.Get(key)));
			return dict;
		}

		long getUserId(HttpListenerRequest request)
		{
			var cookie = request.Cookies["id"];
			long value;
			if (cookie == null || !long.TryParse(cookie.Value, out value))
			{
				value = nextId++;
			}
			
			cookie = cookie ?? new Cookie("id", value.ToString());

			return value;
		}

		/// <summary>
		/// Retrieves the part of the url after the domain name.
		/// </summary>
		string trimURL(string url)
		{
			if (url.StartsWith("http://"))
				url = url.Substring("http://".Length);
			else if (url.StartsWith("https://"))
				url = url.Substring("https://".Length);

			if (url.IndexOf('/') == -1) return "";
			url = url.Substring(url.IndexOf('/') + 1);
			return url;
		}

		void fillActionInfo(string url, Request request)
		{
			url = trimURL(url);
			int index = url.IndexOf('/');
			if (index == -1) return;
			var prefix = url.Substring(0, index);
			url = url.Substring(index + 1); //Remove prefix
			request.Prefix = prefix;
			if (prefix == "api")
			{
				//Find class name
				if ((index = url.IndexOf('/')) == -1) return;
				request.ClassName = url.Substring(0, index);

				url = url.Substring(index + 1); //Remove class name

				//Find method name
				index = url.IndexOf('/');
				if (index == -1) index = url.IndexOf('?');
				if (index == -1) return;

				request.MethodName = url.Substring(0, index);
			}
			else if (prefix == "files" || prefix == "stream")
			{
				//Grab relative path
				request.FileName = url;
			}
		}

		void discoverHandlers()
		{
			foreach(var assembly in AppDomain.CurrentDomain.GetAssemblies())
				foreach (var type in assembly.GetTypes())
				{
					var defaultConstructor = type.GetConstructor(new Type[0]);

					//Skip types we can't create
					if (defaultConstructor == null) continue;

					var methods = ReflectionHelper.FindMethodsWithAttribute<RequestHandlerAttribute, RequestHandler>(type);
					foreach (var method in methods.Select(item => item.Item1))
					{
						var instance = Expression.Variable(type);
						var request = Expression.Parameter(typeof(Request));
						var block = Expression.Block(
							new[] { instance },
							new Expression[] {
								Expression.Assign(instance, Expression.New(defaultConstructor)),
								Expression.Call(instance, method, request)
							});

						var handler = Expression.Lambda<RequestHandler>(block, request).Compile();

						handlers.Add(Tuple.Create(type.Name, method.Name), handler);
						Console.WriteLine("Bound {0}.{1}", type.Name, method.Name);
					}

				}
		}

		Request ConvertRequest(HttpListenerRequest request)
		{
			var converted = new Request
			{
				Method = request.HttpMethod,
				QueryParameters = getQueryParams(request.QueryString),
				URL = request.Url.ToString(),
				SessionId = getUserId(request),
				RawRequest = request
			};
			fillActionInfo(request.Url.ToString(), converted);
			return converted;
		}

		void executeAPI(Request request, HttpListenerRequest httpRequest, HttpListenerResponse response)
		{
			StreamWriter sw = new StreamWriter(response.OutputStream);

			RequestHandler handler;
			if (!handlers.TryGetValue(Tuple.Create(request.ClassName, request.MethodName), out handler))
			{
				sw.WriteLine("Unknown handler");
				sw.Close();
				response.Close();
				return;
			}

			var result = handler(request) ?? "";
			if (result is string)
			{
				response.ContentType = "text/html";
				sw.Write((string)result);
				sw.Close();
			}
			else
			{
				response.ContentType = "application/json";
				var serialiser = new Serialiser(SerialiserType.JSON, sw);
				serialiser.Add(result);
				serialiser.Close();
			}
		}

		void executeFiles(Request request, HttpListenerRequest httpRequest, HttpListenerResponse response)
		{
			BinaryWriter bw = new BinaryWriter(response.OutputStream);
			try
			{
				var filesPath = Path.GetFullPath("../../Files");
				var path = Path.GetFullPath(Path.Combine(filesPath, request.FileName));

				if (!path.StartsWith(filesPath))
					throw new Exception();

				byte[] file = fileAccess.GetFile(path);

				response.ContentType = MIMEType.FromExtension(request.FileName.Substring(request.FileName.LastIndexOf('.') + 1));
				response.ContentLength64 = file.Length;

				bw.Write(file);
			}
			catch
			{
				response.StatusCode = 404;
				bw.Write("404 - File Not Found".ToCharArray());
			}
			finally
			{
				bw.Close();
			}
		}

		void executeStream(Request request, HttpListenerRequest httpRequest, HttpListenerResponse response)
		{
			BinaryWriter bw = new BinaryWriter(response.OutputStream);
			try
			{
				var filesPath = Path.Combine(Environment.CurrentDirectory, "stream");
				var path = Path.GetFullPath(Path.Combine(filesPath, request.FileName));

				if (!path.StartsWith(filesPath))
					throw new Exception();

				Stream stream = streamAccess.GetFileStream(path);

				int bufferLength = 2097152;
				long start = 0;
				if (httpRequest.Headers["Range"] != null)
				{
					long end = -1;
					string range = httpRequest.Headers["Range"].Substring(6);
					if (range.EndsWith("-"))
					{
						long.TryParse(range.Substring(0, range.Length - 1), out start);
						end = start + bufferLength;
						range = range + end.ToString();
					}
					else
					{
						long.TryParse(range.Substring(0, range.IndexOf('-')), out start);
						long.TryParse(range.Substring(range.LastIndexOf('-') + 1), out end);
					}
					response.AddHeader("Content-Range", "bytes " + range + "/" + stream.Length.ToString());
					response.StatusCode = 206;
				}

				response.ContentLength64 = response.StatusCode == 206 ? bufferLength : stream.Length;
				response.KeepAlive = false;
				response.ContentType = MIMEType.FromExtension(request.FileName.Substring(request.FileName.LastIndexOf('.') + 1));
				response.AddHeader("Accept-Ranges", "bytes");

				stream.Seek(start, SeekOrigin.Begin);

				if (response.StatusCode == 206) //Grab only 2MB
				{
					byte[] buffer = new byte[bufferLength];
					stream.Read(buffer, 0, bufferLength);
					bw.Write(buffer);
				}
				else //Grab the whole range, 128KB at a time
				{
					bufferLength = 131072;
					long bytesLeft = stream.Length - start;
					while (bytesLeft > bufferLength)
					{
						byte[] buffer = new byte[bufferLength];
						stream.Read(buffer, 0, buffer.Length);
						bytesLeft -= bufferLength;
						try { bw.Write(buffer); }
						catch { break; } //Connection lost
					}
					//Read the rest
					byte[] rest = new byte[bytesLeft];
					stream.Read(rest, 0, (int)bytesLeft);
					try { bw.Write(rest); }
					catch { } //Connection lost
				}
			}
			catch
			{
				response.StatusCode = 404;
				bw.Write("404 - File Not Found".ToCharArray());
			}
			finally
			{
				bw.Close();
			}
		}

		void Execute(HttpListenerRequest httpRequest, HttpListenerResponse response)
		{
			var request = ConvertRequest(httpRequest);
			response.SetCookie(new Cookie("id", request.SessionId.ToString()));

			if (request.Prefix == "api")
				executeAPI(request, httpRequest, response);
			else if (request.Prefix == "files")
				executeFiles(request, httpRequest, response);
			else if (request.Prefix == "stream")
				executeStream(request, httpRequest, response);
			else
			{
				//No prefix
				request.FileName = trimURL(request.URL);
				executeFiles(request, httpRequest, response);
			}
			response.Close();
		}

		/// <param name="port">The port to lisen on</param>
		/// <param name="threadCount">0 = Environment.ProcessorCount * 2</param>
		/// <param name="assemblies">The paths of extra assemblies to load. Scans all loaded in addition to this.</param>
		public WebServer(int port = 8090, int threadCount = 0, string[] assemblies = null)
		{
			foreach (var assembly in assemblies)
			{
				Assembly.LoadFrom(assembly);
				Console.WriteLine("Loaded {0}", assembly);
			}

			Port = port;
			ThreadCount = threadCount;
		}

		public void Run()
		{
			discoverHandlers();

			Console.WriteLine("Starting with {0} threads on port {1}", ThreadCount, Port);
			ThreadPool threadPool = new ThreadPool(ThreadCount);
			threadPool.ExceptionProcessor = (ex) => {
				Console.WriteLine("-------------------------------------------"); 
				Console.WriteLine(ex.Message);
				Console.WriteLine(ex.StackTrace);
				Console.WriteLine("-------------------------------------------");
			};

			HttpListener listener = new HttpListener();
			listener.Prefixes.Add("http://+:" + Port.ToString() + "/");
			listener.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
			listener.Start();
			while (true)
			{
				var result = listener.BeginGetContext((asyncResult) =>
				{
					var context = listener.EndGetContext(asyncResult);
					//Console.WriteLine("Request for {0} received", context.Request.Url);
					threadPool.Enqueue(() => Execute(context.Request, context.Response));
				}, listener);
				result.AsyncWaitHandle.WaitOne();
				threadPool.ProcessCompletionNotifications();
			}
		}

		public static WebServer FromArguments(string[] args)
		{
			int threadCount = Environment.ProcessorCount * 2;
			List<string> extraAssemblies = new List<string>();
			int port = 8090;

			for (int i = 0; i < args.Length; i++)
			{
				if (args[i].ToLower() == "/assembly")
				{
					if (i + 1 < args.Length)
						extraAssemblies.Add(args[++i]);
					else
						throw new Exception("Argument '/assembly' needs an argument after it");
				}
				else if (args[i].ToLower() == "/threads")
				{
					if (i + 1 < args.Length)
						threadCount = int.Parse(args[++i]); //Exceptions here are fine.
					else
						throw new Exception("Argument '/threads' needs an argument after it");
				}
				else if (args[i].ToLower() == "/port")
				{
					if (i + 1 < args.Length)
						port = int.Parse(args[++i]); //Exceptions here are fine.
					else
						throw new Exception("Argument '/port' needs an argument after it");
				}
			}

			return new WebServer(port, threadCount, extraAssemblies.ToArray());
		}
	}
}
