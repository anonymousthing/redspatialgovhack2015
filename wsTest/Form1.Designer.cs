﻿namespace wsTest
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnNewRequest = new System.Windows.Forms.Button();
			this.lstRequests = new System.Windows.Forms.ListView();
			this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.txtUrl = new System.Windows.Forms.TextBox();
			this.btnSpam = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// btnNewRequest
			// 
			this.btnNewRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnNewRequest.Location = new System.Drawing.Point(383, 385);
			this.btnNewRequest.Name = "btnNewRequest";
			this.btnNewRequest.Size = new System.Drawing.Size(75, 23);
			this.btnNewRequest.TabIndex = 0;
			this.btnNewRequest.Text = "Request!";
			this.btnNewRequest.UseVisualStyleBackColor = true;
			// 
			// lstRequests
			// 
			this.lstRequests.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4});
			this.lstRequests.Location = new System.Drawing.Point(13, 12);
			this.lstRequests.Name = "lstRequests";
			this.lstRequests.Size = new System.Drawing.Size(445, 341);
			this.lstRequests.TabIndex = 1;
			this.lstRequests.UseCompatibleStateImageBehavior = false;
			this.lstRequests.View = System.Windows.Forms.View.Details;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Request Started";
			this.columnHeader1.Width = 150;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Request Completed";
			this.columnHeader2.Width = 150;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Duration";
			this.columnHeader3.Width = 80;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Result";
			// 
			// txtUrl
			// 
			this.txtUrl.Location = new System.Drawing.Point(13, 359);
			this.txtUrl.Name = "txtUrl";
			this.txtUrl.Size = new System.Drawing.Size(445, 20);
			this.txtUrl.TabIndex = 2;
			// 
			// btnSpam
			// 
			this.btnSpam.Location = new System.Drawing.Point(302, 385);
			this.btnSpam.Name = "btnSpam";
			this.btnSpam.Size = new System.Drawing.Size(75, 23);
			this.btnSpam.TabIndex = 3;
			this.btnSpam.Text = "Spam";
			this.btnSpam.UseVisualStyleBackColor = true;
			this.btnSpam.Click += new System.EventHandler(this.btnSpam_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(468, 415);
			this.Controls.Add(this.btnSpam);
			this.Controls.Add(this.txtUrl);
			this.Controls.Add(this.lstRequests);
			this.Controls.Add(this.btnNewRequest);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnNewRequest;
		private System.Windows.Forms.ListView lstRequests;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.TextBox txtUrl;
		private System.Windows.Forms.Button btnSpam;
	}
}

