﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;

using Common;

namespace wsTest
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			txtUrl.Text = "http://localhost:8090/api/Program/Test/";
			btnNewRequest.Click += btnNewRequest_Click;

			//Designer was playing up and I couldn't set these.
			lstRequests.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
			txtUrl.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;

			var timer = new Timer();
			timer.Tick += timer_Tick;
			timer.Interval = 1000;
			timer.Start();
		}

		void timer_Tick(object sender, EventArgs e)
		{
			Tuple<int, DateTime, double, string> item;
			while (resultQueue.TryDequeue(out item))
			{
				var row = lstRequests.Items[item.Item1];
				row.SubItems[1].Text = item.Item2.ToString();
				row.SubItems[2].Text = item.Item3.ToTimeString();
				row.SubItems[3].Text = item.Item4;
			}
		}

		ConcurrentQueue<Tuple<int, DateTime, double, string>> resultQueue = new ConcurrentQueue<Tuple<int, DateTime, double, string>>();

		void btnNewRequest_Click(object sender, EventArgs e)
		{
			int rowId = lstRequests.Items.Count;
			var startTime = DateTime.Now;
			var url = txtUrl.Text;

			lstRequests.Items.Add(new ListViewItem(new[] { startTime.ToString(), "", "", "" }));
			new System.Threading.Thread(() =>
			{
				var request = HttpWebRequest.CreateHttp(url);
				var response = request.GetResponse();

				StreamReader sr = new StreamReader(response.GetResponseStream());
				var text = sr.ReadToEnd();
				sr.Close();

				var endTime = DateTime.Now;
				var duration = (endTime - startTime).TotalSeconds;

				resultQueue.Enqueue(Tuple.Create(rowId, endTime, duration, text));
			}).Start();
		}

		private void btnSpam_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			sw.Start();
			int i = 0;
			int periodMS = 1;
			while (true)
			{
				if (sw.ElapsedMilliseconds > periodMS * i)
				{
					var request = HttpWebRequest.CreateHttp(txtUrl.Text);
					request.GetResponseAsync();
					i++;
				}
			}
		}
	}
}
