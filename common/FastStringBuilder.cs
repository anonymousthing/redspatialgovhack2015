﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	public class FastStringBuilder
	{
		public char[] Internals;
		public int Capacity, Length;

		public FastStringBuilder(int capacity)
		{
			Internals = new char[capacity];
			this.Capacity = capacity;
		}

		public void Append(char c)
		{
			if (Length == Capacity)
			{
				Capacity *= 2;
				char[] chars = new char[Capacity];
				for (int i = 0; i < Internals.Length; i++)
					chars[i] = Internals[i];
			}
			Internals[Length++] = c;
		}

		public void Clear() { Length = 0; }

		public override string ToString() { return new string(Internals, 0, Length); }
	}
}
