﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;

namespace Common
{
	public class ThreadPool
	{
		private class JobItem
		{
			public Func<object, object> Handler;
			public object Argument;
			public Action<object> Callback;
			public object Result;
		}

		private readonly object sync = new object();

		public int ThreadCount { get; private set; }
		private int jobsRemaining = 0;
		private BlockingCollection<JobItem> jobQueue = new BlockingCollection<JobItem>();
		private BlockingCollection<JobItem> completionQueue = new BlockingCollection<JobItem>();

		public Action<Exception> ExceptionProcessor = null;

		private void threadLoop(object thread)
		{
			while (true)
			{
				JobItem item = jobQueue.Take();
				if (item.Handler == null)
					return; //Kill thread signal
				
				try
				{
					lock (sync)
					{
						jobsRemaining--;
						if (jobsRemaining == 0)
							Monitor.Pulse(sync);
					}
					//Console.WriteLine("Start Job: " + Environment.CurrentManagedThreadId);
					item.Result = item.Handler(item.Argument);
				}
				catch (Exception ex)
				{
					item.Callback = null;
					item.Result = ex;
					completionQueue.Add(item);
				}

				if (item.Callback != null)
					completionQueue.Add(item);
			}
		}

		/// <param name="threadCount">The number of threads to create, or 0 to indicate that there should be 1 thread per logical processor.</param>
		public ThreadPool(int threadCount = 0)
		{
			if (threadCount == 0)
				threadCount = Environment.ProcessorCount;

			AddThreads(threadCount);
		}

		public void AddThreads(int count)
		{
			for (int i = 0; i < count; i++)
			{
				var thread = new Thread(threadLoop);
				thread.Start(thread);
			}

			ThreadCount += count;
		}

		/// <summary>
		/// Stops count threads once they have finished any current processing.
		/// </summary>
		/// <param name="count">The number of threads to retire</param>
		public void RetireThreads(int count)
		{
			if (count > ThreadCount)
				throw new ArgumentException("count must be less than or equal to the number of running threads.", "count");

			for (int i = 0; i < count; i++)
				jobQueue.Add(new JobItem()); //Empty job item kills thread.

			ThreadCount -= count;
		}

		/// <summary>
		/// Waits for jobs to complete
		/// </summary>
		public void WaitForCompletion(bool processNotifications = true)
		{
			lock (sync)
			{
				while (Thread.VolatileRead(ref jobsRemaining) != 0)
					Monitor.Wait(sync);
			}

			if (processNotifications)
				ProcessCompletionNotifications();
		}

		public void ProcessCompletionNotifications()
		{
			JobItem item;
			while (completionQueue.TryTake(out item))
			{
				if (item.Callback == null)
				{
					//Exception
					if (ExceptionProcessor == null)
					{
						throw new Exception("", item.Result as Exception);
					}
					else
						ExceptionProcessor(item.Result as Exception);
				}
				else
				{
					item.Callback(item.Result);
				}
			}
		}

		public void Enqueue(Action target, Action completionCallback = null) { Enqueue(() => { target(); return null; }, completionCallback == null ? (Action<object>)null : ((x) => completionCallback())); }
		public void Enqueue(Action<object> target, object argument, Action completionCallback = null) { Enqueue((o) => { target(o); return null; }, argument, (x) => completionCallback()); }
		public void Enqueue(Func<object> target, Action<object> completionCallback = null) { Enqueue((o) => { target(); return null; }, null, completionCallback == null ? (Action<object>)null : (x) => completionCallback(x)); }

		public void Enqueue(Func<object, object> target, object argument, Action<object> completionCallback = null)
		{
			lock (sync) { jobsRemaining++; }

			JobItem item = new JobItem();
			item.Handler = target;
			item.Argument = argument;
			item.Callback = completionCallback;
			jobQueue.Add(item);
		}

		private object collectionProcessor<T>(object arg)
		{
			var args = (Tuple<CollectionProcessorState<T>, int, int>)arg;
			var state = (CollectionProcessorState<T>)args.Item1;

			var collection = state.Collection;
			var action = state.Action;

			int baseIndex = args.Item2;
			int count = args.Item3;
			for (int i = baseIndex; i < baseIndex + count; i++)
			{
				try
				{
					action(collection[i]);
				}
				catch(Exception ex)
				{
					JobItem item = new JobItem();
					item.Result = ex;
					if (state.ExceptionProcessor != null)
						item.Handler = (e) => { state.ExceptionProcessor(e as Exception); return null; };

					completionQueue.Add(item);
				}
			}

			return state;
		}

		private class CollectionProcessorState
		{
			public Action<Exception> ExceptionProcessor;
			public Action CompletionCallback;
			public int RemainCount;
		}

		private class CollectionProcessorState<T> : CollectionProcessorState
		{
			public IList<T> Collection;
			public Action<T> Action;
		}

		private void collectionProcessorCompleted(object arg)
		{
			var state = (CollectionProcessorState)arg;
			state.RemainCount--;
			if (state.RemainCount == 0 && state.CompletionCallback != null)
				state.CompletionCallback();
		}

		/// <summary>
		/// Enqueues the entire collection, splitting it evenly over the all current threads.
		/// </summary>
		public void EnqueueCollection<T>(IList<T> collection, Action<T> action, Action completionCallback = null, Action<Exception> exceptionProcessor = null)
		{
			int threadCount = Math.Max(collection.Count, ThreadCount);

			int itemsPerThread = collection.Count / threadCount;
			int leftoverItems = collection.Count % threadCount;

			CollectionProcessorState<T> state = new CollectionProcessorState<T>
			{
				Action = action,
				Collection = collection,
				CompletionCallback = completionCallback,
				ExceptionProcessor = exceptionProcessor,
				RemainCount = threadCount
			};

			int index = 0;
			for(int i = 0; i < ThreadCount;i++)
			{
				int thisCount = i >= leftoverItems ? itemsPerThread : itemsPerThread + 1;
				var args = new Tuple<CollectionProcessorState<T>, int, int>(state, index, thisCount);
				Enqueue((Func<object, object>)collectionProcessor<T>, args, collectionProcessorCompleted);

				index += thisCount;
			}
		}
	}
}
