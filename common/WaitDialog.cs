﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Common
{
	public partial class WaitDialog : Form
	{
		Action Action;
		Exception Exception;

		public WaitDialog()
		{
			InitializeComponent();
		}

		private WaitDialog(Action action) : this()
		{
			Action = action;
		}

		protected override async void OnShown(EventArgs e)
		{
			if (Action != null)
			{
				try { await Task.Run(Action); }
				catch (Exception ex) { Exception = ex; }
				Close();
			}
		}

		public static void WaitFor(Action action)
		{
			var dialog = new WaitDialog(action);
			dialog.ShowDialog();
			if (dialog.Exception != null)
				throw dialog.Exception;
		}
	}
}
