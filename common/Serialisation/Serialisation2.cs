﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	public class Serialisation2
	{
		static ConcurrentDictionary<Type, Action<object, SerialiserBase>> serialisers = new ConcurrentDictionary<Type, Action<object, SerialiserBase>>();
		static ConcurrentDictionary<Type, Func<DeserialiserBase, object>> deserialisers = new ConcurrentDictionary<Type, Func<DeserialiserBase, object>>();

		class ReflectedMember
		{
			public string Name;
			public Type Type;
		}

		static IEnumerable<ReflectedMember> GetPublicGetSetPropertiesAndFields(Type type)
		{
			return type.GetProperties()
				//We want public instance get/set properties only.
				.Where(prop => prop.CanRead && prop.CanWrite && prop.SetMethod.IsPublic && prop.GetMethod.IsPublic && !prop.SetMethod.IsStatic)
				.Select(prop => new ReflectedMember { Name = prop.Name, Type = prop.PropertyType })
				.Concat(type.GetFields() //Only returns public instance fields.
					.Where(field => !field.IsStatic)
					.Select(field => new ReflectedMember { Name = field.Name, Type = field.FieldType }))
				.ToArray(); //Return as array to allow multiple enumeration.
		}

		static IEnumerable<ReflectedMember> GetPublicGetPropertiesAndFields(Type type)
		{
			return type.GetProperties()
				//We want public instance get/set properties only.
				.Where(prop => prop.CanRead && prop.GetMethod.IsPublic && !prop.GetMethod.IsStatic )
				.Select(prop => new ReflectedMember { Name = prop.Name, Type = prop.PropertyType })
				.Concat(type.GetFields() //Only returns public instance fields.
					.Where(field => !field.IsStatic)
					.Select(field => new ReflectedMember { Name = field.Name, Type = field.FieldType }))
				.ToArray(); //Return as array to allow multiple enumeration.
		}

		static IEnumerable<ReflectedMember> GetTupleMembers(Type type)
		{
			var typeArgs = type.GetGenericArguments();
			return typeArgs.Select((memberType, i) =>
				new ReflectedMember
				{
					Name = "Item" + (i + 1).ToString(),
					Type = memberType
				}
			).ToArray();
		}

		static Expression FormatString(string format, params Expression[] arguments) { return FormatString(Expression.Constant(format), arguments); }
		static Expression FormatString(Expression format, params Expression[] arguments)
		{
			var formatMethod = typeof(string).GetMethod("Format", new[] { typeof(string), typeof(object).MakeArrayType() });
			return Expression.Call(formatMethod, format, Expression.NewArrayInit(typeof(object), arguments));
		}

		static Expression ThrowException<T>(string message) { return ThrowException(typeof(T), message); }
		static Expression ThrowException<T>(string format, params Expression[] arguments) { return ThrowException(typeof(T), FormatString(format, arguments)); }
		static Expression ThrowException(Type exceptionType, string message) { return ThrowException(exceptionType, Expression.Constant(message)); }
		static Expression ThrowException(Type exceptionType, Expression message)
		{
			return Expression.Throw(Expression.New(exceptionType.GetConstructor(new[] { typeof(string) }), message));
		}

		static Expression CheckNullableIsNotNull(Expression nullable, Type nullableType, Expression deserialiser)
		{
			//We only care about System.Nullables.
			if (nullableType.IsClass) return nullable;

			//nullable is nullable, check it is not null & throw an exception if it is
			var exception = ThrowDeserialisationException("Field cannot be null", deserialiser);
			var hasValue = Expression.Call(nullable, nullableType.GetMethod("get_HasValue"));
			var getValue = Expression.Call(nullable, nullableType.GetMethod("get_Value"));

			return Expression.Block(Expression.IfThen(Expression.Not(hasValue), exception), getValue);
		}

		static Expression ThrowDeserialisationException(string message, Expression deserialiser)
		{
			Type deserialiserType = typeof(DeserialiserBase);
			var getLine = deserialiserType.GetMethod("get_Line", new Type[0]);
			var getCol = deserialiserType.GetMethod("get_Column", new Type[0]);

			Expression line = Expression.Convert(Expression.Call(deserialiser, getLine), typeof(object));
			Expression column = Expression.Convert(Expression.Call(deserialiser, getCol), typeof(object));

			return ThrowException<Exception>("{0} @ {1}:{2}", Expression.Constant(message), line, column);
		}

		/// <summary>
		/// Checks value for null. Generates the correct form of IfThen/IfThenElse depending on whether ifNotNull,
		/// ifNull, or both, are supplied.
		/// </summary>
		/// <returns>An expression which evaluates to ifNotNull or ifNull depending on if value is null or not.
		/// The expression returns void if it would have evaluated to ifNotNull/isNull and that argument is null.</returns>
		static Expression CreateNullCheck(Expression value, Expression ifNotNull = null, Expression ifNull = null)
		{
			if (ifNotNull == null && ifNull == null)
				throw new Exception("At least one of ifNotNull and ifNull must be non-null");

			if (ifNull != null && ifNotNull == null) //return null ? ifNull
				return Expression.IfThen(Expression.Equal(value, Expression.Constant(null)), ifNull);
			else if (ifNull == null && ifNotNull != null) //return !null ? ifNotNull
				return Expression.IfThen(Expression.NotEqual(value, Expression.Constant(null)), ifNotNull);
			else //return !null ? ifNotNull : ifNull
				return Expression.IfThenElse(Expression.NotEqual(value, Expression.Constant(null)), ifNotNull, ifNull);
		}

		static void SerialiseArrayElements<T>(T[] array, string name, SerialiserBase serialiser, Action<T> serialiseAction)
		{
			serialiser.StartArray(name, array);
			array.ForEach(serialiseAction);
			serialiser.EndArray(name, array);
		}

		static Action<object, SerialiserBase> getSerialiser(Type t)
		{
			Action<object, SerialiserBase> serialise;
			if (serialisers.TryGetValue(t, out serialise))
				return serialise;

			serialise = MakeSerialiser(t);
			serialisers.AddOrUpdate(t, serialise, (x,y) => y);
			return serialise;
		}

		private static Type __lastDeserialised = null;
		private static Func<DeserialiserBase, object> __lastDeserialiser;

		static Func<DeserialiserBase, object> getDeserialiser(Type t)
		{
			if (__lastDeserialised == t) return __lastDeserialiser;
			__lastDeserialised = t;

			Func<DeserialiserBase, object> deserialise;
			if (deserialisers.TryGetValue(t, out deserialise))
				return __lastDeserialiser = deserialise;

			deserialise = MakeDeserialiser(t);
			deserialisers.AddOrUpdate(t, deserialise, (x, y) => y);
			return __lastDeserialiser = deserialise;
		}

		static void SerialiseObjectField<T>(string name, T value, SerialiserBase serialiser)
		{
			var serialise = getSerialiser(typeof(T));
			serialiser.StartObject(name, value);
			serialise(value, serialiser);
			serialiser.EndObject(name, value);
		}

		/// <summary>
		/// collectionIndexer(collection, index) => item
		/// </summary>
		static Expression CreateSerialiseCollection(Type collectionType, Type elementType,
			Expression collection, Expression collectionLength, Func<Expression, Expression, Expression> collectionIndexer,
			string name, string collectionStartMethod, string collectionEndMethod, Expression serialiser)
		{
			var collectionName = Expression.Constant(name, typeof(string));

			var counter = Expression.Variable(typeof(int));
			var length = Expression.Variable(typeof(int));
			var loopExit = Expression.Label();
			var block = Expression.Block(
				new[] { counter, length },
				new Expression[] { 
					Expression.Call(serialiser, collectionStartMethod, new [] { elementType }, collectionName, collection),
					Expression.Assign(counter, Expression.Constant(0)),
					Expression.Assign(length, collectionLength),
					Expression.Loop(Expression.Block(
						new Expression [] {
							Expression.IfThen(Expression.Equal(counter, length), Expression.Break(loopExit)),
							SerialiseField(collectionIndexer(collection, counter), null, elementType, serialiser),
							Expression.PostIncrementAssign(counter)
						}
					), loopExit),
					Expression.Call(serialiser, collectionEndMethod, new [] { elementType }, collectionName, collection)
				}
				);

			return block;
		}

		static Expression CreateSerialiseArray(Type arrayType, Expression array, string name, Expression serialiser)
		{
			Type elementType = arrayType.GetElementType();
			var arrayLength = Expression.ArrayLength(array);
			Func<Expression, Expression, Expression> arrayAccess = (collection, index) => Expression.ArrayAccess(collection, index);

			return CreateSerialiseCollection(arrayType, elementType, array, arrayLength, arrayAccess, name, "StartArray", "EndArray", serialiser);
		}
		static Expression CreateSerialiseList(Type listType, Expression list, string name, Expression serialiser)
		{
			Type elementType = listType.GetGenericArguments()[0];
			var listLength = Expression.Call(list, listType.GetMethod("get_Count"));
			var getItem = listType.GetMethod("get_Item");
			Func<Expression, Expression, Expression> listAccess = (collection, index) => Expression.Call(collection, getItem, index);

			return CreateSerialiseCollection(listType, elementType, list, listLength, listAccess, name, "StartList", "EndList", serialiser);
		}

		static Expression SerialiseField(Expression value, string valueName, Type valueType, Expression serialiser)
		{
			var serialiserType = typeof(SerialiserBase);

			var memberName = Expression.Constant(valueName, typeof(string));

			var serialiseNull = Expression.Call(serialiser, "AddFieldNull", new[] { valueType }, memberName);

			Expression serialise;
			if (valueType.IsArray)
				serialise = CreateSerialiseArray(valueType, value, valueName, serialiser);
			else if (valueType.IsList())
				serialise = CreateSerialiseList(valueType, value, valueName, serialiser);
			else
			{
				var serialiseMethod = serialiserType.GetMethod("AddField", new[] { typeof(string), valueType.IsEnum ? typeof(string) : valueType });

				if (valueType.IsEnum)
				{
					//Replace value with string
					var getNameMethod = typeof(Enum).GetMethod("GetName", new[] { typeof(Type), typeof(object) });
					var getTypeMethod = typeof(object).GetMethod("GetType", new Type[0]);
					value = Expression.Call(getNameMethod, Expression.Call(value, getTypeMethod), Expression.Convert(value, typeof(object)));
				}

				if (serialiseMethod != null) //Use primitive serialise if possible
					serialise = Expression.Call(serialiser, serialiseMethod, memberName, value);
				else //Can't do that. Do an object serialise.
					serialise = Expression.Call(typeof(Serialisation2), "SerialiseObjectField", new[] { valueType }, memberName, value, serialiser);
			}

			if (valueType.IsClass)
				return Expression.IfThenElse(Expression.Equal(value, Expression.Constant(null)), serialiseNull, serialise);
			else
				return serialise;
		}

		static Action<object, SerialiserBase> MakeSerialiser(Type objType)
		{
			List<Expression> expressions = new List<Expression>();
			List<Expression> parameters = new List<Expression>();

			var raw = Expression.Parameter(typeof(object));
			parameters.Add(raw);
			
			var serialiser = Expression.Parameter(typeof(SerialiserBase));
			parameters.Add(serialiser);

			var obj = Expression.Convert(raw, objType);
			expressions.Add(obj);

			//Retrieve tuples of all members (name/type)
			var members = objType.IsTuple()
				? GetTupleMembers(objType)
				: GetPublicGetPropertiesAndFields(objType);

			//Foreach field and property
			foreach (var member in members)
			{
				expressions.Add(SerialiseField(Expression.PropertyOrField(obj, member.Name), member.Name, member.Type, serialiser));
			}

			return Expression.Lambda<Action<object, SerialiserBase>>(Expression.Block(expressions), raw, serialiser).Compile();
		}

		public static void Serialise(object obj, SerialiserBase serialiser)
		{
			var serialise = getSerialiser(obj.GetType());
			serialiser.StartObject(null, obj);
			serialise(obj, serialiser);
			serialiser.EndObject(null, obj);
		}

		public static void SerialiseAs<T>(T obj, SerialiserBase serialiser)
		{
			var serialise = getSerialiser(typeof(T));
			serialiser.StartObject(null, obj);
			serialise(obj, serialiser);
			serialiser.EndObject(null, obj);
		}

		static T DeserialiseObjectField<T>(DeserialiserBase deserialiser)
		{
			var deserialise = getDeserialiser(typeof(T));
			T deserialisedValue = (T)deserialise(deserialiser);
			return deserialisedValue;
		}

		/// <summary>
		/// addToCollection(collection, item) => collection
		/// Returns a block that fills emptyCollection.
		/// </summary>
		static Expression CreateDeserialiseCollection(Type elementType, Type collectionType,
			Expression emptyCollection, Func<Expression, Expression, Expression> addToCollection, Func<Expression> getNextValue,
			string collectionStartMethod, string collectionEndMethod, Expression deserialiser)
		{
			var expressions = new List<Expression>();

			//Call collectionStartMethod
			var startMethod = Expression.Call(deserialiser, collectionStartMethod, new[] { collectionType });
			expressions.Add(Expression.IfThen(Expression.Not(startMethod), ThrowException<Exception>("Expected start of collection")));

			var breakLabel = Expression.Label();

			//Loop
			//	If !HasNextValue
			//		Break
			//	AddToCollection(GetNextValue)

			var hasNextValueMethod = typeof(DeserialiserBase).GetMethod("HasNextValue", new Type[0]);
			var hasNextValue = Expression.Call(deserialiser, hasNextValueMethod);

			var loopBody = Expression.Block(
				Expression.IfThen(Expression.Not(hasNextValue), Expression.Break(breakLabel)),
				addToCollection(emptyCollection, getNextValue()));

			expressions.Add(Expression.Loop(loopBody, breakLabel));

			//Call collectionEndMethod
			expressions.Add(Expression.Call(deserialiser, collectionEndMethod, new[] { collectionType }));
			return Expression.Block(expressions);
		}

		static Expression CreateDeserialiseArray(Type arrayType, Expression deserialiser)
		{
			var expressions = new List<Expression>();

			var elementType = arrayType.GetElementType();
			Func<Expression> deserialiseArrayMember = () => DeserialiseField(elementType, deserialiser);

			//We don't know the length in advance, so we build a list and then call .ToArray()
			var listType = typeof(List<>).MakeGenericType(new[] { elementType });
			var listConstructor = listType.GetConstructor(new[] { typeof(int) });
			var defaultCapacity = Expression.Constant(32);

			var list = Expression.Variable(listType);
			//Construct our temporary list
			expressions.Add(Expression.Assign(list, Expression.New(listConstructor, defaultCapacity)));

			var addItem = listType.GetMethod("Add", new[] { elementType });

			Func<Expression, Expression, Expression> addToList = (collection, item) =>
				Expression.Block(
					Expression.Call(collection, addItem, item),
					collection);

			//Fill our list
			expressions.Add(CreateDeserialiseCollection(elementType, arrayType, list, addToList, deserialiseArrayMember, "StartArray", "EndArray", deserialiser));

			//Now we need to convert this to an array
			var toArray = listType.GetMethod("ToArray", new Type[0]);
			expressions.Add(Expression.Call(list, toArray));

			return Expression.Block(new[] { list }, expressions);
		}

		static Expression CreateDeserialiseList(Type listType, Expression deserialiser)
		{
			var expressions = new List<Expression>();

			var elementType = listType.GetGenericArguments()[0];
			Func<Expression> deserialiseMember = () => DeserialiseField(elementType, deserialiser);

			var listConstructor = listType.GetConstructor(new[] { typeof(int) });
			var defaultCapacity = Expression.Constant(32);

			//Construct our list
			var list = Expression.Variable(listType);
			expressions.Add(Expression.Assign(list, Expression.New(listConstructor, defaultCapacity)));

			var addItem = listType.GetMethod("Add", new[] { elementType });

			Func<Expression, Expression, Expression> addToList = (collection, item) =>
				Expression.Block(
					Expression.Call(collection, addItem, item),
					collection);

			//Fill our list
			expressions.Add(CreateDeserialiseCollection(elementType, listType, list, addToList, deserialiseMember, "StartList", "EndList", deserialiser));

			//Make sure we end with our list
			expressions.Add(list);

			return Expression.Block(new[] { list }, expressions);
		}

		static Expression DeserialiseField(Type fieldType, Expression deserialiser)
		{
			var deserialiserType = typeof(DeserialiserBase);

			Expression deserialise = null;
			if (fieldType.IsArray)
				deserialise = CreateDeserialiseArray(fieldType, deserialiser);
			else if (fieldType.IsList())
				deserialise = CreateDeserialiseList(fieldType, deserialiser);
			else
			{
				var nullableType = fieldType.MakeNullable();
				var deserialiseMethod = deserialiserType.GetMethod("GetValue", new[] { nullableType.MakeByRefType() });

				if (deserialiseMethod != null)
				{
					var result = Expression.Variable(nullableType);
					deserialise = Expression.Block(new[] { result }, new[] {
						Expression.Call(deserialiser, deserialiseMethod, result),
						CheckNullableIsNotNull(result, nullableType, deserialiser)
					});
				}
				else
				{
					deserialise = Expression.Call(typeof(Serialisation2), "DeserialiseObjectField", new[] { fieldType }, deserialiser);
				}
			}

			return Expression.Convert(deserialise, fieldType);
		}

		/// <summary>
		/// </summary>
		/// <param name="deserialisableMembers">null for GetPublicGetSetPropertiesAndFields(objType)</param>
		static void DeserialiseObjectFields(List<Expression> expressions, List<ParameterExpression> variables,
			Expression obj, Type objType, Expression deserialiser,
			Func<string, Type, Expression> assignMember,
			IEnumerable<ReflectedMember> deserialisableMembers = null)
		{
			var deserialiserType = typeof(DeserialiserBase);

			//Retrieve tuples of all members (name/type)
			var members = (deserialisableMembers ?? GetPublicGetSetPropertiesAndFields(objType))
							.Select(member => new { Name = member.Name, ExpName = Expression.Constant(member.Name), Type = member.Type });

			//Used as we are reading fields, to store the name as we check it against fields we have.
			var nextFieldName = Expression.Variable(typeof(string));
			variables.Add(nextFieldName);

			//Loop {
			//	Get field name
			//		if null break.
			//	ForEach member
			//		If name = member.Name
			//			Read field<member.Type>
			//	Throw exception if not found
			//}

			var loopExpressions = new List<Expression>();
			var breakLabel = Expression.Label();
			var continueLabel = Expression.Label();

			//Get the next field name, breaking if it is null.
			var getNextFieldName = deserialiserType.GetMethod("GetNextFieldName", new Type[0]);
			loopExpressions.Add(Expression.Assign(nextFieldName, Expression.Call(deserialiser, getNextFieldName)));
			loopExpressions.Add(CreateNullCheck(nextFieldName, null, Expression.Break(breakLabel)));

			//Generate checks against getNextFieldName to match it to a field.
			foreach (var member in members)
			{
				var equalityCheck = Expression.Equal(nextFieldName, member.ExpName);

				var readFieldValue = Expression.Block(
					//Deserialise the field, then continue (we can skip the other equality checks)
					assignMember(member.Name, member.Type),
					Expression.Continue(continueLabel));

				loopExpressions.Add(Expression.IfThen(equalityCheck, readFieldValue));
			}

			//Generate a throw expression -- if execution has reached here then the field name was not found.
			//loopExpressions.Add(ThrowException<Exception>("Field '{0}' was not found on '{1}'", nextFieldName, Expression.Constant(objType.FullName)));

			//Create the actual loop
			expressions.Add(Expression.Loop(Expression.Block(loopExpressions), breakLabel, continueLabel));
		}

		static Func<DeserialiserBase, object> MakeDeserialiser(Type objType)
		{
			//These all belong to the Func we will generate.
			var expressions = new List<Expression>();
			var parameters = new List<ParameterExpression>();
			var variables = new List<ParameterExpression>();

			var deserialiserType = typeof(DeserialiserBase);
			var deserialiser = Expression.Parameter(deserialiserType);
			parameters.Add(deserialiser);

			var callStartObject = Expression.IfThen(Expression.Not(Expression.Call(deserialiser, "StartObject", new[] { objType })),
				ThrowDeserialisationException("Expected '" + objType.FullName + "'", deserialiser));

			var callEndObject = Expression.Call(deserialiser, "EndObject", new[] { objType });

			var nextValueIsNull = deserialiserType.GetMethod("NextValueIsNull", new Type[0]);
			var endLabel = Expression.Label(typeof(object));
			expressions.Add(Expression.IfThen(Expression.Call(deserialiser, nextValueIsNull),
				Expression.Block(
					Expression.IfThen(Expression.Not(Expression.Call(deserialiser, "StartObject", new[] { objType })),
					ThrowDeserialisationException("Expected NULL '" + objType.FullName + "'", deserialiser)),
					callEndObject,
					Expression.Goto(endLabel, Expression.Constant(null)
					))));

			//Start by calling StartObject. Throw an exception if it returns false.
			expressions.Add(callStartObject);

			var obj = Expression.Variable(objType);
			variables.Add(obj);

			if (objType.IsTuple())
			{
				var tupleMembers = GetTupleMembers(objType);

				//Create temporary variables to hold these during construction
				var itemVariables = new List<ParameterExpression>();
				itemVariables.AddRange(tupleMembers.Select(member => Expression.Parameter(member.Type)));

				//Get our values
				DeserialiseObjectFields(expressions, variables, obj, objType, deserialiser,
					(name, type) => Expression.Assign(itemVariables[int.Parse(name.Substring(4)) - 1], DeserialiseField(type, deserialiser)),
					tupleMembers);

				variables.AddRange(itemVariables);

				//Construct our tuple
				var tupleConstructor = objType.GetConstructor(objType.GetGenericArguments());
				expressions.Add(Expression.Assign(obj, Expression.New(tupleConstructor, itemVariables)));
			}
			else
			{
				expressions.Add(Expression.Assign(obj, Expression.New(objType)));

				DeserialiseObjectFields(expressions, variables, obj, objType, deserialiser,
					(name, type) => Expression.Assign(Expression.PropertyOrField(obj, name), DeserialiseField(type, deserialiser)));
			}

			//End it.
			expressions.Add(callEndObject);

			//Return obj
			expressions.Add(Expression.Label(endLabel, obj));
			return Expression.Lambda<Func<DeserialiserBase, object>>(Expression.Block(variables, expressions), deserialiser).Compile();
		}

		public static T Deserialise<T>(DeserialiserBase deserialiser) { return (T)Deserialise(deserialiser, typeof(T)); }
		public static object Deserialise(DeserialiserBase deserialiser, Type type)
		{
			return getDeserialiser(type)(deserialiser);
		}
	}
}

//GetNextFieldName null = no more fields
//	GetIntValue
//	GetStringValue
//	GetDoubleValue
//  StartArray
//		HasNextElement
//		GetIntValue/etc
//	EndArray
//	StartObject
//	EndObject