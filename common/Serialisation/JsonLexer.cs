﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	class JsonLexer
	{
		public enum TokenType
		{
			Number,
			String,
			Identifier,
			LParen,
			RParen,
			LCurl,
			RCurl,
			LSquare,
			RSquare,
			Colon,
			Comma,
			EndOfStream,
			Invalid
		}

		public class Token
		{
			public int Line, Column;
			public string Text;
			public TokenType Type;
		}

		public int Line, Column;
		TextReader tr;

		private static readonly Tuple<char, TokenType>[] operators = new[] {
			Tuple.Create('(', TokenType.LParen),
			Tuple.Create(')', TokenType.RParen),
			Tuple.Create('{', TokenType.LCurl),
			Tuple.Create('}', TokenType.RCurl),
			Tuple.Create('[', TokenType.LSquare),
			Tuple.Create(']', TokenType.RSquare),
			Tuple.Create(':', TokenType.Colon),
			Tuple.Create(',', TokenType.Comma)
		};
		private static readonly Tuple<char, char>[] escapes = new[] {
			Tuple.Create('\\', '\\'),
			Tuple.Create('"', '"'),
			Tuple.Create('\'', '\''),
			Tuple.Create('t', '\t'),
			Tuple.Create('r', '\r'),
			Tuple.Create('n', '\n')
		};

		public JsonLexer(TextReader tr)
		{
			this.tr = tr;
			Line = 1;
			Column = 0;
		}
		
		private Token EndOfStream { get { return new Token { Line = Line, Column = Column, Type = TokenType.EndOfStream }; } }
		private Token Error { get { return new Token { Line = Line, Column = Column, Type = TokenType.Invalid }; } }
		private Token Operator(TokenType type) { return new Token { Line = Line, Column = Column - 1, Type = type }; }

		bool isIdentifierChar(char c, bool first = false) { return c == '_' || c == '$' || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (!first && c >= '0' && c <= '9'); }
		bool isNumberChar(char c, bool first = false) { return (first && c == '-') || (c >= '0' && c <= '9') || (!first && c == '.'); }

		char putBackChar = '\0';


		StringBuilder buff = new StringBuilder(64);
		public Token LexToken()
		{
			int next = putBackChar == '\0' ? tr.Read() : putBackChar;
			putBackChar = '\0';

			if (next == -1) return EndOfStream;

			while (char.IsWhiteSpace((char)next))
			{
				if ((char)next == '\n') Line++;
				next = tr.Read();
				if (next == -1) return EndOfStream;
			}

			char c = (char)next;

			//We're now at the first non-whitespace character.
			for (int i = 0; i < operators.Length; i++)
				if (operators[i].Item1 == c)
					return Operator(operators[i].Item2);

			//Position of this char, this is the start of the token we will return.
			int line = Line, col = Column - 1;

			//It's either a number, identifier or string
			buff.Clear();
			TokenType type;
			if (c == '"')
			{
				type = TokenType.String;
				next = tr.Read();
				if (next == -1) return Error; //EndOfStream = invalid (unclosed quote).
				c = (char)next;
				while (c == '\\')
				{
					next = tr.Read();
					if (next == -1) return Error; //EndOfStream = invalid (unclosed quote).
					c = (char)next;
					for (int i = 0; i < escapes.Length; i++)
						if (escapes[i].Item1 == c)
						{
							buff.Append(escapes[i].Item2);
							break;
						}

					next = tr.Read();
					if (next == -1) return Error; //EndOfStream = invalid (unclosed quote).
					c = (char)next;
				}

				while (c != '"')
				{
					buff.Append(c);
					next = tr.Read();
					if (next == -1) return Error; //EndOfStream = invalid (unclosed quote).
					c = (char)next;
					bool hadEscape = false;
					while (c == '\\')
					{
						hadEscape = true;
						if (next == -1) return Error; //EndOfStream = invalid (unclosed quote).
						c = (char)next;
						for (int i = 0; i < escapes.Length; i++)
							if (escapes[i].Item1 == c)
							{
								buff.Append(escapes[i].Item2);
								break;
							}

						next = tr.Read();
						if (next == -1) return Error; //EndOfStream = invalid (unclosed quote).
						c = (char)next;
					}
					if (hadEscape && c != '"') buff.Append(c);
				}
				//Don't put back closing quote, but don't push it onto buff yet.
			}
			else if (isNumberChar(c, true))
			{
				type = TokenType.Number;
				do
				{
					buff.Append(c);
					next = tr.Read();
					if (next == -1) break; //EndOfStream = valid end of number.
					c = (char)next;
				} while (isNumberChar(c));
				//c is not an number char. Put it back
				putBackChar = c;
			}
			else if (isIdentifierChar(c, true))
			{
				type = TokenType.Identifier;
				do
				{
					buff.Append(c);
					next = tr.Read();
					if (next == -1) break; //EndOfStream = valid end of identifier.
					c = (char)next;
				} while (isIdentifierChar(c));
				//c is not an identifier char. Put it back
				putBackChar = c;
			}
			else
			{
				return new Token { Line = Line, Column = Column, Type = TokenType.Invalid };
			}

			return new Token { Line = line, Column = col, Text = buff.ToString(), Type = type };
		}

		public void Close() { tr.Close(); }
	}
}
