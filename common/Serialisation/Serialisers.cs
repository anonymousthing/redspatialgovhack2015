﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	public abstract class SerialiserBase
	{
		public bool SupportsRecursiveObjects { get; protected set; }

		/// <summary>
		/// Adds a field to the current object or array.
		/// If this is an array member, name will be null.
		/// </summary>
		/// <param name="name">May be null</param>
		/// <param name="value"></param>
		public abstract void AddField(string name, string value);
		public abstract void AddField(string name, sbyte value);
		public abstract void AddField(string name, int value);
		public abstract void AddField(string name, long value);
		public abstract void AddField(string name, short value);
		public abstract void AddField(string name, byte value);
		public abstract void AddField(string name, uint value);
		public abstract void AddField(string name, ulong value);
		public abstract void AddField(string name, ushort value);
		public abstract void AddField(string name, float value);
		public abstract void AddField(string name, double value);
		public abstract void AddField(string name, decimal value);
		public abstract void AddField(string name, bool value);
		public abstract void AddField(string name, Enum value);

		public abstract void AddFieldNull<T>(string name);

		/// <summary>
		/// Don't serialise the array here. Each member will be serialised on its own.
		/// </summary>
		public abstract void StartArray<T>(string name, T[] array);
		public abstract void EndArray<T>(string name, T[] array);

		/// <summary>
		/// Don't serialise the list here. Each member will be serialised on its own.
		/// </summary>
		public abstract void StartList<T>(string name, List<T> list);
		public abstract void EndList<T>(string name, List<T> list);

		/// <summary>
		/// If this function returns false, the current object is exited immediately.
		/// </summary>
		public abstract bool StartObject<T>(string name, T obj);
		public abstract void EndObject<T>(string name, T obj);

		public abstract void Close();
	}

	public abstract class DeserialiserBase
	{
		public abstract int Line { get; }
		public abstract int Column { get; }

		public abstract bool NextValueIsNull();

		/// <summary>
		/// Gets a value from the deserialiser.
		/// Out parameters are to make life easier
		/// </summary>
		public abstract void GetValue(out string value);
		public abstract void GetValue(out sbyte? value);
		public abstract void GetValue(out int? value);
		public abstract void GetValue(out long? value);
		public abstract void GetValue(out short? value);
		public abstract void GetValue(out byte? value);
		public abstract void GetValue(out uint? value);
		public abstract void GetValue(out ulong? value);
		public abstract void GetValue(out ushort? value);
		public abstract void GetValue(out float? value);
		public abstract void GetValue(out double? value);
		public abstract void GetValue(out decimal? value);
		public abstract void GetValue(out bool? value);

		/// <summary>
		/// Used by object to see if there is another field to consume.
		/// </summary>
		public abstract string GetNextFieldName();

		/// <summary>
		/// Return false if not array (or if not of T)
		/// </summary>
		public abstract bool StartArray<T>();
		public abstract void EndArray<T>();

		/// <summary>
		/// Return false if not list (or if not of T)
		/// </summary>
		public abstract bool StartList<T>();
		public abstract void EndList<T>();

		public abstract bool StartDictionary<TKey, TValue>();
		public abstract void EndDictionary<TKey, TValue>();

		/// <summary>
		/// Used by array, list, & dictionary to see if there is another value that can be consumed.
		/// </summary>
		public abstract bool HasNextValue();

		/// <summary>
		/// Return false if not object (or if not T)
		/// </summary>
		public abstract bool StartObject<T>();
		public abstract void EndObject<T>();

		public abstract void Close();
	}

	public interface ISerialiserFactory
	{
		SerialiserBase ToFile(string filename);
		SerialiserBase ToStream(Stream stream);
		SerialiserBase ToTextWriter(TextWriter tw);
	}
	public interface IDeserialiserFactory
	{
		DeserialiserBase FromFile(string filename);
		DeserialiserBase FromStream(Stream stream);
		DeserialiserBase FromTextReader(TextReader tr);
	}
	public interface ISerialiserDeserialiseFactor : ISerialiserFactory, IDeserialiserFactory { }

	public enum SerialiserType { JSON, BSON, XML }
	public class Serialiser
	{
		SerialiserBase Inner;
		protected StringWriter sw { get; private set; }

		public Serialiser(SerialiserType type, TextWriter destination) { Init(type, destination); }
		public Serialiser(SerialiserType type, Stream destination) { Init(type, new StreamWriter(destination)); }
		public Serialiser(SerialiserType type, string filename) { Init(type, new StreamWriter(filename)); }

		/// <summary>
		/// Initialises the Serialiser with a StringWriter, available as sw.
		/// </summary>
		/// <param name="type"></param>
		protected Serialiser(SerialiserType type) { Init(type, sw = new StringWriter(new StringBuilder(1024), CultureInfo.InvariantCulture)); }

		private void Init(SerialiserType type, TextWriter tw)
		{
			switch (type)
			{
			case SerialiserType.JSON:
				Inner = new Serialisers.JSONSerialiser(tw);
				break;
			default:
				throw new Exception("Unknown serialiser type");
			}
		}

		public void Add(object value) { Serialisation2.Serialise(value, Inner); }

		public void Close() { Inner.Close(); }
	}

	public class StringSerialiser : Serialiser
	{
		public StringSerialiser(SerialiserType type) : base(type) { }

		public static string Serialise(SerialiserType type, object obj)
		{
			var serialiser = new StringSerialiser(type);
			serialiser.Add(obj);
			serialiser.Close();
			return serialiser.ToString();
		}

		public override string ToString() { return sw.ToString(); }
	}

	public class Deserialiser
	{
		DeserialiserBase Inner;

		public Deserialiser(SerialiserType type, TextReader source) { Init(type, source); }
		public Deserialiser(SerialiserType type, Stream source) { Init(type, new StreamReader(source)); }
		public Deserialiser(SerialiserType type, string str) { Init(type, new StringReader(str)); }
		private Deserialiser(SerialiserType type, bool dummy, string filename) { Init(type, new StreamReader(filename)); }
		public static Deserialiser FromFile(SerialiserType type, string filename) { return new Deserialiser(type, false, filename); }

		private void Init(SerialiserType type, TextReader tr)
		{
			switch (type)
			{
			case SerialiserType.JSON:
				Inner = new Serialisers.JSONDeserialiser(tr);
				break;
			default:
				throw new Exception("Unknown deserialiser type");
			}
		}

		public static T Deserialise<T>(SerialiserType type, string str)
		{
			var deserialiser = new Deserialiser(type, str);
			T value = deserialiser.Next<T>();
			deserialiser.Close();
			return value;
		}
		public static T Deserialise<T>(SerialiserType type, TextReader source)
		{
			var deserialiser = new Deserialiser(type, source);
			T value = deserialiser.Next<T>();
			deserialiser.Close();
			return value;
		}
		public static T Deserialise<T>(SerialiserType type, Stream source)
		{
			var deserialiser = new Deserialiser(type, source);
			T value = deserialiser.Next<T>();
			deserialiser.Close();
			return value;
		}

		public static object Deserialise(SerialiserType type, Type objType, string str)
		{
			var deserialiser = new Deserialiser(type, str);
			object value = deserialiser.Next(objType);
			deserialiser.Close();
			return value;
		}
		public static object Deserialise(SerialiserType type, Type objType, TextReader source)
		{
			var deserialiser = new Deserialiser(type, source);
			object value = deserialiser.Next(objType);
			deserialiser.Close();
			return value;
		}
		public static object Deserialise(SerialiserType type, Type objType, Stream source)
		{
			var deserialiser = new Deserialiser(type, source);
			object value = deserialiser.Next(objType);
			deserialiser.Close();
			return value;
		}

		public T Next<T>() { return Serialisation2.Deserialise<T>(Inner); }
		public object Next(Type type) { return Serialisation2.Deserialise(Inner, type); }

		public void Close() { Inner.Close(); }
	}

	public static class Serialisers
	{
		/// <summary>
		/// TODO: Make it configurable.
		/// </summary>
		class DeserialiserSettings
		{
			public enum ErrorType { Ignore, Error }
			public enum Typing { ExactOrError, ExactOrNull, CoerceOrError, CoerceOrNull }

			//ErrorType MissingFields = ErrorType.Ignore;
			//ErrorType OutOfOrderFields = ErrorType.Ignore;
			//Typing FieldTypeMismatch = Typing.ExactOrError;
			//ErrorType DuplicateFields = ErrorType.Ignore;
		}

		public static ISerialiserDeserialiseFactor JSON { get { return new JSONFactory(); } }

		class JSONFactory : ISerialiserDeserialiseFactor
		{
			public SerialiserBase ToFile(string filename) { return new JSONSerialiser(new StreamWriter(filename)); }
			public SerialiserBase ToStream(Stream stream) { return new JSONSerialiser(new StreamWriter(stream)); }
			public SerialiserBase ToTextWriter(TextWriter tw) { return new JSONSerialiser(tw); }

			public DeserialiserBase FromFile(string filename) { return new JSONDeserialiser(new StreamReader(filename)); }
			public DeserialiserBase FromStream(Stream stream) { return new JSONDeserialiser(new StreamReader(stream)); }
			public DeserialiserBase FromTextReader(TextReader tr) { return new JSONDeserialiser(tr); }
		}

		public class JSONSerialiser : SerialiserBase
		{
			TextWriter tw;
			int indent = 0;
			bool needsCommaNewLine = false;

			HashSet<object> seenObjects = new HashSet<object>();

			public JSONSerialiser(TextWriter textWriter)
			{
				tw = textWriter;
				SupportsRecursiveObjects = false;
			}

			private void writeField(string name, string value)
			{
				checkNeedsComma();
				if (name != null)
				{
					tw.WriteQuoted(name);
					tw.Write(": ");
				}
				tw.Write(value);
				needsCommaNewLine = true;
			}

			private void checkNeedsComma()
			{
				if (needsCommaNewLine)
				{
					needsCommaNewLine = false;
					tw.WriteLine(",");
				}
			}

			public override void AddField(string name, string value)
			{
				checkNeedsComma();
				if (name != null)
				{
					tw.WriteQuoted(name);
					tw.Write(": ");
				}
				tw.WriteQuoted(value);
				needsCommaNewLine = true;
			}
			
			public override void AddField(string name, sbyte value) { writeField(name, value.ToStringQuick()); }
			public override void AddField(string name, int value) { writeField(name, value.ToStringQuick()); }
			public override void AddField(string name, long value) { writeField(name, value.ToStringQuick()); }
			public override void AddField(string name, short value) { writeField(name, value.ToStringQuick()); }
			public override void AddField(string name, byte value) { writeField(name, value.ToStringQuick()); }
			public override void AddField(string name, uint value) { writeField(name, value.ToStringQuick()); }
			public override void AddField(string name, ulong value) { writeField(name, value.ToStringQuick()); }
			public override void AddField(string name, ushort value) { writeField(name, value.ToStringQuick()); }
			public override void AddField(string name, float value) { writeField(name, value.ToString()); }
			public override void AddField(string name, double value) { writeField(name, value.ToString()); }
			public override void AddField(string name, decimal value) { writeField(name, value.ToString()); }
			public override void AddField(string name, bool value) { writeField(name, value.ToStringQuick()); }
			public override void AddField(string name, Enum value) {  }
			public override void AddFieldNull<T>(string name) { writeField(name, "null"); }

			public override void StartArray<T>(string name, T[] array)
			{
				checkNeedsComma();
				if (array == null)
					tw.WriteLine(name == null ? "null" : "{0}: null,", name.Quote());
				else
					tw.WriteLine(name == null ? "[" : "{0}: [", name.Quote());
				indent++;
			}
			public override void EndArray<T>(string name, T[] array)
			{
				indent--;
				if (array != null)
				{
					tw.Write("]");
					needsCommaNewLine = true;
				}
			}

			public override void StartList<T>(string name, List<T> list)
			{
				checkNeedsComma();
				if (list == null)
				{
					tw.Write(name == null ? "null" : "{0}: null", name.Quote());
					needsCommaNewLine = true;
				}
				else
					tw.WriteLine(name == null ? "[" : "{0}: [", name.Quote());
				indent++;
			}

			public override void EndList<T>(string name, List<T> list)
			{
				indent--;
				if (list != null)
				{
					tw.Write("]");
					needsCommaNewLine = true;
				}
			}

			public override bool StartObject<T>(string name, T obj)
			{
				checkNeedsComma();
				if (seenObjects.Contains(obj))
					throw new Exception("Cannot serialise recursive objects");
				seenObjects.Add(obj);

				if (name != null)
				{
					tw.WriteQuoted(name);
					tw.Write(": ");
				}

				if (obj == null)
				{
					tw.Write("null");
					needsCommaNewLine = true;
				}
				else
					tw.WriteLine('{');

				indent++;
				return true;
			}
			public override void EndObject<T>(string name, T obj)
			{
				indent--;
				if (obj != null)
				{
					tw.WriteLine();
					tw.Write("}");
					needsCommaNewLine = true;
				}
			}

			public override void Close() { tw.Close(); }
		}

		public class JSONDeserialiser : DeserialiserBase
		{
			enum TokenType
			{
				Number,
				Identifier,
				String,
				Colon,
				Comma,
				LParen, RParen,
				LSquare, RSquare,
				LCurl, RCurl,
				Invalid
			}

			TextReader tr;
			JsonLexer lexer;

			/// <summary>
			/// We saw we've seen one to start with, because we don't expect to see one
			/// before the first global object.
			/// </summary>
			bool sawComma = true;
			bool justEnteredNull = false;

			/// <summary>
			/// Determines the current parser scope.
			/// 0 = object, 1 = dictionary, 2 = global, 3 = array, 4 = list
			/// 2+ are anonymous scopes, 0-1 are not.
			/// Ex: An array is an anonymous scope, as each value is unnamed.
			/// An object is not an anonymous scope, because each value is named.
			/// This is used to determine if we should throw an error about a missing comma,
			/// depending on the value of sawComma. If anonymous-scope && !sawComma, error.
			/// </summary>
			Stack<ScopeType> scopeType = new Stack<ScopeType>();
			enum ScopeType { Object = 0, Dictionary = 1, Global = 2, Array = 3, List = 4 }

			public JSONDeserialiser(TextReader textReader)
			{
				tr = textReader;
				scopeType.Push(ScopeType.Global);
				init();
			}

			void init()
			{
				lexer = new JsonLexer(tr);
			}

			public override int Line { get { return lexer.Line; } }
			public override int Column { get { return lexer.Column; } }

			bool nextTokenIsNullOrUndefined(bool consumeIfTrue = false)
			{
				var token = peek();
				var result = (token.Type == JsonLexer.TokenType.Identifier && (token.Text == "null" || token.Text == "undefined"));
				if (result && consumeIfTrue)
					next();
				return result;
			}

			JsonLexer.Token peeked = null;
			JsonLexer.Token peek()
			{
				if (peeked != null)
					return peeked;
				return peeked = lexer.LexToken();
			}
			JsonLexer.Token next()
			{
				if (peeked != null)
				{
					var token = peeked;
					peeked = null;
					return token;
				}
				return lexer.LexToken();
			}

			void updateSawComma()
			{
				if (peek().Type == JsonLexer.TokenType.Comma)
				{
					next();
					sawComma = true;
				}
				else
					sawComma = false;
			}

			void checkOkToGetValue()
			{
				if ((scopeType.Peek() != ScopeType.Object && scopeType.Peek() != ScopeType.Dictionary) && !sawComma)
					throw new Exception("Expected comma");
			}

			public override bool NextValueIsNull()
			{
				checkOkToGetValue();
				return nextTokenIsNullOrUndefined(false);
			}

			public override void GetValue(out string value)
			{
				checkOkToGetValue();
				if (nextTokenIsNullOrUndefined(true))
					value = null;
				else
				{
					var token = next();
					value = token.Text;
				}
				updateSawComma();
			}

			delegate bool TryParse<T>(string s, out T value);
			private T? getValue<T>(TryParse<T> TryParse, JsonLexer.TokenType expected = JsonLexer.TokenType.Number)
				where T : struct
			{
				checkOkToGetValue();
				T? value;
				if (nextTokenIsNullOrUndefined(true))
					value = null;
				else
				{
					var token = next();
					T parsed;
					value = (token.Type == expected && TryParse(token.Text, out parsed)) ? (T?)parsed : null;
				}
				updateSawComma();
				return value;
			}

			public override void GetValue(out sbyte? value) { value = getValue<sbyte>(StringUtil.TryParseSbyte); }
			public override void GetValue(out short? value) { value = getValue<short>(StringUtil.TryParseShort); }
			public override void GetValue(out int? value) { value = getValue<int>(StringUtil.TryParseInt); }
			public override void GetValue(out long? value) { value = getValue<long>(StringUtil.TryParseLong); }
			public override void GetValue(out byte? value) { value = getValue<byte>(byte.TryParse); }
			public override void GetValue(out ushort? value) { value = getValue<ushort>(StringUtil.TryParseUShort); }
			public override void GetValue(out uint? value) { value = getValue<uint>(StringUtil.TryParseUInt); }
			public override void GetValue(out ulong? value) { value = getValue<ulong>(StringUtil.TryParseULong); }
			public override void GetValue(out float? value) { value = getValue<float>(StringUtil.TryParseFloat); }
			public override void GetValue(out double? value) { value = getValue<double>(StringUtil.TryParseDouble); }
			public override void GetValue(out decimal? value) { value = getValue<decimal>(decimal.TryParse); }
			public override void GetValue(out bool? value) { value = getValue<bool>(bool.TryParse, JsonLexer.TokenType.Identifier); }

			public override string GetNextFieldName()
			{
				var token = peek();
				if (token.Type != JsonLexer.TokenType.String || !sawComma)
					return null;
				next();

				//Consume the colon.
				if (peek().Type == JsonLexer.TokenType.Colon)
					next();

				sawComma = false;
				return token.Text.Unquote();
			}

			public override bool HasNextValue()
			{
				//No comma, no value.
				if (!sawComma)
					return false;

				//However having a comma doesn't mean we have a value, check our token type.
				var token = peek();
				var validTokens = new[] { JsonLexer.TokenType.Identifier, JsonLexer.TokenType.Number, JsonLexer.TokenType.String, JsonLexer.TokenType.LCurl, JsonLexer.TokenType.LSquare };
				return validTokens.Contains(token.Type);
			}

			private bool tryEnterScope(ScopeType scope, JsonLexer.TokenType expected, bool allowNull = true)
			{
				scopeType.Push(scope);

				if (peek().Type != expected)
				{
					var text = peek().Text;
					if (allowNull && (text == "null" || text == "undefined"))
					{
						justEnteredNull = true;
						next();
						return true;
					}
					return false;
				}

				next();
				sawComma = true;
				return true;
			}

			private void leaveScope(ScopeType scope, JsonLexer.TokenType expected)
			{
				if (scopeType.Pop() != scope)
					throw new Exception("Deserialisation control code called us incorrectly.");

				if (peek().Type != expected && !justEnteredNull)
					throw new Exception("End of " + scope.ToString() + " expected.");

				if (!justEnteredNull)
					next();
				justEnteredNull = false;
				updateSawComma();
			}

			public override bool StartArray<T>() { return tryEnterScope(ScopeType.Array, JsonLexer.TokenType.LSquare); }
			public override void EndArray<T>() { leaveScope(ScopeType.Array, JsonLexer.TokenType.RSquare); }
			public override bool StartList<T>() { return tryEnterScope(ScopeType.List, JsonLexer.TokenType.LSquare); }
			public override void EndList<T>() { leaveScope(ScopeType.List, JsonLexer.TokenType.RSquare); }
			public override bool StartDictionary<TKey, TValue>() { return tryEnterScope(ScopeType.Dictionary, JsonLexer.TokenType.LCurl); }
			public override void EndDictionary<TKey, TValue>() { leaveScope(ScopeType.Dictionary, JsonLexer.TokenType.RCurl); }
			public override bool StartObject<T>() { return tryEnterScope(ScopeType.Object, JsonLexer.TokenType.LCurl); }
			public override void EndObject<T>() { leaveScope(ScopeType.Object, JsonLexer.TokenType.RCurl); }

			public override void Close() { lexer.Close(); }
		}
	}
}
