﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
	public static class StringUtil
	{

		public static string EscapeQuotesAndSlashes(this string s)
		{
			return s.Replace("\\", "\\\\").Replace("\"", "\\\"");
		}
		public static string UnescapeQuotesAndSlashes(this string s)
		{
			return s.Replace("\\\"", "\"").Replace("\\\\", "\\");
		}

		/// <summary>
		/// Escapes quotes & slashes and surrounds the given string in quotes.
		/// </summary>
		public static string Quote(this string s) { return s == null ? null : ("\"" + EscapeQuotesAndSlashes(s) + "\""); }

		/// <summary>
		/// Unescapes quotes & slashes and removes surrounding quotes from the given string. Does nothing if the string doesn't
		/// start & end with quotes.
		/// </summary>
		public static string Unquote(this string s)
		{
			if (s.Length < 2)
				return s;
			if (s[0] != '"' || s[s.Length - 1] != '"')
				return s;

			return UnescapeQuotesAndSlashes(s.Substring(1, s.Length - 2));
		}


		//The below functions are just 'ToString' functions for int/short/etc.
		//They have been tested to be faster than the framework versions.
		//They all use the Invariant Culture, with no formatting options available.
		//They are recommended only for use when profiling has revealed that the framework
		//methods are causing a slowdown. Serialisation uses them because this was
		//revealed to be the case in some instances.
		#region ToStringQuick

		static int toCharBuff(char[] chars, int chari, uint value)
		{
			if (value == 0)
				chars[--chari] = '0';
			else
			{
				do
					chars[--chari] = (char)('0' + (value % 10));
				while ((value /= 10) > 0);
			}
			return chari;
		}

		static int toCharBuff(char[] chars, int chari, ulong value)
		{
			if (value == 0)
				chars[--chari] = '0';
			else
			{
				do
					chars[--chari] = (char)('0' + (value % 10));
				while ((value /= 10) > 0);
			}
			return chari;
		}

		public static string ToStringQuick(this sbyte value)
		{
			//-128
			const int MaxLen = 4;
			char[] chars = new char[MaxLen];
			int chari = MaxLen;

			if (value < 0)
			{
				chari = toCharBuff(chars, chari, (uint)(-value));
				chars[--chari] = '-';
			}
			else
				chari = toCharBuff(chars, chari, (uint)value);

			return new string(chars, chari, MaxLen - chari);
		}

		public static string ToStringQuick(this short value)
		{
			//-32000
			const int MaxLen = 6;
			char[] chars = new char[MaxLen];
			int chari = MaxLen;

			if (value < 0)
			{
				chari = toCharBuff(chars, chari, (uint)(-value));
				chars[--chari] = '-';
			}
			else
				chari = toCharBuff(chars, chari, (uint)value);

			return new string(chars, chari, MaxLen - chari);
		}

		/// <summary>
		/// Faster than the framework version by > 30% in a release build.
		/// No formatting options. Invariant.
		/// </summary>
		public static string ToStringQuick(this int value)
		{
			//-2000000000
			const int MaxLen = 11;
			char[] chars = new char[MaxLen];
			int chari = MaxLen;

			if (value < 0)
			{
				chari = toCharBuff(chars, chari, (uint)(-value));
				chars[--chari] = '-';
			}
			else
				chari = toCharBuff(chars, chari, (uint)value);

			return new string(chars, chari, MaxLen - chari);
		}

		public static string ToStringQuick(this long value)
		{
			//-9223IcantRemember..
			const int MaxLen = 20;
			char[] chars = new char[MaxLen];
			int chari = MaxLen;

			if (value < 0)
			{
				chari = toCharBuff(chars, chari, (ulong)(-value));
				chars[--chari] = '-';
			}
			else
				chari = toCharBuff(chars, chari, (ulong)value);

			return new string(chars, chari, MaxLen - chari);
		}

		public static string ToStringQuick(this byte value)
		{
			//255
			const int MaxLen = 3;
			char[] chars = new char[MaxLen];
			int chari = MaxLen;

			if (value == 0)
				chars[--chari] = '0';
			else
			{
				do
					chars[--chari] = (char)('0' + (value % 10));
				while ((value /= 10) > 0);
			}

			return new string(chars, chari, MaxLen - chari);
		}

		public static string ToStringQuick(this ushort value)
		{
			//65535
			const int MaxLen = 5;
			char[] chars = new char[MaxLen];
			int chari = MaxLen;

			if (value == 0)
				chars[--chari] = '0';
			else
			{
				do
					chars[--chari] = (char)('0' + (value % 10));
				while ((value /= 10) > 0);
			}

			return new string(chars, chari, MaxLen - chari);
		}

		/// <summary>
		/// Faster than the framework version by > 30% in a release build.
		/// No formatting options. Invariant.
		/// </summary>
		public static string ToStringQuick(this uint value)
		{
			//4000000000
			char[] chars = new char[10];
			int chari = 10;

			if (value == 0)
				chars[--chari] = '0';
			else
			{
				while (value > 0)
				{
					chars[--chari] = (char)('0' + (value % 10));
					value /= 10;
				}
			}

			return new string(chars, chari, 10 - chari);
		}

		public static string ToStringQuick(this ulong value)
		{
			//18446744073709551616
			const int MaxLen = 20;
			char[] chars = new char[MaxLen];
			int chari = MaxLen;

			if (value == 0)
				chars[--chari] = '0';
			else
			{
				do
					chars[--chari] = (char)('0' + (value % 10));
				while ((value /= 10) > 0);
			}

			return new string(chars, chari, MaxLen - chari);
		}
		
		public static string ToStringQuick(this bool value)
		{
			return value ? "true" : "false";
		}

		public static bool TryParseSbyte(string s, out sbyte result)
		{
			result = 0;

			if (s.Length == 0) return false;

			int chari = -1;
			const int maxLen = 3;
			bool negative = false;
			if (s[0] == '-')
			{
				negative = true;
				chari++;
			}

			int max = maxLen + (negative ? 1 : 0);
			if (s.Length >= max) //Avoid extra branch in common case of < 100
			{
				if (s.Length == max) //Right on the border
				{
					//Don't worry about if this isn't a number -- that case will be picked up in the loop below.
					if (s[chari + 1] > '1') return false; //Too large -- we may be unable to parse as byte
				}
				else return false; //Too long
			}

			byte value = 0;
			max = Math.Min(max, s.Length) - 1;
			for (; chari < max; )
			{
				char c = s[++chari];
				byte n = (byte)(c - '0');
				if (n > 9) return false; //Bad char
				value = (byte)(value * (byte)10 + n);
			}

			result = negative ? (sbyte)-(sbyte)value : (sbyte)value;

			//Bounds check
			byte valueMax = (byte)(negative ? 128 : 127);
			return value <= valueMax; //Range check
		}

		public static bool TryParseShort(string s, out short result)
		{
			result = 0;

			if (s.Length == 0) return false;

			int chari = -1;
			const int maxLen = 5;
			bool negative = false;
			if (s[0] == '-')
			{
				negative = true;
				chari++;
			}

			int max = maxLen + (negative ? 1 : 0);
			if (s.Length >= max) //Avoid extra branch in common case of < 10,000
			{
				if (s.Length == max) //Right on the border
				{
					//Don't worry about if this isn't a number -- that case will be picked up in the loop below.
					if (s[chari + 1] > '3') return false; //Too large -- we may be unable to parse as ushort
				}
				else return false; //Too long
			}

			ushort value = 0;
			max = Math.Min(max, s.Length) - 1;
			for (; chari < max; )
			{
				char c = s[++chari];
				ushort n = (ushort)(c - '0');
				if (n > 9) return false; //Bad char
				value = (ushort)(value * (ushort)10 + n);
			}

			result = negative ? (short)-(short)value : (short)value;

			//Bounds check
			ushort valueMax = (ushort)(negative ? 32768 : 32767);
			return value <= valueMax; //Range check
		}

		public static bool TryParseInt(string s, out int result)
		{
			result = 0;

			if (s.Length == 0) return false;

			int chari = -1;
			const int maxLen = 11;
			bool negative = false;
			if (s[0] == '-')
			{
				negative = true;
				chari++;
			}

			int max = maxLen + (negative ? 1 : 0);
			if (s.Length >= max) //Avoid extra branch in common case of < 1 billion
			{
				if (s.Length == max) //Right on the border
				{
					//Don't worry about if this isn't a number -- that case will be picked up in the loop below.
					if (s[chari + 1] > '2') return false; //Too large -- we may be unable to parse as uint
				}
				else return false; //Too long
			}

			uint value = 0;
			max = Math.Min(max, s.Length) - 1;
			for (; chari < max;)
			{
				char c = s[++chari];
				uint n = (uint)c - '0';
				if (n > 9) return false; //Bad char
				value = value * 10U + n;
			}

			result = negative ? -(int)value : (int)value;

			//Bounds check
			uint valueMax = negative ? 2147483648U : 2147483647U;
			return value <= valueMax; //Range check
		}

		public static bool TryParseLong(string s, out long result)
		{
			result = 0;

			if (s.Length == 0) return false;

			int chari = -1;
			const int maxLen = 19;
			bool negative = false;
			if (s[0] == '-')
			{
				negative = true;
				chari++;
			}

			int max = maxLen + (negative ? 1 : 0);
			//Don't need to check s.Length == max for long, as ulong can hold a whole extra digit.
			if (s.Length > max) return false; //Too long

			ulong value = 0;
			max = Math.Min(max, s.Length) - 1;
			for (; chari < max; )
			{
				char c = s[++chari];
				ulong n = (ulong)c - '0';
				if (n > 9) return false; //Bad char
				value = value * 10UL + n;
			}

			result = negative ? -(long)value : (long)value;

			//Bounds check
			ulong valueMax = negative ? 9223372036854775808UL : 9223372036854775807UL;
			return value <= valueMax; //Range check
		}

		public static bool TryParseUShort(string s, out ushort result)
		{
			result = 0;

			if (s.Length == 0) return false;

			int chari = -1;
			const int maxLen = 5;

			if (s.Length >= maxLen) //Avoid extra branch in common case of < 1 billion
			{
				if (s.Length == maxLen) //Right on the border
				{
					//Don't worry about if this isn't a number -- that case will be picked up in the loop below.
					if (s[chari + 1] > '6') return false; //Too large
				}
				else return false; //Too long
			}

			ushort value = 0;
			int max = Math.Min(maxLen, s.Length) - 1;
			for (; chari < max; )
			{
				char c = s[++chari];
				ushort n = (ushort)(c - '0');
				if (n > 9) return false; //Bad char
				value = (ushort)(value * 10 + n);
			}

			result = value;

			//Bounds check
			return s.Length != maxLen || value > 9999; //Range check
		}

		public static bool TryParseUInt(string s, out uint result)
		{
			result = 0;

			if (s.Length == 0) return false;

			int chari = -1;
			const int maxLen = 11;

			if (s.Length >= maxLen) //Avoid extra branch in common case of < 1 billion
			{
				if (s.Length == maxLen) //Right on the border
				{
					//Don't worry about if this isn't a number -- that case will be picked up in the loop below.
					if (s[chari + 1] > '4') return false; //Too large
				}
				else return false; //Too long
			}

			uint value = 0;
			int max = Math.Min(maxLen, s.Length) - 1;
			for (; chari < max; )
			{
				char c = s[++chari];
				uint n = (uint)c - '0';
				if (n > 9) return false; //Bad char
				value = value * 10U + n;
			}

			result = value;

			//Bounds check
			return s.Length != maxLen || value > 999999999 ; //Range check
		}

		public static bool TryParseULong(string s, out ulong result)
		{
			result = 0;

			if (s.Length == 0) return false;

			int chari = -1;
			const int maxLen = 20;

			if (s.Length >= maxLen) //Avoid extra branch in common case of < 1 billion
			{
				if (s.Length == maxLen) //Right on the border
				{
					//Don't worry about if this isn't a number -- that case will be picked up in the loop below.
					if (s[chari + 1] > '1') return false; //Too large
				}
				else return false; //Too long
			}

			ulong value = 0;
			int max = Math.Min(maxLen, s.Length) - 1;
			for (; chari < max; )
			{
				char c = s[++chari];
				ulong n = (ulong)(c - '0');
				if (n > 9) return false; //Bad char
				value = value * 10UL + n;
			}

			result = value;

			//Bounds check
			return s.Length != maxLen || value > 9999999999999999999; //Range check
		}

		/// <summary>
		/// Returns -1 on error, else returns decimal point index, or -2 for no decimnal point.
		/// </summary>
		/// <param name="i">The index to start from, normally 0 or 1 (for positive/negative numbers)</param>
		private static int grabFloatingPointIntParts(string s, int i, int[] parts, out int value, out int extraPartCount)
		{
			int len = s.Length;
			int chari = i;
			int integer = 0;
			value = extraPartCount = 0;

			int digitCount = 0;
			while (chari < len)
			{
				char c = s[chari++];
				int n = c - '0';
				if (n > 9 || n < 0)
				{
					if (c == '.') break; //Time for fractional part
					return -1; //Invalid character
				}

				if (digitCount == 9)
				{
					if (extraPartCount == parts.Length) return -1; //Too large

					parts[extraPartCount++] = integer;
					integer = 0;
					digitCount = 0;
				}

				integer = integer * 10 + n;
				digitCount++;
			}

			value = integer;
			return chari - 1;
		}

		/// <summary>
		/// Returns -1 on error, else returns decimal point index, or -2 for no decimnal point.
		/// </summary>
		/// <param name="i">The index to start from, normally 0 or 1 (for positive/negative numbers)</param>
		private static bool grabFloatingPointFractionalParts(string s, int i, int[] parts, out int value, out int extraPartCount, out int totalDigits)
		{
			int len = s.Length;
			int chari = i;
			int fraction = 0;
			value = extraPartCount = totalDigits = 0;

			//Grab fractional part
			int digitCount = 0, totalDigitCount = 0;
			while (chari < len)
			{
				char c = s[chari++];
				int n = c - '0';
				if (n > 9 || n < 0) return false; //Invalid character

				if (digitCount == 9)
				{
					if (extraPartCount == parts.Length) return false; //Too large

					parts[extraPartCount++] = fraction;
					fraction = 0;
					digitCount = 0;
				}

				fraction = fraction * 10 + n;
				digitCount++;
				totalDigitCount++;
			}

			totalDigits = totalDigitCount;
			value = fraction;
			return true;
		}


		//SEVERE: See below
		//FIXME: Decimal/Double limits. Exponential notation.

		public static bool TryParseFloat(string s, out float result)
		{
			result = 0;

			int len = s.Length;
			if (len > 40 || len == 0) return false; //Bail
			int chari = 0;
			bool negative = false;
			if (s[0] == '-')
			{
				negative = true;
				chari++;
			}

			//We parse floating point numbers in two sections.
			//A pre-decimal point part, and a post-decimal point part.

			//We can have 37 significant digits. So we need 4 extra parts.
			//int = 9 significant digits. 4*9=36 + 'integer' = 45.
			const int MaxExtraParts = 4;

			int[] extraIntegerParts = new int[MaxExtraParts];
			int extraIntegerPartCount = 0;
			int integer = 0;

			int[] extraFractionalParts = new int[MaxExtraParts];
			int extraFractionalPartCount = 0;
			int fraction = 0;

			//Grab our parts.

			//Grab whole part
			int decimalPointIndex = grabFloatingPointIntParts(s, chari, extraIntegerParts, out integer, out extraIntegerPartCount);
			if (decimalPointIndex == -1)
				return false;

			int digitCount = 0;
			if (decimalPointIndex != -2)
			{
				if (!grabFloatingPointFractionalParts(s, decimalPointIndex + 1, extraFractionalParts, out fraction, out extraFractionalPartCount, out digitCount))
					return false;
			}

			//Now we have the parts, we need to assemble them.
			float value = 0;
			while (extraIntegerPartCount-- > 0)
			{
				int extra = extraIntegerParts[extraIntegerPartCount];
				value *= 1000000000.0f;
				value += (float)extra;
			}
			int mulCount = (negative ? decimalPointIndex - 1 : decimalPointIndex) % 9;
			for (int i = 0; i < mulCount; i++) value *= 10;
			value += integer;

			float numerator = 0;
			while (extraFractionalPartCount-- > 0)
			{
				int extra = extraFractionalParts[extraFractionalPartCount];
				numerator *= 1000000000.0f;
				numerator += (float)extra;
			}
			for (int i = 0; i < digitCount % 9; i++) numerator *= 10;
			if (digitCount % 9 == 0) numerator *= 1000000000.0f;
			numerator += fraction;
			for (int i = 0; i < digitCount / 3; i++) numerator /= 1000;
			for (int i = 0; i < digitCount % 3; i++) numerator /= 10;

			value += numerator;
			result = negative ? -value : value;
			return true;
		}

		public static bool TryParseDouble(string s, out double result)
		{
			result = 0;

			int len = s.Length;
			if (len > 40 || len == 0) return false; //Bail
			int chari = 0;
			bool negative = false;
			if (s[0] == '-')
			{
				negative = true;
				chari++;
			}

			//We parse floating point numbers in two sections.
			//A pre-decimal point part, and a post-decimal point part.

			//We can have 37 significant digits. So we need 4 extra parts.
			//int = 9 significant digits. 4*9=36 + 'integer' = 45.
			const int MaxExtraParts = 4;

			int[] extraIntegerParts = new int[MaxExtraParts];
			int extraIntegerPartCount = 0;
			int integer = 0;

			int[] extraFractionalParts = new int[MaxExtraParts];
			int extraFractionalPartCount = 0;
			int fraction = 0;

			//Grab our parts.

			//Grab whole part
			int decimalPointIndex = grabFloatingPointIntParts(s, chari, extraIntegerParts, out integer, out extraIntegerPartCount);
			if (decimalPointIndex == -1)
				return false;

			int digitCount = 0;
			if (decimalPointIndex != -2)
			{
				if (!grabFloatingPointFractionalParts(s, decimalPointIndex + 1, extraFractionalParts, out fraction, out extraFractionalPartCount, out digitCount))
					return false;
			}

			//Now we have the parts, we need to assemble them.
			double value = 0;
			while (extraIntegerPartCount-- > 0)
			{
				int extra = extraIntegerParts[extraIntegerPartCount];
				value *= 1000000000.0;
				value += (double)extra;
			}
			int mulCount = (negative ? decimalPointIndex - 1 : decimalPointIndex) % 9;
			for (int i = 0; i < mulCount; i++) value *= 10;
			value += integer;

			double numerator = 0;
			while (extraFractionalPartCount-- > 0)
			{
				int extra = extraFractionalParts[extraFractionalPartCount];
				numerator *= 1000000000.0;
				numerator += (double)extra;
			}
			for (int i = 0; i < digitCount % 9; i++) numerator *= 10;
			if (digitCount % 9 == 0) numerator *= 1000000000.0;
			numerator += fraction;
			for (int i = 0; i < digitCount / 3; i++) numerator /= 1000;
			for (int i = 0; i < digitCount % 3; i++) numerator /= 10;

			value += numerator;
			result = negative ? -value : value;
			return true;
		}

		public static bool TryParseDecimal(string s, out decimal result)
		{
			result = 0;

			int len = s.Length;
			if (len > 40 || len == 0) return false; //Bail
			int chari = 0;
			bool negative = false;
			if (s[0] == '-')
			{
				negative = true;
				chari++;
			}

			//We parse floating point numbers in two sections.
			//A pre-decimal point part, and a post-decimal point part.

			//We can have 37 significant digits. So we need 4 extra parts.
			//int = 9 significant digits. 4*9=36 + 'integer' = 45.
			const int MaxExtraParts = 4;

			int[] extraIntegerParts = new int[MaxExtraParts];
			int extraIntegerPartCount = 0;
			int integer = 0;

			int[] extraFractionalParts = new int[MaxExtraParts];
			int extraFractionalPartCount = 0;
			int fraction = 0;

			//Grab our parts.

			//Grab whole part
			int decimalPointIndex = grabFloatingPointIntParts(s, chari, extraIntegerParts, out integer, out extraIntegerPartCount);
			if (decimalPointIndex == -1)
				return false;

			int digitCount = 0;
			if (decimalPointIndex != -2)
			{
				if (!grabFloatingPointFractionalParts(s, decimalPointIndex + 1, extraFractionalParts, out fraction, out extraFractionalPartCount, out digitCount))
					return false;
			}

			//Now we have the parts, we need to assemble them.
			decimal value = 0;
			while (extraIntegerPartCount-- > 0)
			{
				int extra = extraIntegerParts[extraIntegerPartCount];
				value *= 1000000000.0m;
				value += (decimal)extra;
			}
			int mulCount = (negative ? decimalPointIndex - 1 : decimalPointIndex) % 9;
			for (int i = 0; i < mulCount; i++) value *= 10;
			value += integer;

			decimal numerator = 0;
			while (extraFractionalPartCount-- > 0)
			{
				int extra = extraFractionalParts[extraFractionalPartCount];
				numerator *= 1000000000.0m;
				numerator += (decimal)extra;
			}
			for (int i = 0; i < digitCount % 9; i++) numerator *= 10;
			if (digitCount % 9 == 0) numerator *= 1000000000.0m;
			numerator += fraction;
			for (int i = 0; i < digitCount / 3; i++) numerator /= 1000;
			for (int i = 0; i < digitCount % 3; i++) numerator /= 10;

			value += numerator;
			result = negative ? -value : value;
			return true;
		}


		#endregion
	}
}
