﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
	public static class IEnumerableHelpers
	{
		private class EnumerableEnumerator<T> : IEnumerable<T>
		{
			private System.Collections.IEnumerable enumerable;
			public EnumerableEnumerator(System.Collections.IEnumerable enumerable)
			{
				this.enumerable = enumerable;
			}

			public IEnumerator<T> GetEnumerator()
			{
				var enumerator = enumerable.GetEnumerator();
				while (enumerator.MoveNext())
					yield return (T)enumerator.Current;
				yield break;
			}

			System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
			{
				return GetEnumerator();
			}
		}
		
		/// <summary>
		/// Not sure why you'd use this one though...
		/// </summary>
		/// <returns>The original enumerable</returns>
		public static IEnumerable<T> ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
		{
			foreach (var item in enumerable)
				action(item);
			return enumerable;
		}

		public static IEnumerable<Tuple<T, U>> PairWith<T, U>(this IEnumerable<T> a, IEnumerable<U> b)
		{
			var ita = a.GetEnumerator();
			var itb = b.GetEnumerator();

			while (ita.MoveNext() && itb.MoveNext())
				yield return Tuple.Create(ita.Current, itb.Current);
		}

		public static void ForEachWith<T, U>(this IEnumerable<T> a, IEnumerable<U> b, Action<T, U> action)
		{
			var ita = a.GetEnumerator();
			var itb = b.GetEnumerator();

			while (ita.MoveNext() && itb.MoveNext())
				action(ita.Current, itb.Current);
		}

		public static IEnumerable<U> TrySelect<T, U>(this IEnumerable<T> enumerble, Func<T, U> selector)
		{
			foreach (var item in enumerble)
			{
				bool valid = true;
				U result = default(U);
				try { result = selector(item); }
				catch { valid = false; }
				if (valid) yield return result;
			}
			yield break;
		}
	}
}
