﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Common
{
	/// <summary>
	/// Makes Random static (thread-safe).
	/// Adds some useful helpers.
	/// </summary>
	public class RandomUtil
	{
		static ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random());
		
		public static int Next() { return random.Value.Next(); }
		public static int Next(int max) { return random.Value.Next(max); }
		public static int Next(int min, int max) { return random.Value.Next(min, max); }
	}
}
