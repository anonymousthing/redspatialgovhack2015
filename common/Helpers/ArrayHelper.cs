﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	public static class ArrayHelper
	{
		public static T[] Subarray<T>(this T[] array, int startIndex, int length)
		{
			T[] newArray = new T[length];

			for (int i = 0; i < length; i++)
				newArray[i] = array[i + startIndex];

			return newArray;
		}
	}
}
