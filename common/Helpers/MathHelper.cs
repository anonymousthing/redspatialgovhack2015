﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	public static class MathHelper
	{
		public static int Sqr(this int a) { return a * a; }
		public static float Sqr(this float a) { return a * a; }
		public static double Sqr(this double a) { return a * a; }
	}
}
