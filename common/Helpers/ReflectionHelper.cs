﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Linq.Expressions;

namespace Common
{
	public static class ReflectionHelper
	{
		/// <summary>A wrapper around Activator.CreateInstance</summary>
		public static object CreateInstance(Type t) { return Activator.CreateInstance(t); }

		public static bool IsList(this Type type)
		{
			return type.FullName.StartsWith("System.Collections.Generic.List`1");
		}

		public static bool IsTuple(this Type type)
		{
			return type.FullName.StartsWith("System.Tuple`");
		}

		static Type Nullable = typeof(System.Nullable<>);

		public static Type MakeNullable(this Type type)
		{
			//Classes are nullable.
			if (type.IsClass)
				return type;

			//Nullables are nullable.
			if (type.IsConstructedGenericType && type.GetGenericTypeDefinition() == Nullable)
				return type;

			return Nullable.MakeGenericType(type);
		}

		public static Type[] GetParameterTypes(this MethodInfo method)
		{
			ParameterInfo[] parameters = method.GetParameters();
			Type[] types = new Type[parameters.Length];

			for (int i = 0; i < parameters.Length; i++)
				types[i] = parameters[i].ParameterType;

			return types;
		}

		/// <summary>
		/// Returns an array of Types representing the parameters to a System.Predicate, System.Action, or System.Func.
		/// </summary>
		/// <param name="delegateType">An Action, Func, or Predicate type.</param>
		public static Type[] GetParameterTypesFromDelegate(Type delegateType)
		{
			if (delegateType.FullName.StartsWith("System.Action") || delegateType.FullName.StartsWith("System.Predicate"))
			{
				if (!delegateType.IsGenericType)
					return new Type[0];
				var genericTypes = delegateType.GetGenericArguments();
				return genericTypes;
			}
			else if (delegateType.FullName.StartsWith("System.Func"))
			{
				var genericTypes = delegateType.GetGenericArguments();
				return genericTypes.Subarray(0, genericTypes.Length - 1);
			}
			else
			{
				var method = delegateType.GetMethod("Invoke");
				return method.GetParameterTypes();
			}
			throw new ArgumentException("delegateType must be one of Action, Func or Predicate", "delegateType");
		}
		
		/// <summary>
		/// Returns a Type representing the return type of a System.Predicate, System.Action, or System.Func.
		/// </summary>
		/// <param name="delegateType">An Action, Func, or Predicate type.</param>
		public static Type GetReturnTypeFromDelegate(Type delegateType)
		{
			if (delegateType.FullName.StartsWith("System.Action"))
				return typeof(void);
			else if (delegateType.FullName.StartsWith("System.Predicate"))
				return typeof(bool);
			else if (delegateType.FullName.StartsWith("System.Func"))
			{
				var genericTypes = delegateType.GetGenericArguments();
				return genericTypes[genericTypes.Length - 1];
			}
			else
			{
				var method = delegateType.GetMethod("Invoke");
				return method.ReturnType;
			}
		}

		public static List<Tuple<MethodInfo, Attribute>> FindMethodsWithAttribute<Attribute, FuncTy>(Type type) where Attribute : class
		{
			var attributeType = typeof(Attribute);
			var members = new List<Tuple<MethodInfo, Attribute>>();

			var returnType = GetReturnTypeFromDelegate(typeof(FuncTy));
			var parameterTypes = GetParameterTypesFromDelegate(typeof(FuncTy));

			foreach (var method in type.GetMethods())
			{
				if (method.ReturnType != returnType) continue;
				var methodParams = method.GetParameterTypes();
				if (methodParams.Length != parameterTypes.Length) continue;
				if (!methodParams.PairWith(parameterTypes).All(pair => pair.Item1 == pair.Item2)) continue;

				foreach (var attribute in method.GetCustomAttributes(attributeType, false))
					members.Add(Tuple.Create(method, attribute as Attribute));
			}

			return members;
		}

		public static List<Type> FindDirectlyDerivedTypes<T>() { return FindDirectlyDerivedTypes(typeof(T)); }

		public static List<Type> FindDirectlyDerivedTypes(Type baseType)
		{
			var types = new List<Type>();
			foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
				foreach (var type in assembly.GetTypes())
					if (type.BaseType == baseType)
						types.Add(type);

			return types;
		}
	}
}
