﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	public static class IOHelper
	{
		public static void WriteQuoted(this TextWriter tw, string text)
		{
			tw.Write('"');
			for (int i = 0; i < text.Length; i++)
			{
				if (text[i] == '"' || text[i] == '\\')
					tw.Write("\\");
				tw.Write(text[i]);
			}
			tw.Write('"');
		}
	}
}
